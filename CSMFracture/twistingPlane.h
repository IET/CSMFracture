#ifndef TWISTING_PLANE_H
#define TWISTING_PLANE_H
#ifdef __cplusplus
#include <Eigen/Dense>
#endif
#include "particle.h"

struct TwistingPlane{
#ifdef __cplusplus
	TwistingPlane(Eigen::Vector3f _normal, float _offset, float _angularVelocity, float _width, float _lifetime)
		:normal(_normal.normalized()), offset(_offset), angularVelocity(_angularVelocity), width(_width), lifetime(_lifetime){}

	bool outside(Particle& particle) const;
	void twistParticle(Particle& particle, float timeElapsed) const;
	void backsideReflectBounceParticle(Particle& particle, float timeElapsed, float epsilon) const;
#endif
#ifdef __cplusplus
	Eigen::Vector3f normal; float pad;
#else
	float3 normal;
#endif
	float offset;
	float angularVelocity;
	float width;
	float lifetime;
};
#endif //TWISTING_PLANE_H