//Expanded from the code at http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
//using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessary OpenGL includes
#include <Eigen/Eigen>
#include <Eigen/Dense>

#include <IL/il.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "mesh.h"
#include "Shader.hpp"
#include "ShaderManager.hpp"

class Model
{
	friend class Mesh;

public:
	/*  Model Data */
	std::vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
	std::vector<Mesh> meshes;
	
	std::string directory;
	bool gammaCorrection;

	//Default constructor
	Model();

	// Constructor, expects a filepath to a 3D model.
	Model(std::string const & path, bool gamma = false);

	// Draws the model, and thus all its meshes
	void Draw(Eigen::Matrix4f model);

	Shader* getShader();

	void setShader(Shader* s);
	
	void load(string path);

	~Model();

private:
	Eigen::Vector3f location;
	Eigen::Vector3f rotation;

	Shader* shader;

	/*  Functions   */
	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene);

	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	
	GLint TextureFromFile(const char* path, string directory, bool gamma = false);

protected:
	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);

	// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes std::vector.
	void loadModel(string path);
};
