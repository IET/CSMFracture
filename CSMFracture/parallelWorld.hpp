#include "world.h"
#define __CL_ENABLE_EXCEPTIONS
#include <cl/cl.hpp>
#include <CL/cl_gl.h>

//Maximum number of fractures per frame
#define MAX_FRACTURES 64
//Max fractures / sizeof(int)
#define MAX_FRACTURES_INDICES 2

class ParallelWorld : public World
{
	//TODO - Should this be defined in the clUtils file?	

	float timers[22];

	//OpenCL Physics Kernels
	// Particle outside some moving plane
	cl::Kernel TiltingPlaneTestKernel;
	cl::Kernel TwistingPlaneTestKernel;

	// Cluster plasticity
	cl::Kernel ClearParticleGoalsKernel;
	cl::Kernel ClusterPlasticitiesKernel;
	cl::Kernel ParticleGoalKernel;
	cl::Kernel ZeroParticleGoalPosKernel;
	
	// Strain limiting iteration
	cl::Kernel StrainIterationKernel;
	cl::Kernel StrainLimitingGoalKernel;
	cl::Kernel ParticleVelocityKernel;
	cl::Kernel ClusterComUpdateKernel;

	// Handle fracture
	cl::Kernel FractureKernel;
	cl::Kernel SplitParticlesKernel;
	cl::Kernel SplitOutliersKernel;
	cl::Kernel CullSmallClustersKernel;
	cl::Kernel LonelyParticleKernel;

	// Count clusters
	cl::Kernel ClearParticleClustersKernel;
	cl::Kernel ClearClusterMembersKernel;
	cl::Kernel CountClusterKernel;
	cl::Kernel CountClusterPartKernel;

	// Cluster update
	cl::Kernel UpdateClusterPropsKernel;
	cl::Kernel UpdateClusterTransformsKernel;

	// Do plane interactions
	cl::Kernel MovingPlaneKernel;
	cl::Kernel TwistingPlaneKernel;
	cl::Kernel TiltingPlaneKernel;

	cl::Kernel UpdateClusterFpAndComKernel;
	
	cl_mem pBuff;
	// TODO - Finish initialising buffers
	//OpenCL Buffers
	cl::Buffer clusterBuffer;
	cl::Buffer fractureBuffer;
	cl::Buffer movingPlaneBuffer;
	cl::Buffer tiltingPlaneBuffer;
	cl::Buffer twistingPlaneBuffer;
	cl::Buffer activeParticleBuffer;
	cl::Buffer activeClusterBuffer;
	cl::Buffer activeFractureBuffer;

	std::vector<PotentialFracture> fracturesVector;
	uint fractureIndices[MAX_FRACTURES_INDICES];
	unsigned int *activeParticles;// = new unsigned int[particleFieldSize];
	unsigned int *activeClusters;// = new unsigned int[clusterFieldSize];
	
	//OpenCL Context
	cl::Context context;
	//OpenCL Program
	cl::Program clDynamics;
	//OpenCL Platform
	cl::Platform platform;
	//OpenCL Device
	cl::Device device;

	bool usingGPU;
	bool isAMD;
	//Handle for OpenGL Buffer if using glInterop
	//GLint glBuffer;
public:
	//OpenCL Command Queue
	cl::CommandQueue queue;

	int NUM_ELEMENTS = 512;

	bool glInterop;

	bool clInit();
	bool init();
	void initialiseKernels();
	bool initialiseBuffers(GLuint &glBuffer);
	bool initialiseBitArrays();

	void timestep();
	
	cl::Buffer *particleBuffer;

//	bool checkGLInterop();

	ParallelWorld(bool _usingGPU) :glInterop(_usingGPU), usingGPU(_usingGPU)
	{
		clInit();
	}

	~ParallelWorld()
	{
		delete particleBuffer;
		delete[] activeParticles;
		delete[] activeClusters;
	}
private:
	// Private default constructor so that it can't be used
	ParallelWorld();
};