#pragma once
#include <Eigen/Eigen>
#include "PlaneRenderer.h"
#include "tiltingPlane.h"

namespace Renderer
{
	void DrawPlane(Eigen::Matrix4f &v, Eigen::Matrix4f &p, Eigen::Vector3f &position, Eigen::Vector3f &normal, const float &size, Eigen::Vector4f &color)
	{
		PlaneRenderer::DrawPlane(v, p, position, normal, size, color);
	}
	
	void DrawPlane(Eigen::Matrix4f &v, Eigen::Matrix4f &p, Eigen::Vector3f &position, TiltingPlane &tp, const float &t, const float &size, Eigen::Vector4f &color)
	{
		PlaneRenderer::DrawPlane(v, p, position, tp, t, size, color);
	}
}