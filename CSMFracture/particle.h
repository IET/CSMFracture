#ifndef PARTICLE_H
#define PARTICLE_H

#ifdef __cplusplus
#include <Eigen/Eigen>
#include <Eigen/Dense>
#include <vector>
#include <unordered_set>
#include "color_spaces.h"
#endif

#include "GPUDataTypes.h"

#define MAX_CLUSTERS 10
#define MAX_MEMBERS 768
#define MAX_NEIGHBORS 192
#define MAX_PLANES 32

#define SPLIT 1

#ifdef __cplusplus
struct Particle;
inline void noop(Particle&){}
#endif
struct ParticleCluster
{
	int index;
	float weight;
};

// Note: OpenCl float3 is actually a special case of a float4 so four floats are allocated
// This means that they won't be alligned with Eigen vectors so a (messy) workaround is the padding below
// Could also implement custom vector3 type but I'm not sure that will work with OpenCL SIMD optimisations
struct Particle {
#ifdef __cplusplus
	Eigen::Vector3f	position; float pad;
	Eigen::Vector3f	velocity; float pad1;
	Eigen::Vector3f	restPosition; float pad2;
	Eigen::Vector3f	oldPosition; float pad3;
	Eigen::Vector3f	goalPosition; float pad4;
	Eigen::Vector3f	goalVelocity; float pad5;
	cl_float3 color;
#else
	float3	position,
		velocity,
		restPosition,
		oldPosition,
		goalPosition,
		goalVelocity;
	float3 color;
#endif
	float mass;
	float totalweight; // sum of weights of this particle in all clusters

	int outsideSomeMovingPlane;
	struct ParticleCluster clusters[MAX_CLUSTERS];
	int numClusters;

	short flags;
	int id;
	float padding[2];
};

struct CgPlane {
#ifdef __cplusplus
	Eigen::Vector3f normal; float pad;
#else
	float3 normal;
#endif
	float offset;
	//Device is padding struct to 32b
	float pad1, pad2, pad3;
};

typedef struct CgPlane CgPlane;

struct CollisionGeometry {
#ifdef __cplusplus
	CollisionGeometry() { numPlanes = 0; };
	CollisionGeometry & operator= (const CollisionGeometry &that);
	CollisionGeometry(const CollisionGeometry &that) = default;
	bool project(Eigen::Vector3f &x);
	void init(const Eigen::Vector3f &c, float r) { this->c = c; this->r = r; };
	void addPlane(const Eigen::Vector3f &n, float offset) { planes[numPlanes++] = { n, offset }; };
#endif

	CgPlane planes[MAX_PLANES];

#ifdef __cplusplus
	Eigen::Vector3f c; float pad;
#else
	float3 c;
#endif

	float r;
	int numPlanes;
	float pad1, pad2;
};

//contains the particle index and its weight
struct ClusterMember {
	int index;
	float weight;
#ifdef __cplusplus
	ClusterMember()
	{
		index = -1;
		weight = 0.0f;
	}
	ClusterMember(int i, float w)
	{
		index = i;
		weight = w;
	}
#endif
};
typedef struct ClusterMember ClusterMember;


struct _PotentialFracture{
#ifdef __cplusplus
	_PotentialFracture() {valid = false;};
	cl_float3 normal;
	cl_float3 worldCom;
	cl_float3 splitDirection;
#else
	float3 normal;
	float3 worldCom;
	float3 splitDirection;
#endif
	float effectiveToughness;
	int valid;
	int cIndex;
	int newClusterIndex;
};
typedef struct _PotentialFracture PotentialFracture;

struct Cluster {
#ifdef __cplusplus
	Cluster() { Fp.setIdentity(); cstrain = 0.0f; numMembers = 0; numNeighbors = 0; justFractured = false; }
	Cluster(const Cluster &c) = default;

	void updatePlasticity(const Eigen::Vector3f &sigma, const Eigen::Matrix3f &U, const Eigen::Matrix3f &V,
		float yield, float nu, float hardening);
	Eigen::Matrix4f getVisTransform() const;
#endif

	struct ClusterMember members[MAX_MEMBERS];
	int neighbors[MAX_NEIGHBORS];  // Note: this refers to neighboring CLUSTERS 
//	PotentialFracture potentialFracture;
	struct CollisionGeometry cg;

#ifdef __cplusplus
	Eigen::Matrix3f aInv, Fp, FpNew;
	Eigen::Matrix3f worldToRestTransform, restToWorldTransform;
	Eigen::Matrix3f T;
	Eigen::Matrix3f V, Apq;	//TODO - Remove these
	Eigen::Vector3f restCom; float pad;
	Eigen::Vector3f worldCom; float pad1;
	Eigen::Vector3f velocity; float pad2;
#else
	mat3f aInv, Fp, FpNew;
	mat3f worldToRestTransform, restToWorldTransform;
	mat3f T;
	mat3f V, Apq;
	float3 restCom;
	float3 worldCom;
	float3 velocity;
#endif

	int numMembers;
	int numNeighbors;

	float mass, width;// , renderWidth;
	float toughness;
	float cstrain; // cumulative strain (for work hardening)

	int justFractured;
	float timeSinceLastFracture;
	//float pad3;
	//, pad4, pad5, pad6, pad7, pad8, pad9, pad10, pad11;
};

//Typedefs so that C will not require struct keyword
typedef struct Cluster				Cluster;
typedef struct Particle				Particle;
typedef struct CollisionGeometry	CollisionGeometry;
typedef struct ClusteringParams		ClusteringParams;

#endif //PARTICLE_H