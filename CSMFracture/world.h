#pragma once

#include <iostream>
#include "particle.h"
#include "movingPlane.h"
#include "twistingPlane.h"
#include "tiltingPlane.h"
#include "projectile.hpp"
#include "cylinderObstacle.hpp"

#include "preallocVector.hpp"
#include "profiler.hpp"

//Avoid memory issues with STL vectors
#define EIGEN_USE_NEW_STDVECTOR
#include <Eigen/StdVector>

#include "clustering.h"

#include "WorldParams.h"

//OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <cl/cl.hpp>
//undefine min and max macros so that OpenCL doesn't interfere with the std library functions
#undef max
#undef min
class World {
public:
	//TODO - Remove these, only used for initialisation.
	int numClusters, numParticles;
	std::string filename; //for reloading the file

	void loadFromJson(const std::string& _filename);


	struct ParticleSet {
		std::vector<Eigen::Vector3f> positions;
		Eigen::Vector3f bbMin, bbMax;
	};

	ParticleSet readParticleFile(const std::string& _filename);
	void saveParticleFile(const std::string& _filename) const;

	void timestep();
	void parallelTimestep(cl::Buffer &clusterBuffer, cl::Buffer &particleBuffer);

	void assertFinite() const {
		for (auto& p : particles) {
			assert(p.position.allFinite());
		}
	}

	struct FractureInfo {
		size_t clusterIndex;
		float effectiveToughness;
		Eigen::Vector3f fractureNormal;
	};

	void doFracture(std::vector<FractureInfo> &potentialSplits);
	void splitOutliers();
	void cullSmallClusters();
	void removeLonelyParticles();

	inline void restart() {
		/*	Eigen::Vector3f oldCameraPosition = cameraPosition;
		Eigen::Vector3f oldCameraLookAt = cameraLookAt;
		Eigen::Vector3f oldCameraUp = cameraUp;
		*/
		particles.clear();
		clusters.clear();
		planes.clear();
		movingPlanes.clear();
		twistingPlanes.clear();
		tiltingPlanes.clear();
		projectiles.clear();
		cylinders.clear();
		clusterCenters.clear();

		loadFromJson(filename);
		initializeNeighbors();
		/*
		cameraPosition = oldCameraPosition;
		cameraLookAt = oldCameraLookAt;
		cameraUp = oldCameraUp;
		*/
		prof.dump<std::chrono::duration<float>>(std::cout);
		prof.reset();
		elapsedTime = 0;
	}

	void dumpParticlePositions(const std::string& filename) const;
	void dumpClippedSpheres(const std::string& filename) const;
	void dumpColors(const std::string& filename) const;


	//refactored out into vis, camera/settings stuff
	/*  void draw(SDL_Window* window) const ;
	void drawPretty(SDL_Window* window) const ;
	void drawSingleCluster(SDL_Window* window, int frame) const;
	void drawPlanes() const;
	void drawPlanesPretty() const;
	void drawTPlane(const Eigen::Vector3f& normal, float offset, float roffset, float width) const;
	void drawTiltPlane(const Eigen::Vector3f& normal, const Eigen::Vector3f& tilt, float offset, float roffset, float width) const;clus
	void zoom(int amount);
	void pan(Eigen::Vector2i oldposition,
	Eigen::Vector2i newPosition);

	void move(bool forward);
	*/
	void bounceOutOfPlanes();

	// std::vector<std::vector<int> > clusterCollisionMap;
	//std::unordered_set<std::pair<int,int>> clusterCollisionMap;
	//void buildClusterMaps();
	void initializeNeighbors();
	void selfCollisions();

	void solveConstraints();
	void updateNeighbors(size_t partIndex);


	void setupPlaneConstraints();

	void countClusters();
	void mergeClusters(const std::vector<Particle>& newParticles,
		const std::vector<Cluster>& newClusters);

	//pass a container of clusters to update
	template <typename Container>
	void updateClusterProperties(const Container& clusterIndices);

	std::vector<Particle> particles;
	std::vector<Cluster> clusters;

	void printCOM() const;

	Eigen::Vector3f computeClusterVelocity(const Cluster& c) const;

	template <typename cont>
	float sumMass(const cont& indices) const {
		return std::accumulate(indices.begin(), indices.end(),
			0.0f,
			[this](float acc, typename cont::value_type index) {
			return acc + particles[index].mass;
		});
	}

	float sumWeightedMass(const ClusterMember* members, int &numMembers) const {
		float mass = 0.0f;
		for (int i = 0; i < numMembers; ++i) {
			auto &member = members[i];
			auto &p = particles[member.index];
			mass += (member.weight / p.totalweight) * p.mass;
		}
		return mass;
	}

	Eigen::Vector3f sumWeightedRestCOM(const ClusterMember* members, int &numMembers) const {
		float mass = 0.0f;
		Eigen::Vector3f com = Eigen::Vector3f::Zero();
		for (int i = 0; i < numMembers; ++i) {
			auto &member = members[i];
			auto &p = particles[member.index];
			float w = (member.weight / p.totalweight) * p.mass;
			mass += w;
			com += w * p.restPosition;
		}
		return (com / mass);
	}

	Eigen::Vector3f sumWeightedWorldCOM(const ClusterMember* members, int &numMembers) const {
		float mass = 0.0f;
		Eigen::Vector3f com = Eigen::Vector3f::Zero();
		for (int i = 0; i < numMembers; ++i) {
			auto &member = members[i];
			auto &p = particles[member.index];
			float w = (member.weight / p.totalweight) * p.mass;
			mass += w;
			com += w * p.position;
		}
		return (com / mass);
	}

	//compute the APQ matrix (see meuller 2005)
	Eigen::Matrix3f computeApq(const Cluster& c) const;
	void updateTransforms(Cluster& c) const;

	inline Eigen::Vector3f getMomentum() {
		return std::accumulate(particles.begin(), particles.end(),
			Eigen::Vector3f{ 0.0f,0.0f,0.0f },
			[](Eigen::Vector3f acc, const Particle& p) {
			return acc + p.velocity;
		});
	}


	void strainLimitingIteration();
	
	WorldParams worldParameters;

	std::vector<Eigen::Vector4f, Eigen::aligned_allocator<Eigen::Vector4f>> planes;
	std::vector<MovingPlane> movingPlanes;
	std::vector<TwistingPlane> twistingPlanes;
	std::vector<TiltingPlane> tiltingPlanes;
	std::vector<Projectile> projectiles;
	std::vector<CylinderObstacle> cylinders;
	std::vector<size_t> clusterCenters;
	
	//float dt, elapsedTime;
	float elapsedTime;
	float neighborRadius, neighborRadiusMax;

	ClusteringParams clusteringParams;

	bool fractureOn, selfCollisionsOn;

	//int numConstraintIters;
	//float omega, gamma, alpha, springDamping;
	//float yield, nu, hardening; // plasticity parameters
	//float toughness, toughnessBoost, toughnessFalloff;
	//float collisionRestitution;
	//float collisionGeometryThreshold;
	//float outlierThreshold;

	benlib::Profiler prof;

	~World()
	{
		std::cout << "World Profiler Percentages:" << std::endl;
		prof.dumpPercentages();
	}
};
