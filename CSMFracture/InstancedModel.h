#pragma once

#include "model.h"
#include "InstancedMesh.h"
#include "Shader.hpp"


class InstancedModel : public Model {
public:

	InstancedModel();
	InstancedModel(int n);

	//TODO - This shouldn't really be public
	Eigen::Vector3f *positions;
	Eigen::Vector4f *colors;

	// Constructor, expects a filepath to a 3D model. Allows instanced rendering.
	InstancedModel(string const & path, int instances, bool gamma = false);
	InstancedModel(std::string const & path, std::vector<Particle> &particles, bool gamma = false);

	//Hiding parent methods
	//Draw the model using instanced rendering
	void Draw(Eigen::Matrix4f &v, Eigen::Matrix4f &p);
	void Draw(Eigen::Matrix4f &v, Eigen::Matrix4f &p, int numInstances);
	void Draw(Eigen::Matrix4f &v, Eigen::Matrix4f &p, std::vector<Particle> &particles);

	//Getters and Setters
	int getInstances();

	InstancedModel::~InstancedModel();

	std::vector<InstancedMesh> meshes;

private:
	int instances;

	
	//TODO - find a better method than just hiding all of these
	//Hiding parent methods
	InstancedMesh processMesh(aiMesh* mesh, const aiScene* scene);
	InstancedMesh processMesh(aiMesh* mesh, const aiScene* scene, std::vector<Particle> &particles);

	void processNode(aiNode* node, const aiScene* scene);
	void processNode(aiNode* node, const aiScene* scene, std::vector<Particle> &particles);

	void loadModel(std::string path);
	void loadModel(std::string path, std::vector<Particle> &particles);

	void setInstances(int n);
};