#ifndef COLORSPACES_H
#define COLORSPACES_H

#include <algorithm>
#include <iostream>
#include <math.h>
#include <vector>

class RGBColor;
class HSLColor;

class RGBColor  {
	public:
		RGBColor() : r(0), g(0), b(0)  {}
		RGBColor(float _r, float _g, float _b) : r(_r), g(_g), b(_b)  {}

		HSLColor to_hsl();

		float r,g,b;

		friend std::ostream& operator <<(std::ostream &out, const RGBColor& _vec)  {
			out << "<" << _vec.r << ", " << _vec.g << ", " << _vec.b << ">";
			return out;
		}

	private:
};

class HSLColor  {
	public:
		HSLColor() : h(0), s(0), l(0)  {}
		HSLColor(float _h, float _s, float _l) : h(_h), s(_s), l(_l)  {}

		RGBColor to_rgb();

		friend std::ostream& operator <<(std::ostream &out, const HSLColor& _vec)  {
			out << "<" << _vec.h << ", " << _vec.s << ", " << _vec.l << ">";
			return out;
		}

		float h,s,l;
};

#endif
