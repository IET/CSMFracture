#include "parallelWorld.hpp"
#include "clUtils.hpp"

size_t uSnap(size_t a, size_t b) {
	return ((a % b) == 0) ? a : (a - (a % b) + b);
}

bool ParallelWorld::clInit()
{
	if (!clUtils::getPlatform(usingGPU, platform))
		return false;

	if (!clUtils::getDevice(usingGPU, platform, device))
		return false;

	isAMD = (!strcmp(platform.getInfo<CL_PLATFORM_VENDOR>().c_str(), "Advanced Micro Devices, Inc."));
		
	if (usingGPU)
	{
		if (!clUtils::getClGlContext(platform, device, context, usingGPU, glInterop))
			return false;
	}
	else
	{
		context = cl::Context(device);
		glInterop = false;
	}
	
	if (glInterop)
		glInterop = clUtils::checkGLInterop(device);

	if (!clUtils::buildClProgram(context, device, clDynamics, "Dynamics.cl"))
		return false;
/*
	if (!clUtils::readBinary("Dynamics.bin", context, device, clDynamics))
		return false;
*/
	try {
		queue = cl::CommandQueue(context, device);
	}
	catch (cl::Error err)
	{
		std::cout << "Failed to create queue" << std::endl;
		std::cout << "Error in method: " << err.what() << std::endl;
		std::cout << "Error returned: " << clUtils::getErrorString(err.err()) << std::endl;
		return false;
	}

	cl_int Device_max_workgroup_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	if (NUM_ELEMENTS > Device_max_workgroup_size)
		NUM_ELEMENTS = Device_max_workgroup_size;

	return true;
}

void ParallelWorld::initialiseKernels()
{
	// Particle outside some moving plane
	TiltingPlaneTestKernel  = cl::Kernel(clDynamics, "TestOutsideTiltingPlane");
	TwistingPlaneTestKernel = cl::Kernel(clDynamics, "TestOutsideTwistingPlane");

	// Cluster plasticity
	ClearParticleGoalsKernel  = cl::Kernel(clDynamics, "ClearParticleGoals");
	ClusterPlasticitiesKernel = cl::Kernel(clDynamics, "UpdateClusterPlasticities");
	ParticleGoalKernel 		  = cl::Kernel(clDynamics, "UpdateParticleGoals");
	
	// Strain limiting iteration
	ZeroParticleGoalPosKernel = cl::Kernel(clDynamics, "ZeroParticleGoalPositions");
	StrainIterationKernel 	  = cl::Kernel(clDynamics, "StrainLimitingIteration");
	StrainLimitingGoalKernel  = cl::Kernel(clDynamics, "StrainLimitingGoalUpdate");

	ParticleVelocityKernel 	 = cl::Kernel(clDynamics, "UpdateParticleVelocities");
	ClusterComUpdateKernel 	 = cl::Kernel(clDynamics, "UpdateClusterComs");

	// Handle fracture
	FractureKernel 			= cl::Kernel(clDynamics, "FracturePotentialSplits");
	SplitParticlesKernel 	= cl::Kernel(clDynamics, "SplitParticles");
	SplitOutliersKernel 	= cl::Kernel(clDynamics, "SplitOutliers");
	CullSmallClustersKernel = cl::Kernel(clDynamics, "CullSmallClusters");
	LonelyParticleKernel 	= cl::Kernel(clDynamics, "RemoveLonelyParticles");

	// Count clusters
	ClearParticleClustersKernel = cl::Kernel(clDynamics, "ClearParticleClusters");
	ClearClusterMembersKernel 	= cl::Kernel(clDynamics, "ClearClusterMembers");
	CountClusterKernel 			= cl::Kernel(clDynamics, "CountClusters");
	CountClusterPartKernel 		= cl::Kernel(clDynamics, "CountClustersPerParticle");

	// Cluster update
	UpdateClusterPropsKernel 	  = cl::Kernel(clDynamics, "UpdateClusterProperties");
	UpdateClusterTransformsKernel = cl::Kernel(clDynamics, "UpdateClusterTransforms");

	// Do plane interactions
	MovingPlaneKernel 	= cl::Kernel(clDynamics, "MovingPlaneInteraction");
	TwistingPlaneKernel = cl::Kernel(clDynamics, "TwistingPlaneInteraction");
	TiltingPlaneKernel 	= cl::Kernel(clDynamics, "TiltingPlaneInteraction");

	UpdateClusterFpAndComKernel = cl::Kernel(clDynamics, "UpdateFpAndCom");
}

bool ParallelWorld::initialiseBuffers(GLuint &glBuffer)
{
	cl_int status = CL_SUCCESS;
	cl::Event event;
	bool success = true;

	cl::Context glContext;

	if (glInterop)
		particleBuffer = new cl::BufferGL();
	else
		particleBuffer = new cl::Buffer();

	// Particles
	try {
		if (glInterop)
			*particleBuffer = cl::BufferGL(context, CL_MEM_READ_WRITE, glBuffer, &status);
		else
		{
			if (usingGPU)
			{
				*particleBuffer = cl::Buffer(particles.begin(), particles.end(), false);
			}
			else
			{
				cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_WRITE | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), particles.size() * sizeof(Particle), &*particles.begin(), &status);
				clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(Particle) * particles.size(), &*particles.begin(), 0, NULL, &event());
				queue.finish();
				*particleBuffer = cl::Buffer(mem);
			}
		}
	}
	catch (cl::Error err)
	{
		std::cout << "Exception creating Buffer For Particles" << std::endl;
		std::cout << "Error: " << clUtils::getErrorString(err.err()) << " in method: " << err.what() << std::endl;
		success = false;
	}

	// Clusters
	try {
		if (usingGPU && isAMD)
		{
			clusterBuffer = cl::Buffer(clusters.begin(), clusters.end(), false);
		}
		else
		{
			cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_WRITE | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), clusters.size() * sizeof(Cluster), &*clusters.begin(), &status);
			clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(Cluster) * clusters.size(), &*clusters.begin(), 0, NULL, &event());
			queue.finish();
			clusterBuffer = cl::Buffer(mem);
		}
	}
	catch (cl::Error err)
	{
		std::cout << "Error creating Cluster Buffer" << std::endl;
		std::cout << "Error in method: " << err.what() << std::endl;
		std::cout << "Error returned: " << clUtils::getErrorString(err.err()) << std::endl;
		success = false;
	}

	// Moving Planes
	if (movingPlanes.size() > 0)
	{
		try {
			if (usingGPU && isAMD)
			{
				movingPlaneBuffer = cl::Buffer(movingPlanes.begin(), movingPlanes.end(), true);
			}
			else
			{
				cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_ONLY | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), movingPlanes.size() * sizeof(MovingPlane), &*movingPlanes.begin(), &status);
				clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(MovingPlane) * movingPlanes.size(), &*movingPlanes.begin(), 0, NULL, &event());
				queue.finish();
				movingPlaneBuffer = cl::Buffer(mem);
			}
		}
		catch (cl::Error err)
		{
			std::cout << "Error creating Moving Plane Buffer" << std::endl;
			std::cout << "Error in method: " << err.what() << std::endl;
			std::cout << "Error returned: " << clUtils::getErrorString(err.err()) << std::endl;
			success = false;
		}
	}

	// Twisting Planes
	if (twistingPlanes.size() > 0)
	{

		try {
			if (usingGPU && isAMD)
			{
				twistingPlaneBuffer = cl::Buffer(twistingPlanes.begin(), twistingPlanes.end(), true);
			}
			else
			{
				cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_ONLY | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), twistingPlanes.size() * sizeof(TwistingPlane), &*twistingPlanes.begin(), &status);
				clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(TwistingPlane) * twistingPlanes.size(), &*twistingPlanes.begin(), 0, NULL, &event());
				queue.finish();
				twistingPlaneBuffer = cl::Buffer(mem);
			}
		}
		catch (cl::Error err)
		{
			std::cout << "Error creating Twisting Plane Buffer" << std::endl;
			std::cout << "Error in method: " << err.what() << std::endl;
			std::cout << "Error returned: " << clUtils::getErrorString(err.err()) << std::endl;
			success = false;
		}
	}

	// Tilting Planes
	if (tiltingPlanes.size() > 0)
	{
		try {
			if (usingGPU && isAMD)
			{
				tiltingPlaneBuffer = cl::Buffer(tiltingPlanes.begin(), tiltingPlanes.end(), true);
			}
			else
			{
				cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_ONLY | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), tiltingPlanes.size() * sizeof(TiltingPlane), &*tiltingPlanes.begin(), &status);
				clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(TiltingPlane) * tiltingPlanes.size(), &*tiltingPlanes.begin(), 0, NULL, &event());
				queue.finish();
				tiltingPlaneBuffer = cl::Buffer(mem);
			}
		}
		catch (cl::Error err)
		{
			std::cout << "Error creating Tilting Plane Buffer" << std::endl;
			std::cout << "Error in method: " << err.what() << std::endl;
			std::cout << "Error returned: " << clUtils::getErrorString(status) << std::endl;
			success = false;
		}
	}

	int particleFieldSize = (particles.size() / 32) + 1;
	int clusterFieldSize = (clusters.size() / 32) + 1;
	activeParticles = new unsigned int[particleFieldSize];
	activeClusters = new unsigned int[clusterFieldSize];

	//Initialise particle bits
	for (int i = 0; i < (numParticles / 32); ++i)
		activeParticles[i] |= 0xffffffff;
	for (int i = (numParticles / 32); i < particleFieldSize; ++i)
		activeParticles[i] &= 0x00000000;
	for (int i = 0; i < numParticles % 32; ++i)
		activeParticles[numParticles / 32] |= (0x1 << i);

	//Initialise Cluster bits
	for (int i = 0; i < (numClusters / 32); ++i)
		activeClusters[i] |= 0xffffffff;
	for (int i = (numClusters / 32); i < clusterFieldSize; ++i)
		activeClusters[i] &= 0x00000000;
	for (int i = 0; i < numClusters % 32; ++i)
		activeClusters[numClusters / 32] |= (0x1 << i);
	for (int i = 0; i < MAX_FRACTURES_INDICES; ++i)
		fractureIndices[i] = 0;

	try {
		if (usingGPU && isAMD)
		{
			activeParticleBuffer = cl::Buffer(activeParticles, activeParticles + particleFieldSize, false);
			activeClusterBuffer = cl::Buffer(activeClusters, activeClusters + clusterFieldSize, false);
			activeFractureBuffer = cl::Buffer(fractureIndices, fractureIndices + 1, false);
		}
		else
		{
			cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_WRITE | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), particleFieldSize * sizeof(uint), &activeParticles[0], &status);
			clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(uint) * particleFieldSize, &activeParticles[0], 0, NULL, &event());
			queue.finish();
			activeParticleBuffer = cl::Buffer(mem);

			mem = clCreateBuffer(context(), CL_MEM_READ_WRITE | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), clusterFieldSize * sizeof(uint), &activeClusters[0], &status);
			clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(uint) * clusterFieldSize, &activeClusters[0], 0, NULL, &event());
			queue.finish();
			activeClusterBuffer = cl::Buffer(mem);

			mem = clCreateBuffer(context(), CL_MEM_READ_WRITE | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), MAX_FRACTURES_INDICES * sizeof(uint), &fractureIndices[0], &status);
			clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(uint), &fractureIndices[0], 0, NULL, &event());
			queue.finish();
			activeFractureBuffer = cl::Buffer(mem);
		}
	}
	catch (cl::Error err)
	{
		std::cout << "Failed to create active particle and cluster buffers, " << err.what() << " returned: " << err.err() << std::endl;
		success = false;
	}

	try {
		fracturesVector.resize(32);

		if (usingGPU && isAMD)
		{
			fractureBuffer = cl::Buffer(fracturesVector.begin(), fracturesVector.end(), false);
		}
		else
		{

			cl_mem mem = clCreateBuffer(context(), CL_MEM_READ_WRITE | (usingGPU ? 0 : CL_MEM_USE_HOST_PTR), fracturesVector.size() * sizeof(PotentialFracture), &*fracturesVector.begin(), &status);
			clEnqueueWriteBuffer(queue(), mem, CL_TRUE, 0, sizeof(PotentialFracture), &*fracturesVector.begin(), 0, NULL, &event());
			queue.finish();
			fractureBuffer = cl::Buffer(mem);
		}
	}
	catch (cl::Error err)
	{
		std::cout << "Failed to create fracture buffer, " << err.what() << " returned: " << err.err() << std::endl;
		success = false;
	}

	return success;
}

//TODO - Maybe write a wrapper for each method? So much boilerplate...
// Yeah, definitely make them different methods
// But very few are re-used...
void ParallelWorld::timestep()
{
	// TODO - This shouldn't be defined here
	int numFractures = 32;
	
	if (glInterop)
	{
		glFinish();

		cl_int status = CL_SUCCESS;
		status = clEnqueueAcquireGLObjects(queue(), 1, &(*particleBuffer)(), 0, NULL, NULL);

		if (status != CL_SUCCESS)
		{
			std::cerr << "Error aquiring glObjects." << std::endl << "Returned error: " << clUtils::getErrorString(status) << std::endl;
		}
	}

	try {
		int numMovingPlanes = movingPlanes.size();
		int numTwistingPlanes = twistingPlanes.size();
		int numTiltingPlanes = tiltingPlanes.size();

		if (numTiltingPlanes > 0)
		{
			TiltingPlaneTestKernel.setArg(0, *particleBuffer);
			TiltingPlaneTestKernel.setArg(1, twistingPlaneBuffer);
			TiltingPlaneTestKernel.setArg(2, activeParticleBuffer);
			TiltingPlaneTestKernel.setArg(3, numTiltingPlanes);
			TiltingPlaneTestKernel.setArg(4, elapsedTime);
			queue.enqueueNDRangeKernel(TiltingPlaneTestKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		}
		
		if (numTwistingPlanes > 0)
		{
			TwistingPlaneTestKernel.setArg(0, *particleBuffer);
			TwistingPlaneTestKernel.setArg(1, twistingPlaneBuffer);
			TwistingPlaneTestKernel.setArg(2, activeParticleBuffer);
			TwistingPlaneTestKernel.setArg(3, numTwistingPlanes);
			TwistingPlaneTestKernel.setArg(4, elapsedTime);
			queue.enqueueNDRangeKernel(TwistingPlaneTestKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		}
		queue.finish();

		ClearParticleGoalsKernel.setArg(0, *particleBuffer);
		ClearParticleGoalsKernel.setArg(1, activeParticleBuffer);
		queue.enqueueNDRangeKernel(ClearParticleGoalsKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		queue.finish();

		ClusterPlasticitiesKernel.setArg(0, clusterBuffer);
		ClusterPlasticitiesKernel.setArg(1, *particleBuffer);
		ClusterPlasticitiesKernel.setArg(2, activeClusterBuffer);
		ClusterPlasticitiesKernel.setArg(3, fractureBuffer);
		ClusterPlasticitiesKernel.setArg(4, activeFractureBuffer);
		ClusterPlasticitiesKernel.setArg(5, worldParameters);
		queue.enqueueNDRangeKernel(ClusterPlasticitiesKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		queue.finish();

		ParticleGoalKernel.setArg(0, *particleBuffer);
		ParticleGoalKernel.setArg(1, clusterBuffer);
		ParticleGoalKernel.setArg(2, activeParticleBuffer);
		ParticleGoalKernel.setArg(3, worldParameters);
		queue.enqueueNDRangeKernel(ParticleGoalKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		queue.finish();


		for (int i = 0; i < worldParameters.numConstraintIters; ++i)
		{
			ZeroParticleGoalPosKernel.setArg(0, *particleBuffer);
			ZeroParticleGoalPosKernel.setArg(1, activeParticleBuffer);
			queue.enqueueNDRangeKernel(ZeroParticleGoalPosKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			StrainIterationKernel.setArg(0, clusterBuffer);
			StrainIterationKernel.setArg(1, *particleBuffer);
			StrainIterationKernel.setArg(2, activeClusterBuffer);
			StrainIterationKernel.setArg(3, worldParameters);
			queue.enqueueNDRangeKernel(StrainIterationKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			StrainLimitingGoalKernel.setArg(0, *particleBuffer);
			StrainLimitingGoalKernel.setArg(1, clusterBuffer);
			StrainLimitingGoalKernel.setArg(2, activeParticleBuffer);
			StrainLimitingGoalKernel.setArg(3, worldParameters);
			queue.enqueueNDRangeKernel(StrainLimitingGoalKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();
		}

		ParticleVelocityKernel.setArg(0, *particleBuffer);
		ParticleVelocityKernel.setArg(1, activeParticleBuffer);
		ParticleVelocityKernel.setArg(2, worldParameters);
		queue.enqueueNDRangeKernel(ParticleVelocityKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
//		queue.finish();

		ClusterComUpdateKernel.setArg(0, clusterBuffer);
		ClusterComUpdateKernel.setArg(1, *particleBuffer);
		ClusterComUpdateKernel.setArg(2, activeClusterBuffer);
		queue.enqueueNDRangeKernel(ClusterComUpdateKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		queue.finish();

		if (fractureOn)
		{
			// V. High performance impact (29%)
			//auto timestepTime = physicsProf.timeName("fracture");
			{
				//auto timestepTime = fractureProf.timeName("doFracture");	
				FractureKernel.setArg(0, fractureBuffer);
				FractureKernel.setArg(1, activeFractureBuffer);
				FractureKernel.setArg(2, clusterBuffer);
				FractureKernel.setArg(3, *particleBuffer);
				FractureKernel.setArg(4, activeClusterBuffer);
				FractureKernel.setArg(5, activeParticleBuffer);
				FractureKernel.setArg(6, worldParameters);
				queue.enqueueNDRangeKernel(FractureKernel, cl::NullRange, cl::NDRange(numFractures), cl::NDRange(numFractures));
				queue.finish();

			}

			{
				//auto timestepTime = fractureProf.timeName("countClusters");
				ClearParticleClustersKernel.setArg(0, *particleBuffer);
				ClearParticleClustersKernel.setArg(1, activeParticleBuffer);
				queue.enqueueNDRangeKernel(ClearParticleClustersKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();

				CountClusterKernel.setArg(0, clusterBuffer);
				CountClusterKernel.setArg(1, *particleBuffer);
				CountClusterKernel.setArg(2, activeClusterBuffer);
				CountClusterKernel.setArg(3, activeParticleBuffer);
				queue.enqueueNDRangeKernel(CountClusterKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}

			UpdateClusterPropsKernel.setArg(0, clusterBuffer);
			UpdateClusterPropsKernel.setArg(1, *particleBuffer);
			UpdateClusterPropsKernel.setArg(2, activeClusterBuffer);
			queue.enqueueNDRangeKernel(UpdateClusterPropsKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			UpdateClusterTransformsKernel.setArg(0, clusterBuffer);
			UpdateClusterTransformsKernel.setArg(1, *particleBuffer);
			UpdateClusterTransformsKernel.setArg(2, activeClusterBuffer);
			queue.enqueueNDRangeKernel(UpdateClusterTransformsKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			{
				//TODO - Can these be one kernel? They are kinda similar and don't require syncing between runs
				//auto timestepTime = fractureProf.timeName("splitParticles");
				SplitParticlesKernel.setArg(0, *particleBuffer);
				SplitParticlesKernel.setArg(1, clusterBuffer);
				SplitParticlesKernel.setArg(2, activeParticleBuffer);
				SplitParticlesKernel.setArg(3, fractureBuffer);
				SplitParticlesKernel.setArg(4, activeFractureBuffer);
				queue.enqueueNDRangeKernel(SplitParticlesKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();

				SplitOutliersKernel.setArg(0, *particleBuffer);
				SplitOutliersKernel.setArg(1, clusterBuffer);
				SplitOutliersKernel.setArg(2, activeParticleBuffer);
				SplitOutliersKernel.setArg(3, worldParameters);
				queue.enqueueNDRangeKernel(SplitOutliersKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}

			{
				//auto timestepTime = fractureProf.timeName("countParticleClusters");
				//CountClusters again but this time assigning cluster members based on particle clusters
				ClearClusterMembersKernel.setArg(0, clusterBuffer);
				ClearClusterMembersKernel.setArg(1, activeClusterBuffer);
				queue.enqueueNDRangeKernel(ClearClusterMembersKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();

				CountClusterPartKernel.setArg(0, *particleBuffer);
				CountClusterPartKernel.setArg(1, clusterBuffer);
				CountClusterPartKernel.setArg(2, activeParticleBuffer);
				CountClusterPartKernel.setArg(3, activeClusterBuffer);
				queue.enqueueNDRangeKernel(CountClusterPartKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}

			{
				//auto timestepTime = fractureProf.timeName("smallClustersAndLonelyParticles");

				CullSmallClustersKernel.setArg(0, clusterBuffer);
				CullSmallClustersKernel.setArg(1, *particleBuffer);
				CullSmallClustersKernel.setArg(2, activeClusterBuffer);
				queue.enqueueNDRangeKernel(CullSmallClustersKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();

				LonelyParticleKernel.setArg(0, *particleBuffer);
				LonelyParticleKernel.setArg(1, clusterBuffer);
				LonelyParticleKernel.setArg(2, activeParticleBuffer);
				queue.enqueueNDRangeKernel(LonelyParticleKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}
			//TODO - Do I even need these indices? fractures already have a bool to say whether they're valid
			//Clear fracture indices
			cl::Event event;
			// Note: cl::copy uses EnqueueMapBuffer rather than write buffer so could possibly use that on all platforms
			if (usingGPU && isAMD)
				cl::copy(fractureIndices, fractureIndices + 1, activeFractureBuffer);
			else
				clEnqueueWriteBuffer(queue(), activeFractureBuffer(), CL_TRUE, 0, sizeof(uint), &fractureIndices[0], 0, NULL, &event());

		} //if fractureOn

		{
			// auto timestepTime = physicsProf.timeName("countClusters");
			ClearParticleClustersKernel.setArg(0, *particleBuffer);
			ClearParticleClustersKernel.setArg(1, activeParticleBuffer);
			queue.enqueueNDRangeKernel(ClearParticleClustersKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			CountClusterKernel.setArg(0, clusterBuffer);
			CountClusterKernel.setArg(1, *particleBuffer);
			CountClusterKernel.setArg(2, activeClusterBuffer);
			CountClusterKernel.setArg(3, activeParticleBuffer);
			queue.enqueueNDRangeKernel(CountClusterKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();
		}

		{
			//auto timestepTime = physicsProf.timeName("clusterProperties");
			// Medium performance impact (10%)
			UpdateClusterPropsKernel.setArg(0, clusterBuffer);
			UpdateClusterPropsKernel.setArg(1, *particleBuffer);
			UpdateClusterPropsKernel.setArg(2, activeClusterBuffer);
			queue.enqueueNDRangeKernel(UpdateClusterPropsKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			UpdateClusterTransformsKernel.setArg(0, clusterBuffer);
			UpdateClusterTransformsKernel.setArg(1, *particleBuffer);
			UpdateClusterTransformsKernel.setArg(2, activeClusterBuffer);
			queue.enqueueNDRangeKernel(UpdateClusterTransformsKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
			queue.finish();

			//TODO - Self Collisions
		}
		{
			//Negligible performance impact (0.02%)
			//auto timestepTime = physicsProf.timeName("planeInteractions");

			// BounceOutOfPlanes() (Negligible performance drop)
			if (numMovingPlanes > 0)
			{
				MovingPlaneKernel.setArg(0, *particleBuffer);
				MovingPlaneKernel.setArg(1, movingPlaneBuffer);
				MovingPlaneKernel.setArg(2, activeParticleBuffer);
				MovingPlaneKernel.setArg(3, worldParameters);
				MovingPlaneKernel.setArg(4, numMovingPlanes);
				MovingPlaneKernel.setArg(5, elapsedTime);
				queue.enqueueNDRangeKernel(MovingPlaneKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}

			if (numTwistingPlanes > 0)
			{
				TwistingPlaneKernel.setArg(0, *particleBuffer);
				TwistingPlaneKernel.setArg(1, twistingPlaneBuffer);
				TwistingPlaneKernel.setArg(2, activeParticleBuffer);
				TwistingPlaneKernel.setArg(3, numTwistingPlanes);
				TwistingPlaneKernel.setArg(4, elapsedTime);
				queue.enqueueNDRangeKernel(TwistingPlaneKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}

			if (numTiltingPlanes > 0)
			{
				TiltingPlaneKernel.setArg(0, *particleBuffer);
				TiltingPlaneKernel.setArg(1, tiltingPlaneBuffer);
				TiltingPlaneKernel.setArg(2, activeParticleBuffer);
				TiltingPlaneKernel.setArg(3, numTiltingPlanes);
				TiltingPlaneKernel.setArg(4, elapsedTime);
				queue.enqueueNDRangeKernel(TiltingPlaneKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(particles.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
				queue.finish();
			}
		}

		UpdateClusterFpAndComKernel.setArg(0, clusterBuffer);
		UpdateClusterFpAndComKernel.setArg(1, *particleBuffer);
		UpdateClusterFpAndComKernel.setArg(2, activeClusterBuffer);
		queue.enqueueNDRangeKernel(UpdateClusterFpAndComKernel, cl::NullRange, cl::NDRange(clUtils::uSnap(clusters.size(), NUM_ELEMENTS)), cl::NDRange(NUM_ELEMENTS));
		queue.finish();

	}
	catch (cl::Error err)
	{
		std::cout << "Exception thrown running dynamics kernels" << std::endl;
		std::cout << "Error: " << clUtils::getErrorString(err.err()) << " in method: " << err.what() << std::endl;
	}

	//std::cout << elapsedTime << std::endl;
	//cl::finish();
	if (glInterop)
		clEnqueueReleaseGLObjects(queue(), 1, &(*particleBuffer)(), 0, 0, 0);
	queue.finish();
	elapsedTime += worldParameters.dt;
}
