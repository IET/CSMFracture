#include "movingPlane.h"
#include "particle.h"


bool MovingPlane::outside(Particle& particle) const {
   if(particle.restPosition.dot(normal) > offset){
      return true;
   }

   return false;
}

   

void MovingPlane::bounceParticle(Particle& particle, float timeElapsed) const {
   float currentOffset = offset + timeElapsed*velocity;

   if(outside(particle)){
      Eigen::Vector3f tangential = 
         particle.restPosition - (particle.restPosition.dot(normal))*normal;

      particle.position = tangential + currentOffset*normal;
   }
}


void MovingPlane::dragParticle(Particle& particle, float timeElapsed) const {
   if(outside(particle)){
      particle.velocity = velocity*normal;
   }
}

bool MovingPlane::backsideReflectBounceParticle(Particle& particle, float timeElapsed, float epsilon) const {
   //JAL believes this needs to be debug, it causes weird offsetting
  if (outside(particle)) return false;
   float w = (offset + timeElapsed*velocity);

   if (particle.position.dot(normal) > w) {
      particle.position += (epsilon + w - particle.position.dot(normal))*normal;

      //zero velocity in the normalal direction
      particle.velocity -= particle.velocity.dot(normal)*normal;
      particle.velocity *= 0.4f; //friction
	  return true;
   }
   return false;
}
