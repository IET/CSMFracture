#ifndef MOVING_PLANE_H
#define MOVING_PLANE_H
#ifdef __cplusplus
#include <Eigen/Dense>
#endif
#include "particle.h"

struct MovingPlane{
#ifdef __cplusplus
	MovingPlane(Eigen::Vector3f _normal, float _offset, float _velocity)
		:normal(_normal.normalized()), offset(_offset), velocity(_velocity){}
	bool outside(Particle& particle) const;
	void bounceParticle(Particle& particle, float timeElapsed) const;
	void dragParticle(Particle& particle, float timeElapsed) const;
	bool backsideReflectBounceParticle(Particle& particle, float timeElapsed, float epsilon) const;
#endif

#ifdef __cplusplus
	Eigen::Vector3f normal; float pad;
#else
	float3 normal;
#endif
	float offset, velocity;
	float pad1, pad2;
};
#endif //MOVING_PLANE_H