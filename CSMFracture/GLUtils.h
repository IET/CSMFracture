#pragma once

#include "Eigen/Core"

#define _USE_MATH_DEFINES
#include <math.h>

#define DEG_TO_RAD(x) x * (M_PI/180)


Eigen::Matrix4f perspective
(
    float fovy,
	float aspect,
	float zNear,
	float zFar
)
{
    assert(aspect > 0);
    assert(zFar > zNear);

	float radf = DEG_TO_RAD(fovy);

	float tanHalfFovy = tan(radf / 2.0f);

    Eigen::Matrix4f res = Eigen::Matrix4f::Zero();
    res(0,0) = 1.0f / (aspect * tanHalfFovy);
    res(1,1) = 1.0f / (tanHalfFovy);
    res(2,2) = - (zFar + zNear) / (zFar - zNear);
    res(3,2) = - 1.0f;
    res(2,3) = - (2.0f * zFar * zNear) / (zFar - zNear);
    return res;
}

Eigen::Matrix4f lookAt
(
    Eigen::Vector3f const & eye,
    Eigen::Vector3f const & center,
    Eigen::Vector3f const & up
)
{
    Eigen::Vector3f f = (center - eye).normalized();
	Eigen::Vector3f u = up.normalized();
	Eigen::Vector3f s = f.cross(u).normalized();
    u = s.cross(f);

    Eigen::Matrix4f res;
    res <<  s.x(),s.y(),s.z(),-s.dot(eye),
            u.x(),u.y(),u.z(),-u.dot(eye),
            -f.x(),-f.y(),-f.z(),f.dot(eye),
            0.0f, 0.0f, 0.0f, 1.0f;

    return res;
}
