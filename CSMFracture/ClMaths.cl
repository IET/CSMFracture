#ifndef CL_MATHS_H
#define CL_MATHS_H
#include "GPUDataTypes.h"

#define CUBE(x) ((x)*(x)*(x))
#define SQR(x) ((x)*(x))

__constant mat3f mat3fzero = { .data = {{0,0,0}, {0,0,0}, {0,0,0} } };
__constant mat3f mat3fidentity = { .data = { { 1,0,0 },{ 0,1,0 },{ 0,0,1 } } };

inline float norm(float3 v)
{
	return sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}

inline float SqrNorm(float3 v)
{
	return ((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}

void ClearMat3f(mat3f *m)
{
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			m->data[i][j] = 0.0f;
}

inline mat3f Vec3fasDiagonal(float3 v)
{
	mat3f ret = { .data = {{v.x,0,0}, { 0,v.y,0 }, { 0,0,v.z }} };
	return ret;
}

inline float Mat3fDeterminant(const mat3f *m)
{	
	float det = 0.0f;
	for (int i = 0; i < 3; ++i)
		det += m->data[0][i] * (m->data[1][(i + 1) % 3] * m->data[2][(i + 2) % 3] - m->data[1][(i + 2) % 3] * m->data[2][(i + 1) % 3]);

	return det;
}

inline mat3f Mat3fInverse(mat3f m)
{
	float det = Mat3fDeterminant(&m);
	mat3f ret;
	ClearMat3f(&ret);

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			ret.data[j][i] = ((m.data[(i + 1) % 3][(j + 1) % 3] * m.data[(i + 2) % 3][(j + 2) % 3]) - (m.data[(i + 1) % 3][(j + 2) % 3] * m.data[(i + 2) % 3][(j + 1) % 3])) / det;
		}
	}
	return ret;
}

mat3f Mat3fTranspose(const mat3f *m)
{
	mat3f ret;
	ClearMat3f(&ret);
	
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			ret.data[j][i] = m->data[i][j];
		}
	}

	return ret;
}



void MatScalarMult3f_inPlace(mat3f *m, float f)
{
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			m->data[i][j] *= f;
}

void MatMatAdd3f_inPlace(mat3f *a, mat3f *b)
{
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			a->data[i][j] += b->data[i][j];
}

inline mat3f MatMatAdd3f(mat3f a, mat3f b)
{
	mat3f res;
	ClearMat3f(&res);

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			res.data[i][j] = a.data[i][j] + b.data[i][j];

	return res;
}

inline mat3f MatMatMult3f(mat3f a, mat3f b)
{
	mat3f res;
	ClearMat3f(&res);
	
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			for (int k = 0; k < 3; ++k)
			{
				res.data[i][j] += a.data[i][k] * b.data[k][j];
			}
		}
	}
	return res;
}

mat3f MatMatMult3fp(const mat3f *a, const mat3f *b)
{
	mat3f res = mat3fzero;
	
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			for (int k = 0; k < 3; ++k)
				res.data[i][j] += a->data[i][k] * b->data[k][j];
	return res;
}

inline float3 MatVecMult3f(const mat3f *m, float3 v)
{
	return (float3)((m->data[0][0] * v.x) + (m->data[0][1] * v.y) + (m->data[0][2] * v.z),
					(m->data[1][0] * v.x) + (m->data[1][1] * v.y) + (m->data[1][2] * v.z),
					(m->data[2][0] * v.x) + (m->data[2][1] * v.y) + (m->data[2][2] * v.z));
}



inline mat3f MatScalarMult3f(mat3f m, float f)
{
	mat3f res;
	ClearMat3f(&res);

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			res.data[i][j] = m.data[i][j] * f;
	return res;
}

inline mat3f VecOuterProduct(float3 a, float3 b)
{
	mat3f res;
//	float A[3];
//	float B[3];

//	A[0] = a->x;	A[1] = a->y;	A[2] = a->z;
//	B[0] = b->x;	B[1] = b->y;	B[2] = b->z;

	//Unrolled loop should be more efficient
	res.data[0][0] = a.x * b.x;	
	res.data[0][1] = a.x * b.y;	
	res.data[0][2] = a.x * b.z;

	res.data[1][0] = a.y * b.x;
	res.data[1][1] = a.y * b.y;
	res.data[1][2] = a.y * b.z;

	res.data[2][0] = a.z * b.x;
	res.data[2][1] = a.z * b.y;
	res.data[2][2] = a.z * b.z;
/*
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			res.data[i][j] = A[i] * B[j];
		}
	}
*/
	return res;
}
#endif //CL_MATHS_H