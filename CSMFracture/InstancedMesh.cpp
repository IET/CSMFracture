#include "InstancedMesh.h"

InstancedMesh::InstancedMesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> texture, int instances, Eigen::Vector3f *positions, Eigen::Vector4f *colors)
{
	this->instances		= instances;
	this->positions		= positions;
	this->colors		= colors;

	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	// Now that we have all the required data, set the vertex buffers and its attribute pointers.
	this->setupMesh();
}

InstancedMesh::InstancedMesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> texture, std::vector<Particle> &particles)
{
	this->instances = instances;

	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	// Now that we have all the required data, set the vertex buffers and its attribute pointers.
	this->setupMesh(particles);
}

void InstancedMesh::Draw(int numInstances) {
	// Draw mesh
	glBindVertexArray(this->VAO);

	glDrawElementsInstancedARB(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0, numInstances);
	glBindVertexArray(0);
}

void InstancedMesh::Draw() {
	// Draw mesh
	glBindVertexArray(this->VAO);

	glDrawElementsInstancedARB(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0, instances);
	glBindVertexArray(0);
}

void InstancedMesh::Draw(std::vector<Particle> &particles) {
	// Draw mesh
	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->particleBuffer);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particles.size() * sizeof(Particle), &particles[0]);

	//	glBindBuffer(GL_ARRAY_BUFFER, this->CBO);
	//	glBufferSubData(GL_ARRAY_BUFFER, 0, numInstances * sizeof(Eigen::Vector4f), &colors[0]);

	glDrawElementsInstancedARB(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0, particles.size());
	glBindVertexArray(0);
}

void InstancedMesh::setupMesh() {
	// Create buffers/arrays
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);
	glGenBuffers(1, &this->CBO);

	glBindVertexArray(this->VAO);
	// Load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	// A great thing about structs is that their memory layout is sequential for all its items.
	// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a Eigen::Vector3f/2 array which
	// again translates to 3/2 floats which translates to a byte array.
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLint), &this->indices[0], GL_STATIC_DRAW);

	// Set the vertex attribute pointers
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

	glGenBuffers(1, &this->particleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
	glBufferData(GL_ARRAY_BUFFER, instances * sizeof(Eigen::Vector3f), &positions[0], GL_DYNAMIC_DRAW);

	// Set attribute pointers for position (3 * float)
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Eigen::Vector3f), (GLvoid*)0);

	//Setup color buffer
//	glEnableVertexAttribArray(3);
//	glBindBuffer(GL_ARRAY_BUFFER, this->CBO);
//	glBufferData(GL_ARRAY_BUFFER, instances * sizeof(Eigen::Vector4f), &this->colors[0], GL_STREAM_DRAW);
//	glVertexAttribPointer(3, 4, GL_FLOAT, GL_TRUE, sizeof(Eigen::Vector4f), (GLvoid*)0);

	glVertexAttribDivisorARB(0, 0); // particles vertices : always reuse the same  vertices -> 0
	glVertexAttribDivisorARB(1, 1); // positions
	glVertexAttribDivisorARB(2, 0); // Always use the same texcoords
	glVertexAttribDivisorARB(3, 1); // color

	glBindVertexArray(0);

}

void InstancedMesh::setupMesh(std::vector<Particle> &particles) {
	// Create buffers/arrays
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);
	//glGenBuffers(1, &this->CBO);

	glBindVertexArray(this->VAO);
	// Load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	// A great thing about structs is that their memory layout is sequential for all its items.
	// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a Eigen::Vector3f/2 array which
	// again translates to 3/2 floats which translates to a byte array.
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLint), &this->indices[0], GL_STATIC_DRAW);

	// Set the vertex attribute pointers
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

	glGenBuffers(1, &this->particleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
	glBufferData(GL_ARRAY_BUFFER, particles.size() * sizeof(Particle), &particles[0], GL_DYNAMIC_DRAW);

	instances = particles.size();

	int buffSize;
	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &buffSize);
	
	assert(buffSize == (sizeof(Particle) * particles.size()));
	
	// Set attribute pointers for position (3 * float)
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)0);

	//Setup color buffer
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_TRUE, sizeof(Particle), (GLvoid*)offsetof(Particle, color));

	glVertexAttribDivisorARB(0, 0); // particles vertices : always reuse the same  vertices -> 0
	glVertexAttribDivisorARB(1, 1); // positions
	glVertexAttribDivisorARB(2, 0); // Always use the same texcoords
	glVertexAttribDivisorARB(3, 1); // color

	glBindVertexArray(0);

}
InstancedMesh::~InstancedMesh(){
	//glDeleteBuffers(1, &this->VBO);
	//glDeleteBuffers(1, &this->EBO);
	//glDeleteBuffers(4, &this->matrixBuffer);
	//delete[] positions;
	//delete[] colors;
}