#include "mesh.h"
#include "particle.h"
#include <GL/glew.h> // Contains all the necessery OpenGL includes

class InstancedMesh : public Mesh {
public:
	InstancedMesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> texture, int instances, Eigen::Vector3f *positions, Eigen::Vector4f *colors);
	InstancedMesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> texture, std::vector<Particle> &particles);
		
	GLuint particleBuffer;

	void Draw();
	void Draw(int numInstances);
	void Draw(std::vector<Particle> &particles);

	~InstancedMesh();

private:
	int instances;
	GLuint CBO;	//Color buffer object

	void setupMesh();
	void setupMesh(std::vector<Particle> &particles);

	Eigen::Vector3f *positions;
	Eigen::Vector4f *colors;
};