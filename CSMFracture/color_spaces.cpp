#include "color_spaces.h"

HSLColor RGBColor::to_hsl()  {
	float c_max = std::max(r, std::max(g,b));
	float c_min = std::min(r, std::min(g,b));
	float delta = c_max-c_min;

	float l = 0.5f*(c_max+c_min);
	float s = l < 0.5f ? delta/(c_max+c_min) : delta/(2.0f-c_max-c_min);

	float pi_over_3 = (float)acos(-1)/3.0f;
	float h = 0;
	if(r > g && r > b)
		h = pi_over_3*((g-b)/delta);
	else if(g > b)
		h = pi_over_3*((b-r)/delta+2);
	else
		h = pi_over_3*((r-g)/delta+4);
	h = h < 0 ? h+2*(float)acos(-1) : h;
	return HSLColor(h,s,l);
}

RGBColor HSLColor::to_rgb()  {
	float t_1 = l < 0.5f ? l*(1.0f+s) : l+s-l*s;
	float t_2 = 2.0f*l - t_1;
	if(t_2 < 0 || t_2 > 1)
		std::cout << "bad t_2!" << std::endl;

	float norm_h = h/(2.0f*(float)acos(-1));
	float t_r = norm_h+(1.0f/3.0f);
	t_r = t_r > 1 ? t_r-1 : t_r;
	float t_g = norm_h;
	float t_b = norm_h-(1.0f/3.0f);
	t_b = t_b < 0 ? t_b+1 : t_b;

	RGBColor rgb;
	if(t_r < 1.0f/6.0f)
		rgb.r = t_2 + (t_1-t_2)*6.0f*t_r;
	else if(t_r < 1.0f/2.0f)
		rgb.r = t_1;
	else if(t_r < 2.0f/3.0f)
		rgb.r = t_2 + (t_1-t_2)*6.0f*(2.0f/3.0f-t_r);
	else
		rgb.r = t_2;

	if(t_g < 1.0f/6.0f)
		rgb.g = t_2 + (t_1-t_2)*6.0f*t_g;
	else if(t_g < 1.0f/2.0f)
		rgb.g = t_1;
	else if(t_g < 2.0f/3.0f)
		rgb.g = t_2 + (t_1-t_2)*6.0f*(2.0f/3.0f-t_g);
	else
		rgb.g = t_2;

	if(t_b < 1.0f/6.0f)
		rgb.b = t_2 + (t_1-t_2)*6.0f*t_b;
	else if(t_b < 1.0f/2.0f)
		rgb.b = t_1;
	else if(t_b < 2.0f/3.0f)
		rgb.b = t_2 + (t_1-t_2)*6.0f*(2.0f/3.0f-t_b);
	else
		rgb.b = t_2;

	return rgb;
}


