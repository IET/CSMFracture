#include "clustering.h"
#include "range.hpp"
#include "utils.h"

#include "accelerationGrid.h"
#include "profiler.hpp"
#include <random>
#include <iostream>
#include <algorithm>

inline float cube(float x) { return x*x*x; }

std::vector<Cluster> makeRandomClusters(std::vector<Particle>& particles, float neighborRadius) {
	// std::random_device rd; unused since we're using the default seed always
	std::mt19937 gen(std::mt19937::default_seed);
	
	std::vector<Cluster> clusters;

	auto r = benlib::range(particles.size());
	auto lonelyParticles = std::vector<size_t>(r.begin(), r.end());

	AccelerationGrid<Particle, RestPositionGetter> restPositionGrid;
	restPositionGrid.numBuckets = 16; //TODO tune me, 512 buckets now... seems reasonable?
	restPositionGrid.updateGrid(particles);
	
	for (auto& p : particles) 
	{ 
		p.numClusters = 0; 
		p.totalweight = 0.0f; 
	}


	while (!lonelyParticles.empty()) {
		std::uniform_int_distribution<> randomDist(0, lonelyParticles.size() - 1);
		int r = randomDist(gen);
		//auto currentParticle = lonelyParticles.back();
		//lonelyParticles.pop_back();
		auto currentParticle = lonelyParticles[r];
		lonelyParticles.erase(lonelyParticles.begin() + r);
		Cluster c;
		c.restCom = particles[currentParticle].restPosition;
		std::vector<int> neighbors = restPositionGrid.getNearestNeighbors(particles, c.restCom, neighborRadius);
		c.numMembers = neighbors.size();
		float w = 1.0f;

		for (unsigned int i = 0; i<neighbors.size(); i++) {
			auto n = neighbors[i];
			Particle &p = particles[n];
			//p.numClusters++;
			p.totalweight += w;
			p.clusters[p.numClusters].index = clusters.size();
			p.clusters[p.numClusters].weight = w;
			++p.numClusters;
			c.members[i] = { n, w };
		}
		clusters.push_back(c);

		//std::cout<<lonelyParticles.size()<<std::endl;
		auto it = std::remove_if(lonelyParticles.begin(), lonelyParticles.end(),
			[&neighbors](size_t n) {
			//neighbors aren't lonely anymore
			return utils::containsValue(neighbors, n);
		});
		lonelyParticles.erase(it, lonelyParticles.end());
		//std::cout<<lonelyParticles.size()<<std::endl;
	}

	for (auto& c : clusters) 
		c.cg.init(c.restCom, neighborRadius);

	for (auto& c : clusters) {
		c.mass = 0.0f;
		c.restCom = Eigen::Vector3f::Zero();

		for (int i = 0; i < c.numMembers; ++i) {
			auto &member = c.members[i];
			assert(member.index > -1);
			auto &p = particles[member.index];
			float w = member.weight / p.totalweight;
			c.restCom += w * p.mass * p.position;
			c.mass += w * p.mass;
		}

		c.restCom /= c.mass;
	}
	return clusters;
}

std::vector<Cluster> makeClusters(std::vector<Particle>& particles,
	const ClusteringParams& params) {

	std::cout << "using clusteringAlgorithm " << params.clusteringAlgorithm << std::endl;
	std::vector<Cluster> clusters;

	bool converged;
	int iters = 0;
	float convergenceThreshold = 1e-6f * params.sqrNeighborRadius; // 0.1f% motion allowed
																   //std::random_device rd; //unused
	std::mt19937 gen(std::mt19937::default_seed);
	std::uniform_int_distribution<> randomDist(0, particles.size() - 1);
	std::vector<bool> picked(particles.size(), false);


	if (params.clusteringAlgorithm == 2) {
		return makeRandomClusters(particles, params.neighborRadius);
	}

	AccelerationGrid<Particle, RestPositionGetter> restPositionGrid;
	restPositionGrid.numBuckets = 16; //TODO tune me, 512 buckets now... seems reasonable?
	restPositionGrid.updateGrid(particles);

	std::cout << "nClusters: " << params.nClusters << std::endl;
	std::cout << "k means initialization...";
	if (params.clusteringAlgorithm < 2) { //kmeans or fuzzy cmeans
		//seed nClusters clusters
		assert(particles.size() >= params.nClusters);
		clusters.reserve(params.nClusters);
		
		while (clusters.size() < params.nClusters) {
			Cluster c;
			int r = randomDist(gen);
			if (picked[r]) continue;
			picked[r] = true;
			c.restCom = particles[r].restPosition;
			clusters.push_back(c);
		}

		converged = false;
		
		while (!converged) {
			iters++;
			converged = true;

			for (auto& c : clusters)
				c.numMembers = 0;

			for (int i = 0; i<particles.size(); i++) {
				auto& p = particles[i];
				const auto& pRest = p.restPosition;

				//closest to pRest
				auto bestPair = utils::minProjectedElement(
					clusters,
					[&pRest](const Cluster& c) {
					return (c.restCom - pRest).squaredNorm();
				});
				assert(bestPair.first->numMembers < MAX_MEMBERS);
				bestPair.first->members[bestPair.first->numMembers++] = ClusterMember( i, 1.0f );
				int bestCluster = std::distance(clusters.begin(), bestPair.first);

				if (p.numClusters != bestCluster) converged = false;
				p.numClusters = bestCluster;
				p.totalweight = 1.0f;
			}

			for (auto& c : clusters) {
				c.mass = 0.0f;
				c.restCom = Eigen::Vector3f::Zero();
				for (int i = 0; i < c.numMembers; ++i) {
					auto &member = c.members[i];
					if (member.index < 0)
						assert(member.index > -1 && member.weight > 0.0f);
					auto &p = particles[member.index];
					float w = member.weight / p.totalweight;
					c.restCom += w * p.mass * p.position;
					c.mass += w * p.mass;
				}
				c.restCom /= c.mass;
			}
		} //end while not converged
	}

	std::cout << "finished" << std::endl;

	//fill in weights to clusters and particles
	if (params.clusteringAlgorithm == 1) {
		for (auto& p : particles)
		{
			p.numClusters = 0;
			p.totalweight = 0.0f;
		}

		for (auto j = 0; j<clusters.size(); j++) {
			auto &c = clusters[j];
			std::vector<int> neighbors =
				restPositionGrid.getNearestNeighbors(particles, c.restCom, params.neighborRadius);

			c.numMembers = neighbors.size();

			for (unsigned int i = 0; i<neighbors.size(); i++) {
				auto n = neighbors[i];
				Particle &p = particles[n];
				float w = params.kernel(c.restCom - p.restPosition);
				c.members[i] = { n, w };
				p.totalweight += w;
				p.clusters[p.numClusters].index = j;
				p.clusters[p.numClusters].weight = w;
				++(p.numClusters);
				p.numClusters++;
			}
		}

		for (auto& c : clusters) {
			c.cg.init(c.restCom, params.neighborRadius);
			c.mass = 0.0f;
			c.restCom = Eigen::Vector3f::Zero();

			for (int i = 0; i < c.numMembers; ++i) {
				auto &member = c.members[i];
				assert(member.index > -1);
				auto &p = particles[member.index];
				float w = member.weight / p.totalweight;
				c.restCom += w * p.mass * p.position;
				c.mass += w * p.mass;
			}

			c.restCom /= c.mass;
		}

		for (auto& p : particles) {
			if (p.numClusters == 0) {
				std::cout << "Particle has no cluster" << std::endl;
				return{}; //no clusters
			}
		}
	}

	// fuzzy c-means loop
	if (params.clusteringAlgorithm < 1) {
		std::cout << "starting c means loop..." << std::endl;
		iters = 0;


		while ((!converged || iters < 5) && iters < params.clusterItersMax) {
			converged = true;
			iters++;

			if (iters % 100 == 0) {
				std::cout << "starting cmeans iteration " << iters << std::endl;
			}

			for (auto& p : particles) {
				//p.clusters.clear();
				p.numClusters = 0;
				p.totalweight = 0.0f;
			}

			for (auto j = 0; j<clusters.size(); j++) {
				Cluster &c = clusters[j];
				int oldMembers = c.numMembers;
				std::vector<int> neighbors =
					restPositionGrid.getNearestNeighbors(particles, c.restCom, params.neighborRadius);
				c.numMembers = neighbors.size();

				for (unsigned int i = 0; i<neighbors.size(); i++) {
					auto n = neighbors[i];
					Particle &p = particles[n];
					float w = params.kernel(c.restCom - p.restPosition);
					c.members[i] = { n, w };
					p.totalweight += w;
					p.clusters[p.numClusters].index = j;
					p.clusters[p.numClusters].weight = w;
					++p.numClusters;
				}

				if (oldMembers != c.numMembers) {
					converged = false;
				}
			}

			//stick particles not within neighborRadius of a cluster in the nearest cluster
			for (int i = 0; i < particles.size(); i++) {
				auto& p = particles[i];
				if (p.numClusters > 0) continue;

				const auto& pRest = p.restPosition;

				auto bestPair = utils::minProjectedElement(clusters,
					[&pRest](const Cluster& c) { return (float)(c.restCom - pRest).squaredNorm(); });
				bestPair.first->members[bestPair.first->numMembers++] = ClusterMember( static_cast<int>(i), params.blackhole );

				p.totalweight = 1.0f;
				converged = false;
			}

			//compute the FCM weights x_i_c = 1/(sum_d_inclusters(||x_i - x_c||/||x_i - x_d||)^2/(m-1)
			if (params.clusterKernel == 4) {

				//almost certain this doesn't need to be nested so deep
				for (int i = 0; i < particles.size(); i++) {
					auto& p = particles[i];
					p.totalweight = 0;

					//members.second holds distances now
					std::vector<float> updatedWeights(p.numClusters);


					for (int cIndex = 0; cIndex = p.numClusters; cIndex++) {
						float sum = 0;
						auto& c = clusters[cIndex];
						std::vector<ClusterMember> memberVector(c.members, c.members + c.numMembers);

						auto pInC = utils::findIfInContainer(memberVector, [i](const ClusterMember& member) {
							return member.index == i; });
						float xToC = pInC->weight;

						//only the clusters that p is part of
						for (int k = 0; i < p.numClusters; ++k) {
							int dIndex = p.clusters[k].index;
							auto& d = clusters[dIndex];
							std::vector<ClusterMember> memberVector1(d.members, d.members + d.numMembers);

							auto pInD = utils::findIfInContainer(memberVector1, [i](const ClusterMember& member) {
								return member.index == i; });

							//it'd better be in there...
							assert(pInD != memberVector1.end());
							sum += pow(xToC / pInD->weight, 2.0f / params.kernelWeight - 1);
						}

						updatedWeights[cIndex] = 1.0f / sum;
					}

					//now overwrite distances in members with weights
					for (int k = 0; k < p.numClusters; ++k) {
						int cIndex = p.clusters[k].index;
						auto& c = clusters[cIndex];
						std::vector<ClusterMember> memberVector(c.members, c.members + c.numMembers);

						auto pInC = utils::findIfInContainer(memberVector, [i](const ClusterMember& member) {
							return member.index == i; });
						assert(pInC != memberVector.end());
						pInC->weight = updatedWeights[cIndex];
					}
				}

				for (auto &c : clusters)
				{
					for (int i = 0; i < c.numMembers; ++i)
					{
						auto &member = c.members[i];
						assert(member.index > -1);
						particles[member.index].totalweight += member.weight;
					}
				}
			}

			//compute cluster COMs
			for (auto& c : clusters) {
				c.worldCom = c.restCom; //store the last rest COM here for now
				c.mass = 0.0f;
				c.restCom = Eigen::Vector3f::Zero();

				for (int i = 0; i < c.numMembers; ++i) {
					auto &member = c.members[i];
					//assert(member.index > -1);
					if (member.index < 0)
					{
						std::cout << "Member " << i << "has negative index" << std::endl;
						assert(0);
					}
					auto &p = particles[member.index];
					float w = member.weight / p.totalweight;
					c.restCom += w * p.mass * p.position;
					c.mass += w * p.mass;
				}
				c.restCom /= c.mass;
				if ((c.restCom - c.worldCom).squaredNorm() > convergenceThreshold) {
					converged = false;
				}
			}

		}
		for (auto& c : clusters) c.cg.init(c.restCom, params.neighborRadius);
		std::cout << "Fuzzy c-means clustering ran " << iters << " iterations." << std::endl;
	}

	for (auto& p : particles) {
		if (p.numClusters == 0) {
			std::cout << "Particle has no cluster" << std::endl;
			return{}; //empty set of clusters means clustering didn't work
		}
	}

	if (!converged) return{}; //no clusters means it didn't work
	std::cout << "numClusters: " << clusters.size() << std::endl;

	return clusters;
}


std::vector<Cluster> iterateMakeClusters(std::vector<Particle>& particles, ClusteringParams& params) {
	
	params.sqrNeighborRadius = params.neighborRadius * params.neighborRadius;
	params.poly6norm = 315.0f / (64.0f * M_PI * cube(cube(params.neighborRadius)));
	std::vector<Cluster> clusters;

	while ((clusters = makeClusters(particles, params)).empty()) {
		params.nClusters = std::ceil(1.25f*params.nClusters);
		params.neighborRadius *= 1.25f;
		if (params.nClusters > params.nClustersMax &&
			params.neighborRadius > params.neighborRadiusMax) {
			std::cout << "exceeded limits, nclusters: "
				<< params.nClusters << " " << params.nClustersMax << std::endl
				<< "neighborhood radius: " << params.neighborRadius
				<< " " << params.neighborRadiusMax << std::endl;
			exit(0);
		}
		params.nClusters = std::min(params.nClusters, params.nClustersMax);
		params.neighborRadius = std::min(params.neighborRadius, params.neighborRadiusMax);
		params.sqrNeighborRadius = params.neighborRadius * params.neighborRadius;
		params.poly6norm = 315.0f / (64.0f * M_PI * cube(cube(params.neighborRadius)));
		std::cout << "trying clustering again with nClusters = "
			<< params.nClusters << " neighborRadius = "
			<< params.neighborRadius << std::endl;
	}
	std::cout << "nClusters = " << params.nClusters << std::endl;
	return clusters;

}


float ClusteringParams::kernel(const Eigen::Vector3f &x) const {
	switch (clusterKernel) {
	case 0: // 1 / r^2
		return 1.0f / ((float)x.squaredNorm() + 1e-4f);
	case 1: // constant weight
		return 1.0f;
	case 2: // poly 6
		return poly6norm * cube(sqrNeighborRadius - (float)x.squaredNorm());
	case 3: // blend
		return poly6norm * cube(sqrNeighborRadius - (float)x.squaredNorm()) + kernelWeight;
	case 4: // fuzzy c-means
		return x.norm();
	default:
		return 1.0f / ((float)x.squaredNorm() + 1e-4f);
	}
}
