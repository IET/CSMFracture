// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#if defined _WIN32 || defined _WIN64
//Use STL min/max instead of windows
#define NOMINMAX
//Supress errors about printf/sprintf security
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include "targetver.h"
#include <memory>
#include <stdio.h>
#include <tchar.h>

#endif // _WIN32

// TODO: reference additional headers your program requires here
