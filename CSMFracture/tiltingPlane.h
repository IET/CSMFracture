#ifndef TILTING_PLANE_H
#define TILTING_PLANE_H
#ifdef __cplusplus
#include <Eigen/Dense>
#endif
#include "particle.h"

struct TiltingPlane{
#ifdef __cplusplus
	TiltingPlane(Eigen::Vector3f _normal, Eigen::Vector3f _tilt, float _offset, float _angularVelocity, float _width, float _lifetime)
		:normal(_normal.normalized()), tilt(_tilt.normalized()), offset(_offset), angularVelocity(_angularVelocity), width(_width), lifetime(_lifetime){}
	bool outside(Particle& particle) const;
	void tiltParticle(Particle& particle, float timeElapsed) const;
#endif
#ifdef __cplusplus
	Eigen::Vector3f normal; float pad;
	Eigen::Vector3f tilt; float pad2;
#else
	float3 normal;
	float3 tilt;
#endif
	float offset;
	float angularVelocity;
	float width;
	float lifetime;
};
#endif //TILTING_PLANE_H
