#ifndef EIGEN_SVD_CL
#define EIGEN_SVD_CL

#include "GPUDataTypes.h"
#include "ClMaths.cl"

#define FLT_DENORM_MIN 1.4013e-45
//2e-149
#define SIGN(a,b) ((b) > 0.0 ? fabs(a) : - fabs(a))
#define SQR(x) ((x)*(x))

typedef struct {
	float s, c;
} JacobiRotation;

void applySvdRotationLeftMat3f( mat3f *m, int p, int q, const JacobiRotation *j);
void applySvdRotationRightMat3f(mat3f *m, int p, int q, const JacobiRotation *j);

bool precondition2x2(mat3f *work_matrix, int p, int q, mat3f *u, mat3f *v, float* maxDiag)
{
	float precision = FLT_EPSILON * 2;
	float considerAsZero = FLT_MIN * 2;
	
	JacobiRotation rot;
	float n = sqrt(SQR(work_matrix->data[p][p]) + SQR(work_matrix->data[q][p]));

	//TODO - Determine if this is necessary

	if (n == 0.0f)
	{
		//Ensure 1st column is 0
		work_matrix->data[p][p] = work_matrix->data[q][p] = 0.0f; 
		
		//if imaginary part of work_matrix[p][q] > 0 ...

		//if imaginary part of work_matrix[q][q] > 0 ...
	}
	else
	{
		rot.c = work_matrix->data[p][p] / n;
		rot.s = work_matrix->data[q][p] / n;

		applySvdRotationLeftMat3f(work_matrix, p, q, &rot);

		JacobiRotation tmp = rot;
		tmp.s *= -1;
		applySvdRotationRightMat3f(u, p, q, &tmp);

		// If imaginary part of work_matrix[p][q] > 0 ...

		//if imaginary part of work_matrix[q][q] > 0 ...
	}

	//Update largest diagonal entry
	*maxDiag  = max(*maxDiag, max(fabs(work_matrix->data[p][p]), fabs(work_matrix->data[q][q])));

	//check if 2x2 block is already diagonal
	float threshold = max(considerAsZero, precision * (*maxDiag));
	return fabs(work_matrix->data[p][q]) > threshold || fabs(work_matrix->data[q][p]) > threshold;
}

void applySvdRotationLeftMat2f(mat2f *m, int p, int q, const JacobiRotation *j)
{
//	float2 x = (float2)(m->data[p][0], m->data[p][1]);
//	float2 y = (float2)(m->data[q][0], m->data[q][1]);

    float c = j->c;
    float s = j->s;

    //TODO - Precision? Should these be exact equality?
    if (c == 1 && s == 0)
   		return;

	float xi = m->data[p][0];
	float yi = m->data[q][0];
	m->data[p][0] =  c * xi + s * yi;
	m->data[q][0] = -s * xi + c * yi;

	xi = m->data[p][1];
	yi = m->data[q][1];
	m->data[p][1] =  c * xi + s * yi;
	m->data[q][1] = -s * xi + c * yi;

//	m->data[p][0] = x.x; m->data[p][1] = x.y;
//	m->data[q][0] = y.x; m->data[q][1] = y.y;
}

void applySvdRotationLeftMat3f(mat3f *m, int p, int q, const JacobiRotation *j)
{    
//	float3 x = (float3)(m->data[p][0], m->data[p][1], m->data[p][2]);
//	float3 y = (float3)(m->data[q][0], m->data[q][1], m->data[q][2]);

    float c = j->c;
    float s = j->s;

    //TODO - Precision? Should these be exact equality?
    if (c == 1 && s == 0)
   		return;

	float xi = m->data[p][0];
	float yi = m->data[q][0];
	m->data[p][0] =  c * xi + s * yi;
	m->data[q][0] = -s * xi + c * yi;

	xi = m->data[p][1];
	yi = m->data[q][1];
	m->data[p][1] =  c * xi + s * yi;
	m->data[q][1] = -s * xi + c * yi;

	xi = m->data[p][2];
	yi = m->data[q][2];
	m->data[p][2] =  c * xi + s * yi;
	m->data[q][2] = -s * xi + c * yi;

//	m->data[p][0] = x.x; m->data[p][1] = x.y; m->data[p][2] = x.z;
//	m->data[q][0] = y.x; m->data[q][1] = y.y; m->data[q][2] = y.z;	
}

void applySvdRotationRightMat3f(mat3f *m, int p, int q, const JacobiRotation *j)
{    
//	float3 x = (float3)(m->data[0][p], m->data[1][p], m->data[2][p]);
//	float3 y = (float3)(m->data[0][q], m->data[1][q], m->data[2][q]);

	//Transpose j
    float c =  j->c;
    float s = -j->s;

    //TODO - Precision? Should these be exact equality?
    if (c == 1 && s == 0)
   		return;
   	
	float xi = m->data[0][p];
	float yi = m->data[0][q];
	m->data[0][p] =  c * xi + s * yi;
	m->data[0][q] = -s * xi + c * yi;

	xi = m->data[1][p];
	yi = m->data[1][q];
	m->data[1][p] =  c * xi + s * yi;
	m->data[1][q] = -s * xi + c * yi;

	xi = m->data[2][p];
	yi = m->data[2][q];
	m->data[2][p] =  c * xi + s * yi;
	m->data[2][q] = -s * xi + c * yi;

//	m->data[0][p] = x.x; m->data[1][p] = x.y; m->data[2][p] = x.z;
//	m->data[0][q] = y.x; m->data[1][q] = y.y; m->data[2][q] = y.z;	
}

bool makeJacobi(JacobiRotation *j,const mat2f *m, int p, int q)
{
//	float x = m->data[p][p];
//	float y = m->data[p][q];
//	float z = m->data[q][q];

	float3 v = (float3)(m->data[p][p], m->data[p][q], m->data[q][q]);

	if (v.y == 0)
	{
		j->c = 1.0f;
		j->s = 0.0f;
		return false;
	}	
	else
	{
		float tau = (v.x - v.z) / (2 * fabs(v.y));
		float w = sqrt(SQR(tau) + 1.0f);
		float t;

		if (tau > 0.0f)
		{
			t = 1 / (tau + w);
		}
		else
		{
			t = 1 / (tau - w);
		}
		float n = 1 / sqrt(SQR(t) + 1);
		j->s = - sign(t) * (v.y / fabs(v.y)) * fabs(t) * n;
		j->c = n;
		return true;
	}
}

inline JacobiRotation jacobiMult(const JacobiRotation *j1, const JacobiRotation *j2)
{
	JacobiRotation ret;

	ret.c = j1->c * j2->c - j1->s * j2->s;
	ret.s = j1->c * j2->s + j1->s * j2->c;

	return ret;
}

void JacobiSVD2x2(const mat3f *matrix, int p, int q, JacobiRotation *left, JacobiRotation *right)
{
	mat2f m;

	m.data[0][0] = matrix->data[p][p]; 
	m.data[0][1] = matrix->data[p][q]; 
	m.data[1][0] = matrix->data[q][p];
	m.data[1][1] = matrix->data[q][q];

	JacobiRotation rot1;

	float t = m.data[0][0] + m.data[1][1];
	float d = m.data[1][0] - m.data[0][1];

	if (d == 0.0f)
	{
		rot1.s = 0.0f;
		rot1.c = 1;
	}
	else
	{
		// If d!=0, then t/d cannot overflow because the magnitude of the
  		// entries forming d are not too small compared to the ones forming t.
		float u = t/d;
		float tmp = sqrt(1.0f + SQR(u));

		rot1.s = 1.0f / tmp;
		rot1.c = u / tmp;
	}

	applySvdRotationLeftMat2f(&m,0, 1, &rot1);

	makeJacobi(right, &m, 0, 1);

	//Calculate right transpose 
	JacobiRotation tmp = *right;
	tmp.s *= -1.0f;

	*left = jacobiMult(&rot1, &tmp);
}


int svdcmp(const mat3f *a, float3 *w, mat3f *u, mat3f *v) {
	float precision = FLT_EPSILON * 4;
	float considerAsZero = FLT_DENORM_MIN * 4;

	vector3f sigma;

	int nCols, nRows;
	nCols = nRows = 3;
	int diagonalSize = min(nCols, nRows);
	float scale = 0.0f;

	mat3f work_matrix = *a;

	*u = mat3fidentity;
	*v = mat3fidentity;

	for (int i = 0; i < nRows; ++i)
		for (int j = 0; j < nCols; ++j)
			scale = max(scale, fabs(work_matrix.data[i][j]));
	
	if (scale == 0.0f)
		scale = 1.0f;

	for (int i = 0; i < nRows; ++i)
		for (int j = 0; j < nCols; ++j)
			work_matrix.data[i][j] /= scale;

	float maxDiagonalEntry = FLT_MIN;
	for (int i = 0; i < diagonalSize; ++i)
		maxDiagonalEntry = max(maxDiagonalEntry, fabs(work_matrix.data[i][i]));

	bool finished = false;
	//Perform main jacobi iteration
	while (!finished)
	{
		finished = true;

		for (int i = 1; i < diagonalSize; ++i)
		{
			for (int j = 0; j < i; ++j)
			{
				float threshold = max(considerAsZero, precision * maxDiagonalEntry);

				if (fabs(work_matrix.data[i][j]) > threshold ||
					fabs(work_matrix.data[j][i]) > threshold)
				{
					finished = false;

					if (precondition2x2(&work_matrix, i, j, u, v, &maxDiagonalEntry))
					{
//						printf("Performing Jacobi Rotation\n");
						JacobiRotation left, right;
						JacobiSVD2x2(&work_matrix, i, j, &left, &right);

						//Apply Rotation to Work Matrix 
						applySvdRotationLeftMat3f( &work_matrix, i, j, &left);
						applySvdRotationRightMat3f(&work_matrix, i, j, &right);

						// if (ComputeU)
						JacobiRotation tmp;	// tmp = left transpose
						tmp.c = left.c; tmp.s = -left.s;
						applySvdRotationRightMat3f(u, i, j, &tmp);

						applySvdRotationRightMat3f(v, i, j, &right);

						//Recompute maximum diagonal element
						for (int i = 0; i < diagonalSize; ++i)
							maxDiagonalEntry = max( maxDiagonalEntry, max(fabs(work_matrix.data[i][i]), fabs(work_matrix.data[j][j])) );
					}
				}
			}
		}
	}

	// Store absolute values of diagonal elements of work matrix as singular values
	for (int i = 0; i < diagonalSize; ++i)
	{
		float n = fabs(work_matrix.data[i][i]);
		sigma.a[i] = n;

		if (n != 0)
		{
			u->data[0][i] *= work_matrix.data[i][i] / n;
			u->data[1][i] *= work_matrix.data[i][i] / n;
			u->data[2][i] *= work_matrix.data[i][i] / n;
		}
	}	

	int nonZeroSingularValues = diagonalSize;

	//Sort singular values in descending order and compute number of non-zero values
	for (int i = 0; i < diagonalSize; ++i)
	{
		int pos; 
		float maxCoeff = FLT_MIN;

		for (int j = 0; j < (diagonalSize - i); ++j)
		{
			int index = diagonalSize - j - 1;
			if (sigma.a[index] > maxCoeff)
			{
				pos = index;
				maxCoeff = sigma.a[index];
			}
		}

		pos -= i;

		//TODO - Should this be exact equality?
		if (maxCoeff == 0.0f)
		{
			nonZeroSingularValues = i;
			break;
		}

		if (pos) // not equal to zero
		{
			//Swap values
			pos += i;
			float tmp = sigma.a[i];
			sigma.a[i] = sigma.a[pos];
			sigma.a[pos] = tmp;
			
			float3 tmpV = (float3)(u->data[0][pos], u->data[1][pos], u->data[2][pos]); 
			u->data[0][pos] = u->data[0][i]; u->data[1][pos] = u->data[1][i]; u->data[2][pos] = u->data[2][i];
			u->data[0][i] = tmpV.x; u->data[1][i] = tmpV.y; u->data[2][i] = tmpV.z;

			tmpV = (float3)(v->data[0][pos], v->data[1][pos], v->data[2][pos]);
			v->data[0][pos] = v->data[0][i]; v->data[1][pos] = v->data[1][i]; v->data[2][pos] = v->data[2][i];
			v->data[0][i] = tmpV.x; v->data[1][i] = tmpV.y; v->data[2][i] = tmpV.z;
		}
	}
	
	//(*u) = Mat3fTranspose(u);
	//(*v) = Mat3fTranspose(v);

	sigma.v *= scale;
	*w = sigma.v;
	return(0);
}

#endif // EIGEN_SVD_CL