#pragma once

#include "particle.h"
#include "ClusteringParams.h"

struct RestPositionGetter{
	Eigen::Vector3f operator()(const Particle& p) const{
		return p.restPosition;
	}
};

//params will change if clustering fails
std::vector<Cluster> iterateMakeClusters(std::vector<Particle>& particles,  ClusteringParams& params);

std::vector<Cluster> makeClusters(std::vector<Particle>& particles, const ClusteringParams& params);

//Will mody particleSet in place to set weights, and cluster
std::vector<Cluster> makeRandomClusters(std::vector<Particle>& particles, float neighborRadius);
