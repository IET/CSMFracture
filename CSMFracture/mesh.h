//Courtesy of http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes

#include <Eigen/Eigen>
#include <Eigen/Dense>

#include <assimp/Importer.hpp>

#include <stdlib.h> //rand
#include <time.h> 

#include "Shader.hpp"

struct Vertex {
	// Position
	Eigen::Vector3f Position;
	// Normal
	Eigen::Vector3f Normal;
	// TexCoords
	Eigen::Vector2f TexCoords;
	// Tangent
	Eigen::Vector3f Tangent;
	// Bitangent
	Eigen::Vector3f Bitangent;
};

struct Texture {
	GLuint id;
	string type;
	aiString path;
};

class Mesh {
public:
	/*  Mesh Data  */
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;
	GLuint VAO;

	/*  Functions  */
	// Constructors
	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);

	// Render the mesh
	void Draw(Shader* shader);


	~Mesh();

private:
	bool		isInstanced;
	int			instances;

	/*  Functions    */
	// Initializes all the buffer objects/arrays
	void setupMesh();

protected:
	Mesh();	//TODO - Implement some kind of stub
public:
	/*  Render data  */
	GLuint VBO, EBO;

};



