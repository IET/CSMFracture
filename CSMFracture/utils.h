#pragma once

#include <Eigen/Dense>
#include <vector>
#include <utility>

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#endif //_WIN32

#ifdef __APPLE__
//why, apple?   why????
#include <OpenGL/glu.h>
#else
#include <gl/glu.h>
#endif


namespace utils{

  //R, S
  /*
	Replacing this with something faster is very likely to give you a big speed boost
   */
	inline std::pair<Eigen::Matrix3f, Eigen::Matrix3f> polarDecomp(const Eigen::Matrix3f& A){
	
		Eigen::JacobiSVD<Eigen::Matrix3f> solver(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
	
		Eigen::Matrix3f U = solver.matrixU();
		Eigen::Matrix3f V = solver.matrixV();
		Eigen::Vector3f sigma = solver.singularValues();
	
		Eigen::Matrix3f Q = U*V.transpose();
//		std::cout << "Q: " << std::endl << Q << std::endl;
		if(Q.determinant() < 0)
		{
			sigma(2) *= -1;
			V.col(2) *= -1;
			Q = U*V.transpose();
//			std::cout << "V in Polar Decomposition: " << std::endl << V << std::endl;
		}
		
		return std::make_pair(Q, V*sigma.asDiagonal()*V.transpose());
	}

	
	inline void drawSphere(float r, int lats, int longs) {
		int i, j;
		for(i = 1; i <= lats; i++) {
			float lat0 = M_PI * (-0.5f + (float) (i - 1) / lats);
			float z0  = sin(lat0);
			float zr0 =  cos(lat0);
    
			float lat1 = M_PI * (-0.5f + (float) i / lats);
			float z1 = sin(lat1);
			float zr1 = cos(lat1);
    
			glBegin(GL_QUAD_STRIP);
			for(j = 0; j <= longs; j++) {
				float lng = 2 * M_PI * (float) j / longs;
				float x = cos(lng);
				float y = sin(lng);
    
				glNormal3f(x * zr1, y * zr1, z1);
				glVertex3f(r*x * zr1, r*y * zr1, r*z1);
				glNormal3f(x * zr0, y * zr0, z0);
				glVertex3f(r*x * zr0, r*y * zr0, r*z0);
			}
			glEnd();
		}
	}

	inline void drawClippedSphere(float r, int lats, int longs,
		const Eigen::Vector3f& center,
		const std::vector<std::pair<Eigen::Vector3f, float> >& planes) {
		
		int i, j;
		for(i = 1; i <= lats; i++) {
			float lat0 = M_PI * (-0.5f + (float) (i - 1) / lats);
			float z0  = sin(lat0);
			float zr0 =  cos(lat0);

			float lat1 = M_PI * (-0.5f + (float) i / lats);
			float z1 = sin(lat1);
			float zr1 = cos(lat1);

			glBegin(GL_QUADS);

			for(j = 0; j < longs; j++) {
				float lng0 = 2 * M_PI * (float) j / longs;
				float x0 = cos(lng0);
				float y0 = sin(lng0);
				float lng1 = 2 * M_PI * ((float) (j+1)) / longs;
				float x1 = cos(lng1);
				float y1 = sin(lng1);

				Eigen::Vector3f n0(x0 * zr1, y0 * zr1, z1);
				Eigen::Vector3f n1(x0 * zr0, y0 * zr0, z0);
				Eigen::Vector3f n2(x1 * zr0, y1 * zr0, z0);
				Eigen::Vector3f n3(x1 * zr1, y1 * zr1, z1);

				Eigen::Vector3f v0 = r*n0+center;
				Eigen::Vector3f v1 = r*n1+center;
				Eigen::Vector3f v2 = r*n2+center;
				Eigen::Vector3f v3 = r*n3+center;

				//always draw if there are no planes
				bool draw = true;

				//if there are planes, check to make sure we're on the right side
				for (int p=0; p<planes.size(); p++) {
					Eigen::Vector3f norm = planes[p].first;
					float offset = planes[p].second;

					if (norm.dot(v0) > -offset &&
						norm.dot(v1) > -offset &&
						norm.dot(v2) > -offset &&
						norm.dot(v3) > -offset) {
					
						draw = false;
					}
				}

				if (draw) {
					glNormal3f(n0(0),n0(1),n0(2));
					glVertex3f(v0(0),v0(1),v0(2));
					glNormal3f(n1(0),n1(1),n1(2));
					glVertex3f(v1(0),v1(1),v1(2));
					glNormal3f(n2(0),n2(1),n2(2));
					glVertex3f(v2(0),v2(1),v2(2));
					glNormal3f(n3(0),n3(1),n3(2));
					glVertex3f(v3(0),v3(1),v3(2));
				}
			}

			glEnd();
		}
	}


	inline std::pair<Eigen::Vector3f, Eigen::Vector3f> getPlaneTangents(const Eigen::Vector3f& normal){

		const Eigen::Vector3f xVector{1.0f, 0.0f, 0.0f};
		Eigen::Vector3f span1 = (fabs(normal.dot(xVector)) < 0.9f) ?
			normal.cross(xVector) :
			normal.cross(Eigen::Vector3f{0.0f, 1.0f, 0.0f});
		Eigen::Vector3f span2 = normal.cross(span1);
	
		span1.normalize();
		span2.normalize();
	
		assert(fabs(normal.squaredNorm() - 1.0f) < 0.01f);
		assert(fabs(span1.squaredNorm() - 1.0f) < 0.01f);
		assert(fabs(span2.squaredNorm() - 1.0f) < 0.01f);
		assert(fabs(normal.dot(span1)) < 0.01f);
		assert(fabs(normal.dot(span2)) < 0.01f);
		assert(fabs(span1.dot(span2)) < 0.01f);
	
		return std::make_pair(span1, span2);
	}
	
	inline void drawCylinder(const Eigen::Vector3f& supportingPoint, const Eigen::Vector3f& normal, float radius){
		const float length = 10;
		const int divisions = 10;
	
		const auto tangents = getPlaneTangents(normal);
	
		glBegin(GL_QUAD_STRIP);
		for(int i = 0; i <= divisions; ++i){
			float theta = i*2.0f*M_PI / divisions;
			Eigen::Vector3f offset = radius * (std::cos(theta) * tangents.first + std::sin(theta) * tangents.second);
			Eigen::Vector3f v1 = supportingPoint - length*normal + offset;
			Eigen::Vector3f v2 = supportingPoint + length*normal + offset;
			glVertex3fv(v1.data());
			glVertex3fv(v2.data());
		}
		glEnd();
	}

	template <typename Cont, typename Value>
	void actuallyErase(Cont& container, const Value& value){
		container.erase(std::remove(container.begin(), container.end(), value), container.end());
	}

	template <typename Cont, typename Pred>
	void actuallyEraseIf(Cont& container, Pred&& predicate){
		container.erase(std::remove_if(container.begin(), container.end(), std::forward<Pred>(predicate)), container.end());
	}

	inline void drawPlane(const Eigen::Vector3f& normal, float offset, float size) {
		Eigen::Vector3f tangent1, tangent2;

		tangent1 = normal.cross(Eigen::Vector3f{1,0,0});
		if(tangent1.isZero(1e-3f)){
			tangent1 = normal.cross(Eigen::Vector3f{0,0,1});
			if(tangent1.isZero(1e-3f)){
				tangent1 = normal.cross(Eigen::Vector3f{0,1,0});
			}
		}

		tangent1.normalize();

		tangent2 = normal.cross(tangent1);
		tangent2.normalize(); //probably not necessary

		const float sos = normal.dot(normal);
		const Eigen::Vector3f supportPoint{normal.x()*offset/sos,
			normal.y()*offset/sos,
			normal.z()*offset/sos};

		glBegin(GL_QUADS);
		glNormal3fv(normal.data());
		glVertex3fv((supportPoint + size*(tangent1 + tangent2)).eval().data());
		glVertex3fv((supportPoint + size*(-tangent1 + tangent2)).eval().data());
		glVertex3fv((supportPoint + size*(-tangent1 - tangent2)).eval().data());
		glVertex3fv((supportPoint + size*(tangent1  - tangent2)).eval().data());
		glEnd();
	}

  template <typename Cont, typename Value>
  bool containsValue(const Cont& container, const Value& value){
	  return std::find(container.begin(), container.end(), value) != container.end();
  }
  
	//pick the element of the container that minimizes Functor(elem), for example, the distance to a point
	template <typename Cont, typename Functor,
			typename It = typename Cont::iterator,
			typename Val = decltype(std::declval<Functor>()(*std::declval<It>()))>
	  std::pair<It, Val>
	  minProjectedElement(Cont& cont, Functor&& functor){
	  
		using std::begin;
		using std::end;
		if(begin(cont) == end(cont)){
			return std::make_pair(end(cont), std::numeric_limits<Val>::quiet_NaN());
		}

		It bestIt = begin(cont);
		Val bestVal = functor(*bestIt);

		for(auto it = begin(cont); it != end(cont); ++it){
			Val thisVal = functor(*it);
			if(thisVal < bestVal){
				bestVal = thisVal;
				bestIt = it;
			}
		}
		return std::make_pair(bestIt, bestVal);
  }

	template<typename Cont, typename Val, typename It = typename Cont::iterator>
	It findInContainer(Cont& cont, const Val& val){
		using std::begin;
		using std::end;
		return std::find(begin(cont), end(cont), val);
	}
  
	template<typename Cont, typename Pred, typename It = typename Cont::iterator>
	It findIfInContainer(Cont& cont, Pred&& pred){
		using std::begin;
		using std::end;
		return std::find_if(begin(cont), end(cont), std::forward<Pred>(pred));
	}
	
	template<typename T>
	struct incomplete;
}
