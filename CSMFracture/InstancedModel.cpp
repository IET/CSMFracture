#pragma once

#include "InstancedModel.h"
#include "particle.h"

InstancedModel::InstancedModel()
{
	this->instances = 1000;
	gammaCorrection = false;
	
	positions = new Eigen::Vector3f [instances];
	colors = new Eigen::Vector4f[instances];
}

InstancedModel::InstancedModel(int n)
{
	instances = n;
	gammaCorrection = false;

	positions = new Eigen::Vector3f[instances];
	colors = new Eigen::Vector4f[instances];
}

//Getters and Setters
int InstancedModel::getInstances()
{
	return this->instances;
}

// Constructor, expects a filepath to a 3D model. Allows instanced rendering.
InstancedModel::InstancedModel(std::string const & path, int instances, bool gamma)
{
	gammaCorrection = gamma;
	this->instances = instances;
	positions = new Eigen::Vector3f[instances];
	colors = new Eigen::Vector4f[instances];

	this->loadModel(path);
	ilInit();

	//TODO - Allow different instanced shaders to be used
	this->setShader(ShaderManager::loadShader("instanced"));
}

// Constructor, expects a filepath to a 3D model. Allows instanced rendering.
InstancedModel::InstancedModel(std::string const & path, std::vector<Particle> &particles, bool gamma)
{
	gammaCorrection = gamma;

	this->loadModel(path, particles);
	ilInit();

	//TODO - Allow different instanced shaders to be used
	this->setShader(ShaderManager::loadShader("instanced"));
}



//Draw only a limited number of instances
void InstancedModel::Draw(Eigen::Matrix4f &v, Eigen::Matrix4f &p, int numInstances) {
	this->getShader()->enableShader();

	this->getShader()->setUniformMatrix4fv("view", v);
	this->getShader()->setUniformMatrix4fv("projection", p);

	for (GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw(numInstances);

	this->getShader()->disableShader();
}

//Draw only a limited number of instances
void InstancedModel::Draw(Eigen::Matrix4f &v, Eigen::Matrix4f &p) {
	this->getShader()->enableShader();

	this->getShader()->setUniformMatrix4fv("view", v);
	this->getShader()->setUniformMatrix4fv("projection", p);

	for (GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw();

	this->getShader()->disableShader();
}

//Draw only a limited number of instances
void InstancedModel::Draw(Eigen::Matrix4f &v, Eigen::Matrix4f &p, std::vector<Particle> &particles) {
	this->getShader()->enableShader();

	this->getShader()->setUniformMatrix4fv("view", v);
	this->getShader()->setUniformMatrix4fv("projection", p);

	for (GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw(particles);

	this->getShader()->disableShader();
}

void InstancedModel::loadModel(std::string path)
{
	// Read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	// Check for errors
	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return;
	}
	// Retrieve the directory path of the filepath
	this->directory = path.substr(0, path.find_last_of('/'));

	// Process ASSIMP's root node recursively
	this->processNode(scene->mRootNode, scene);
}

void InstancedModel::loadModel(std::string path, std::vector<Particle> &particles)
{
	// Read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	// Check for errors
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return;
	}
	// Retrieve the directory path of the filepath
	this->directory = path.substr(0, path.find_last_of('/'));

	// Process ASSIMP's root node recursively
	this->processNode(scene->mRootNode, scene, particles);
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void InstancedModel::processNode(aiNode* node, const aiScene* scene)
{
	// Process each mesh located at the current node
	for(GLuint i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]]; 
		this->meshes.push_back(this->processMesh(mesh, scene));			
	}
	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for(GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene);
	}
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void InstancedModel::processNode(aiNode* node, const aiScene* scene, std::vector<Particle> &particles)
{
	// Process each mesh located at the current node
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->meshes.push_back(this->processMesh(mesh, scene, particles));
	}
	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene, particles);
	}
}

InstancedMesh InstancedModel::processMesh(aiMesh* mesh, const aiScene* scene)
{
	// Data to fill
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	// Walk through each of the mesh's vertices
	for(GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		
		// Positions
		vertex.Position = Eigen::Vector3f(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
		
		// Normals
		vertex.Normal = Eigen::Vector3f(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);
		
		// Texture Coordinates
		if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			Eigen::Vector2f vec(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = Eigen::Vector2f(0.0f, 0.0f);
			
		if (mesh->mTangents != NULL) 
		{
			// Tangent
			vertex.Tangent = Eigen::Vector3f(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z);
			
			// Bitangent
			vertex.Bitangent = Eigen::Vector3f(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z);
		}
		else
		{
			vertex.Tangent = Eigen::Vector3f(0.0f, 0.0f, 0.0f);				
			vertex.Bitangent = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for(GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices std::vector
		for(GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// Process materials
	if(mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. Height maps
		vector<Texture> heightMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}
		
	// Return a mesh object created from the extracted mesh data
	return InstancedMesh(vertices, indices, textures, instances, positions, colors);
}

InstancedMesh InstancedModel::processMesh(aiMesh* mesh, const aiScene* scene, std::vector<Particle> &particles)
{
	// Data to fill
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;

		// Positions
		vertex.Position = Eigen::Vector3f(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);

		// Normals
		vertex.Normal = Eigen::Vector3f(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);

		// Texture Coordinates
		if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			Eigen::Vector2f vec(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = Eigen::Vector2f(0.0f, 0.0f);

		if (mesh->mTangents != NULL)
		{
			// Tangent
			vertex.Tangent = Eigen::Vector3f(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z);

			// Bitangent
			vertex.Bitangent = Eigen::Vector3f(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z);
		}
		else
		{
			vertex.Tangent = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
			vertex.Bitangent = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for (GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices std::vector
		for (GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// Process materials
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. Height maps
		vector<Texture> heightMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}

	// Return a mesh object created from the extracted mesh data
	return InstancedMesh(vertices, indices, textures, particles);
}

InstancedModel::~InstancedModel(){

	//if (positions != nullptr)
		//delete[] positions;
		//positions = nullptr;
}