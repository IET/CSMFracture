#include "stdafx.h"
#include <memory>
#include <iostream>

//OpenGL Stuff
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <glfw3.h>

#include "GLUtils.h"

#include "world.h"
#include "parallelWorld.hpp"
#include "color_spaces.h"
#include "particle.h"
#include "range.hpp"

//OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <cl/cl.hpp>
#include <CL/cl_gl.h>
#include "GPUDataTypes.h"
#include "clUtils.hpp"

//Model Loading and Rendering
#include "model.h"
#include "InstancedModel.h"
#include "Renderer.h"

#include "profiler.hpp"

#define clGetGLContextInfoKHR clGetGLContextInfoKHR_proc
static clGetGLContextInfoKHR_fn clGetGLContextInfoKHR;

const int MAX_PARTICLES = 100000;

//OpenCL Methods and Variables
//Perform GPU Computations

bool parallelPhysics = false;
bool usingGPU = false;

//Handles Rendering Initialisation 
bool glInit();
bool loadModels(int n);

//Handle user input
bool keys[1024];
void handleInput();
bool paused = false;

//GLFW Window Parameters
GLFWwindow* window;
const char* windowTitle = "Ductile Fracture For Clustered shape Matching";
const int SCREEN_WIDTH = 960;
const int SCREEN_HEIGHT = 540;

// Camera Parameters
const float nearPlane = 0.1f;
const float farPlane = 1000.0f;
const float fov = 45.0f; //Default Field of View

// Function prototypes
//GLFW Input Callbacks
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

//Particle Color Indices
std::vector<GLint> particleColors;
std::vector<Eigen::Vector3f> clusterColors;

//Models
InstancedModel instancedSphere(MAX_PARTICLES);
Model sphere;	//For Projectiles

//TODO - Handle this better
float neighbourRadius = 0.01f;

const float PLANE_SIZE = 1;

// Definition of world parameters
World *world;

std::vector<Particle> particles;

int main(int argc, char** argv) {
	if (argc < 2) {
		std::cout << "usage: ./opengl3app <input.json> [dump eye ex ey ex [lx ly lz]] \n -g = usingGPU \n -p = parallelPhysics" << std::endl;
		return 1;
	}

	for (int i = 2; i < argc; ++i)
	{
		if (strcmp(argv[i], "-p") == 0)
			parallelPhysics = true;
		if (strcmp(argv[i], "-g") == 0)
			usingGPU = true;
	}

	//Initialise OpenGL
	if (!glInit()) {
		std::cout << "Failed to initialise OpenGL" << std::endl;
		// Block for input
		std::cin.sync();
		std::cin.get();
		return -1;
	}

	if (parallelPhysics)
		world = new ParallelWorld(usingGPU);
	else
		world = new World();

	//load world parameters
	world->loadFromJson(argv[1]);
	world->initializeNeighbors();

	if (parallelPhysics)
	{
		world->particles.resize(clUtils::uSnap(world->particles.size() * 5, ((ParallelWorld*)world)->NUM_ELEMENTS));
		world->clusters.resize(clUtils::uSnap(world->clusters.size() * 3, ((ParallelWorld*)world)->NUM_ELEMENTS));
	}

	if (!loadModels(world->numParticles))
	{
		std::cout << "Failed to load 3D models." << std::endl;
		// Block for input
		std::cin.sync();
		std::cin.get();
		return -1;
	}

	if (parallelPhysics)
	{
		ParallelWorld *w = (ParallelWorld*)world;
		w->initialiseBuffers(instancedSphere.meshes[0].particleBuffer);
		w->initialiseKernels();
	}

	uint fractureIndices[1] = { 0 };
	cl::Buffer fractureIndicesBuffer(fractureIndices, fractureIndices + 1, false);
	
	bool dumpFrames = argc > 2;

	Eigen::Vector3f eye{ 0.0f, 0.0f, 3.0f };
	if (argc >= 7) {
		eye.x() = atof(argv[4]);
		eye.y() = atof(argv[5]);
		eye.z() = atof(argv[6]);
	}

	//TODO - Add lighting in shader
	Eigen::Vector3f lPos{ 20.0f, 25.0f, 20.0f };
	if (argc >= 10) {
		lPos.x() = atof(argv[7]);
		lPos.y() = atof(argv[8]);
		lPos.z() = atof(argv[9]);
	}

	const float meshSize = 100;
	const float sphereSize = 0.01f; //.03 for broken heart, armadillo
	const float scaleFactor = sphereSize / meshSize;

	std::string fileBase = "renderFrames/frame.%04d.png";
	char fname[1024];

	//Create Projection Matrix
	Eigen::Matrix4f projection = perspective(fov, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, nearPlane, farPlane);
	//Create View Matrix
	Eigen::Matrix4f view = lookAt(eye, Eigen::Vector3f(0.0f, 0.0f, 0.0f), Eigen::Vector3f(0.0f, 1.0f, 0.0f));

	int frame = 0;
	float elapsedTime = 0.0f;
	benlib::Profiler mainProf;
//	benlib::Profiler fractureProf;
//	benlib::Profiler physicsProf;

/*
	cl::EnqueueArgs clusterWriteArgs(64, NUM_ELEMENTS);
	cl::EnqueueArgs clusterArgs(uSnap(world->clusters.size(), NUM_ELEMENTS), NUM_ELEMENTS);
	cl::EnqueueArgs particleArgs(uSnap(world->particles.size(), NUM_ELEMENTS), NUM_ELEMENTS);
*/
	while (!glfwWindowShouldClose(window)) {
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		handleInput();

		if (!paused)
		{
			//Set BG Color & Clear the colorbuffer
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			{
				auto timestepTime = mainProf.timeName("physicsTimestep");
				// TODO - Actual polymorphism with an abstract timestep method
				//Run OpenCL Kernels
				if (parallelPhysics)
					((ParallelWorld*)world)->timestep();
				else
					world->timestep();
			}

			for (auto i : benlib::range(world->projectiles.size())) 
			{
				//TODO - Draw projectiles
			}


			{

				auto particleRenderTime = mainProf.timeName("rendering");

				for (auto i : benlib::range(world->movingPlanes.size())) {
					//auto particleRenderTime = mainProf.timeName("renderPlanes");

					Eigen::Vector3f normal = world->movingPlanes[i].normal;
					Eigen::Vector3f position = (normal * world->movingPlanes[i].offset) + (world->movingPlanes[i].velocity * elapsedTime * normal);
					Renderer::DrawPlane(view, projection, position, normal, PLANE_SIZE, Eigen::Vector4f(0.6f, 0.2f, 0.2f, 1.0f));
				}

				for (auto i : benlib::range(world->tiltingPlanes.size())) {
					//auto particleRenderTime = mainProf.timeName("renderPlanes");

					Eigen::Vector3f normal = world->tiltingPlanes[i].normal;
					Eigen::Vector3f position = (normal * world->tiltingPlanes[i].offset);
					Renderer::DrawPlane(view, projection, position, world->tiltingPlanes[i], elapsedTime/4.0f, PLANE_SIZE/2.0f, Eigen::Vector4f(0.6f, 0.2f, 0.2f, 1.0f));
				}

				//Draw particles
				if (parallelPhysics)
				{
					if (((ParallelWorld*)world)->glInterop) {
						instancedSphere.Draw(view, projection, world->numParticles);
					}
					else
					{
						ParallelWorld *w = (ParallelWorld*)world;
						cl::Event event;
						clEnqueueReadBuffer(w->queue(), (*w->particleBuffer)(), CL_TRUE, 0, sizeof(Particle) * particles.size(), &*particles.begin(), 0, NULL, &event());

//						cl::copy(*((ParallelWorld*)world)->particleBuffer, particles.begin(), particles.end());
//						for (int i = 0; i < particles.size(); ++i)
//							particles[i] = world->particles[i];

						instancedSphere.Draw(view, projection, particles);
					}
				}
				else
				{
					for (int i = 0; i < particles.size(); ++i)
						particles[i] = world->particles[i];
					instancedSphere.Draw(view, projection, particles);
				}
			}
	
			//std::cout << "finished frame: " << frame << std::endl;
			frame++;
			// Swap the screen buffers
			glfwSwapBuffers(window);
			elapsedTime += world->worldParameters.dt;
		}

		if (elapsedTime > 15)
			glfwSetWindowShouldClose(window, GL_TRUE);
	}


	glfwTerminate();
	std::cout << "Main Loop Profiler Percentages:" << std::endl;
	mainProf.dumpPercentages();
//	std::cout << std::endl << "Physics Profiler Percentages:" << std::endl;
//	physicsProf.dumpPercentages();
//	mainProf.dump<std::chrono::milliseconds>();
//	std::cout << std::endl << "Fracture Profiler Percentages:" << std::endl;
//	fractureProf.dumpPercentages();
//	fractureProf.dump<std::chrono::milliseconds>();
	std::cout << std::endl;
	ofstream csvDumpFile;
	csvDumpFile.open("profiler.csv");
	mainProf.dumpCSV(csvDumpFile);
	csvDumpFile.close();
	delete world;
	return 0;
}


bool glInit() {

	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl << std::endl;
	// Init GLFW
	if (!glfwInit())
	{
		std::cout << "ERROR: Could not initialise GLFW...";
		return false;
	}

	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create a GLFWwindow object that we can use for GLFW's functions
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, windowTitle, nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		std::cout << "ERROR: Could not create winodw...";
		return false;
	}

	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Capture cursor, useful for looking around scene
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return false;
	}

	// Define the viewport dimensions
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	//Set background color
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

	//	if (!loadModels())
	//	return false;

	return true;
}

bool loadModels(int n)
{
/*
	particles.resize(world->particles.size());
	for (int i = 0; i < world->particles.size(); ++i)
		particles[i] = world->particles[i];
*/
	particles.resize(n);
	for (int i = 0; i < n; ++i)
		particles[i] = world->particles[i];

	if (parallelPhysics && usingGPU && ((ParallelWorld*)world)->glInterop)
	{
		instancedSphere = InstancedModel("../models/sphere.obj", world->particles);	
	}
	else
	{
		instancedSphere = InstancedModel("../models/sphere.obj", particles);
	}
	sphere.load("../models/sphere.obj");

	return true;
}

void handleInput()
{
	if (keys[GLFW_KEY_R])
	{
		world->restart();
	}
	if (keys[GLFW_KEY_P])
	{
		paused = !paused;
		keys[GLFW_KEY_P] = false;
	}
	if (keys[GLFW_KEY_SPACE])
	{
		paused = !paused;
		keys[GLFW_KEY_SPACE] = false;
	}
}

//GLFW Input Callbacks
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//Guard against any out-of-range keys, like media keys
	if (key > 1023 || key < 0)
		return;

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{

}
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{

}
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

}