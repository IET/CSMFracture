	
//TODO - Think this many includes is causing exceptionally long build times
//#include "ClusteringParams.h"
#include "GPUDataTypes.h"

#include "eigenSVD.cl"
#include "Particle.h"
#include "ClMaths.cl"
#include "Utils.cl"
#include "planeUtils.cl"

#include "WorldParams.h"


typedef struct {
	mat3f m1;
	mat3f m2;
}PolarDecomposition;

inline PolarDecomposition PolarDecomp(const mat3f *A)
{
	PolarDecomposition ret;
	
	mat3f U,V, Vt;
	float3 sigma;

	svdcmp(A, &sigma, &U, &V);

	Vt = Mat3fTranspose(&V);
	mat3f Q = MatMatMult3f(U, Vt);
	float detQ = Mat3fDeterminant(&Q);


	if (detQ < 0.0f)
	{
		sigma.z *= -1;
		
		V.data[0][2] *= -1;
		V.data[1][2] *= -1;
		V.data[2][2] *= -1;

		Vt = Mat3fTranspose(&V);
		Q = MatMatMult3f(U, Vt);
	}
	mat3f S;
	S.data[0][0] = sigma.x;	S.data[0][1] = 0.0f;	S.data[0][2] = 0.0f;
	S.data[1][0] = 0.0f;	S.data[1][1] = sigma.y;	S.data[1][2] = 0.0f;
	S.data[2][0] = 0.0f;	S.data[2][1] = 0.0f;	S.data[2][2] = sigma.z;

	ret.m1 = Q;
	ret.m2 = MatMatMult3f(MatMatMult3f(V, S), Vt);
	return ret;
}

__kernel void TestFloatKernel(__global float * input)
{
	uint i = get_global_id(0);
	input[i] = (float)i;
}

__kernel void TestKernel(__global Particle *particles)
{
	uint i = get_global_id(0);
	if (i == 0)
		return;
	float a = 0.00012;
	float b = 200000;

	a *= b;
	b /= 1000;
	b = b* a * 10.0f;

	printf("PARTICLE %d\n", i);
}

//TODO - Perhaps merge these two kernels
__kernel void TestOutsideTwistingPlane(__global Particle *particles, __constant struct TwistingPlane *twistingPlanes,__global uint *activeParticles,int numTwistingPlanes,float elapsedTime)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];

	for (int j = 0; j < numTwistingPlanes; ++j)
	{
		struct TwistingPlane tp = twistingPlanes[j];
		if (ParticleOutsidePlane(&p, tp.normal, tp.offset))
		{
			if (tp.lifetime < elapsedTime)
			{
				particles[i].outsideSomeMovingPlane = false;
			}
		}
	}
}

__kernel void TestOutsideTiltingPlane(__global Particle *particles, __constant struct TiltingPlane *tiltingPlanes,__global uint *activeParticles,int numTiltingPlanes,float elapsedTime)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];

	for (int j = 0; j < numTiltingPlanes; ++j)
	{
		struct TiltingPlane tp = tiltingPlanes[j];
		if (ParticleOutsidePlane(&p, tp.normal, tp.offset))
		{
			if (tp.lifetime < elapsedTime)
			{
				particles[i].outsideSomeMovingPlane = false;
			}
		}
	}
}

__kernel void ClearParticleGoals(__global Particle *particles,__global uint *activeParticles)
{
	uint i = get_global_id(0);

	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	particles[i].oldPosition	= particles[i].position;
	particles[i].goalPosition	= (float3)(0.0f, 0.0f, 0.0f);
	particles[i].goalVelocity	= (float3)(0.0f, 0.0f, 0.0f);

	particles[i].flags = 0;
}

//TODO - Now figure out where potentialsplits are.
__kernel void UpdateClusterPlasticities(__global Cluster *clusters,__global Particle *particles,__global uint *activeClusters, __global PotentialFracture *potentialFractures, __global uint *fractureIndices,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Cluster c = clusters[i];

//	if (i == 0)
//		printf("cluster %d WorldCom = [%f, %f, %f]\n", i, c.worldCom.x, c.worldCom.y, c.worldCom.z);

	clusters[i].toughness = wp.toughnessBoost > 0.0f ? (wp.toughness * (1.0f + wp.toughnessBoost * exp(-wp.toughnessFalloff * clusters[i].timeSinceLastFracture))) : clusters[i].toughness;
	clusters[i].timeSinceLastFracture += (wp.toughnessBoost > 0.0f ? wp.dt : 0.0f);
	
	mat3f Apq = mat3fzero;

		//Compute Cluster Velocity & Weighted COM
	float mass = 0.0f;
	float3 clusterVelocity = (float3)(0.0f, 0.0f, 0.0f);
	float3 com = (float3)(0.0f, 0.0f, 0.0f);

	//Read only access to particle buffer
	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;
		float w = (memberWeight / particles[memberIndex].totalweight) * particles[memberIndex].mass;

		mass += w;
		clusterVelocity += w * particles[memberIndex].velocity;
		com += (w * particles[memberIndex].position);
	}
	c.worldCom = (com / mass);
	clusterVelocity /= mass;

	c.velocity = clusterVelocity;

	//Read only access to particle buffer
	for (uint j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;

		float3 pj = particles[memberIndex].position - c.worldCom;
		float3 qj = particles[memberIndex].restPosition - c.restCom;
		float fracWeight = memberWeight / particles[memberIndex].totalweight;

		mat3f outerProd = VecOuterProduct(pj, qj);
		mat3f temp = MatScalarMult3f(outerProd, fracWeight * particles[memberIndex].mass);
		MatMatAdd3f_inPlace(&Apq, &temp);
	}

	mat3f A = MatMatMult3f(Apq, clusters[i].aInv);
	c.Fp = clusters[i].Fp;
	mat3f FpInv = Mat3fInverse(c.Fp);

	if ((wp.nu) > 0.0f)
		A = MatMatMult3f(A, FpInv);
	
	mat3f V, U;
	float3 sigma;
	svdcmp(&A, &sigma, &U, &V);

	PotentialFracture pf;
	pf.valid			 	= fabs(sigma.x - 1.0f) > c.toughness && (c.justFractured == false);
	pf.normal			 	= (float3)(V.data[0][0], V.data[1][0], V.data[2][0]);
	pf.effectiveToughness 	= sigma.x - c.toughness;
	pf.cIndex 				= i;
	c.justFractured = ((!pf.valid) && c.justFractured == true && fabs(sigma.x - 1.0f) <= c.toughness) ? false : c.justFractured;

	if (pf.valid && wp.fractureOn)
		AddFracture(&pf, potentialFractures, fractureIndices);


//	if (clusters[i].potentialFracture.valid)
//		printf("Cluster %d is about to fracture!\n", i);
	
	//cluster.updatePlasticity			
	c.FpNew = c.Fp;

	if (wp.nu > 0.0f)
	{
		if (sigma.z >= 1e-4f)
		{
			//TODO - necessary intermediate?
			float3 FpHat = sigma;

			FpHat *= (1.0f / cbrt(FpHat.x * FpHat.y * FpHat.z));

			float norm = sqrt(SQR(FpHat.x - 1.0f) + SQR(FpHat.y - 1.0f) + SQR(FpHat.z - 1.0f));
			float localYield = wp.yield + wp.hardening * c.cstrain;

			mat3f FpHatDiag = mat3fzero;

			float gamma = min(1.0f, (wp.nu) * (norm - localYield) / norm);
			FpHatDiag.data[0][0] = pow(FpHat.x, gamma);
			FpHatDiag.data[1][1] = pow(FpHat.y, gamma);
			FpHatDiag.data[2][2] = pow(FpHat.z, gamma);
			// TODO - Check the validity of the below.
			if (norm > localYield)
				c.FpNew = MatMatMult3f(MatMatMult3f(FpHatDiag, Mat3fTranspose(&V)), MatScalarMult3f(c.Fp, Mat3fDeterminant(&V)));
		}
		c.cstrain += sqrt(SQR(sigma.x - 1.0f) + SQR(sigma.y - 1.0f) + SQR(sigma.z - 1.0f));
	}



	c.T = MatMatMult3f(U, Mat3fTranspose(&V));
	
	if (wp.nu > 0.0f)
		c.T = MatMatMult3f(c.T, c.Fp); // plasticity

	clusters[i] = c;
/*
	clusters[i].velocity = clusterVelocity;
	clusters[i].T = T;
	clusters[i].worldCom = worldCom;
	clusters[i].cstrain = cstrain;
	clusters[i].Fp = Fp;
	clusters[i].FpNew = FpNew;
*/
}

__kernel void UpdateParticleGoals(__global Particle *particles,__global Cluster *clusters,__global uint *activeParticles,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];
	
	for (int j = 0; j < p.numClusters; ++j)
	{
		int cIndex = p.clusters[j].index;
		float weight = p.clusters[j].weight;
		float w = weight / p.totalweight;

		mat3f T = clusters[cIndex].T;
		float3 rest = (p.restPosition - clusters[cIndex].restCom);

		p.goalPosition += w * (MatVecMult3f(&T, rest) + clusters[cIndex].worldCom);
		p.goalVelocity += w * clusters[cIndex].velocity;
	}

	if (p.numClusters <= 0)
	{
		p.goalPosition = p.position;
		p.goalVelocity = p.velocity;
	}
	if (!p.outsideSomeMovingPlane)
	{
		p.velocity += wp.dt * wp.gravity + (wp.alpha / wp.dt) * (p.goalPosition - p.position) +
			wp.springDamping * (p.goalVelocity - p.velocity);
	}	
	
	particles[i].goalPosition = p.goalPosition;
	particles[i].position += wp.dt * p.velocity;
	particles[i].velocity = p.velocity;
	particles[i].goalVelocity = p.goalVelocity;
}

__kernel void ZeroParticleGoalPositions(__global Particle *particles,__global uint* activeParticles)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	particles[i].goalPosition = (float3)(0.0f, 0.0f, 0.0f);
}

__kernel void StrainLimitingIteration(__global Cluster *clusters,__global Particle *particles,__global uint *activeClusters,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Cluster c = clusters[i];

	//Calculate World COM
	float mass = 0.0f;
	float3 com = (float3)(0.0f, 0.0f, 0.0f);

	// read only access to particle buffer
	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;
		float w = (memberWeight / particles[memberIndex].totalweight) * particles[memberIndex].mass;
		mass += w;
		com += w * particles[memberIndex].position;
	}
	c.worldCom = (com / mass);
	mat3f Apq = mat3fzero;

	// Read only access to particle buffer
	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;

		float3 pj = particles[memberIndex].position	- c.worldCom;
		float3 qj = particles[memberIndex].restPosition - c.restCom;
		float fracWeight = memberWeight / particles[memberIndex].totalweight;

		mat3f outerProd = VecOuterProduct(pj, qj);
		mat3f temp = MatScalarMult3f(outerProd, fracWeight * particles[memberIndex].mass);
		MatMatAdd3f_inPlace(&Apq, &temp);
	}

	mat3f A = MatMatMult3f(Apq, c.aInv);

	if (wp.nu > 0.0f)
		A = MatMatMult3f(A, Mat3fInverse(c.Fp));

	PolarDecomposition pr = PolarDecomp(&A);
	mat3f T = pr.m1;
	
	if (wp.nu > 0.0f)
		T = MatMatMult3f(T, c.Fp);
	
	clusters[i].T = T;
	clusters[i].worldCom = c.worldCom;
}

__kernel void StrainLimitingGoalUpdate(__global Particle *particles,__global Cluster *clusters,__global uint *activeParticles,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;
	Particle p = particles[i];

	float gammaSquared = wp.gamma * wp.gamma;

	for (int j = 0; j < p.numClusters; ++j)
	{
		int cIndex = p.clusters[j].index;
		float weight = p.clusters[j].weight;
		float w = weight / p.totalweight;

		mat3f T = clusters[cIndex].T;

		float3 rest = p.restPosition - clusters[cIndex].restCom;
		float3 goal = MatVecMult3f(&T, rest) + clusters[cIndex].worldCom;

		float ratio = SqrNorm(goal - p.position) / (clusters[cIndex].width * clusters[cIndex].width);

		if (ratio > gammaSquared)
			p.goalPosition += w * (goal + sqrt(gammaSquared / ratio) * (p.position - goal));
		else
			p.goalPosition += w * p.position;
	}
	
	particles[i].goalPosition = p.goalPosition;

	if (particles[i].numClusters == 0)
	{
		particles[i].goalPosition = particles[i].position;
	}
	particles[i].position = wp.omega * particles[i].goalPosition + (1.0f - wp.omega) * particles[i].position;
}

__kernel void UpdateParticleVelocities(__global Particle *particles,__global uint *activeParticles,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;


	particles[i].velocity = (1.0f / wp.dt) * (particles[i].position - particles[i].oldPosition);
}

__kernel void UpdateClusterComs(__global Cluster *clusters,__global Particle *particles,__global uint *activeClusters)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	float mass = 0.0f;
	float3 com = (float3)(0.0f, 0.0f, 0.0f);
	
	Cluster c = clusters[i];

	// Read only access to particle buffer 
	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;
		float w = (memberWeight / particles[memberIndex].totalweight) * particles[memberIndex].mass;

		mass += w;
		com += w * particles[memberIndex].position;
	}

	clusters[i].worldCom = (com / mass);
}

// TODO - Fracture!
__kernel void FracturePotentialSplits(__global PotentialFracture *ps, __global uint *fractureIndices, __global Cluster *clusters, __global Particle *particles, __global uint *activeClusters,__global uint *activeParticles,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = fractureIndices[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	ps[i].valid = 0;
	Cluster c = clusters[ps[i].cIndex];

	float3 worldCom = c.worldCom;
	ps[i].worldCom = worldCom;
	
	//TODO - This is nested too deep
	mat3f Apq = mat3fzero;
	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;

		float3 pj = particles[memberIndex].position - c.worldCom;
		float3 qj = particles[memberIndex].restPosition - c.restCom;
		float fracWeight = memberWeight / particles[memberIndex].totalweight;

		mat3f outerProd = VecOuterProduct(pj, qj);
		mat3f temp = MatScalarMult3f(outerProd, fracWeight * particles[memberIndex].mass);
		MatMatAdd3f_inPlace(&Apq, &temp);
	}

	mat3f A = MatMatMult3f(Apq, c.aInv);
	mat3f FpInv = Mat3fInverse(c.Fp);

//	A = wp.nu > 0.0f ? MatMatMult3f(A, FpInv) : A;
	if (wp.nu > 0.0f)
		A = MatMatMult3f(A, FpInv);

	mat3f U, V;
	float3 sigma;
	svdcmp(&A, &sigma, &U, &V);

	//TODO - Too much branching?
	if ( (fabs(sigma.x - 1) < c.toughness) || c.justFractured)
		return;

//	printf("Fracturing Cluster: %d.\n", ps[i].cIndex);

	float3 splitDirection = (float3)(V.data[0][0], V.data[1][0], V.data[2][0]);
	ps[i].splitDirection = splitDirection;
	
	int partition = partition_members(c.members, c.numMembers, &splitDirection, &c.worldCom, particles);
	int oldSize = partition;
	int newSize = (c.numMembers - partition);

/*
	for (int j = 0; j < c.numMembers; ++j)
	{
		if (j == oldSize)
			printf("====================================================\n");
		printf("dot(worldCom - p.position, splitDirection) > 0 = %d.\n", dot(worldCom - particles[c.members[j].index].position, splitDirection) > 0);

		if (dot(worldCom - particles[c.members[j].index].position, splitDirection) <= 0)
			break;
	}
	printf("------------------------------------------------------------------------\n");
	printf("A = [%f, %f, %f]\n[%f, %f, %f]\n[%f, %f, %f]\n", A.data[0][0], A.data[0][1], A.data[0][2], A.data[1][0], A.data[1][1], A.data[1][2], A.data[2][0], A.data[2][1], A.data[2][2]);
	printf("splitDirection = [%f, %f, %f]\n", splitDirection.x, splitDirection.y, splitDirection.z);
	printf("WorldCom = [%f, %f, %f]\n", worldCom.x, worldCom.y, worldCom.z);
*/

	if (newSize == 0 || oldSize == 0)
		return;

	Cluster newCluster;
	newCluster.numMembers = newSize;

	for (int j = 0; j < newSize; ++j)	
	{
		newCluster.members[j] = c.members[partition + j];
		particles[newCluster.members[j].index].flags = SPLIT;
//		particles[newCluster.members[j].index].color = (float3)(1.0f, 0.0f, 0.0f);
	}

	newCluster.numNeighbors = c.numNeighbors;
	newCluster.Fp = c.Fp;
	newCluster.FpNew = c.FpNew;
	newCluster.cstrain = c.cstrain;
	newCluster.toughness = c.toughness;

	for (int j = 0; j < c.numNeighbors; ++j)
		newCluster.neighbors[j] = c.neighbors[j];
	
	newCluster.neighbors[newCluster.numNeighbors++] = ps[i].cIndex;
	newCluster.numMembers = newSize;

	//Remove particles from old cluster
	c.numMembers = oldSize;

	//Update collision geometry
	newCluster.cg = c.cg;

	//Re-using matrix A to store intermediate result
	A = Mat3fInverse(MatMatMult3f(Apq, c.aInv));

	//we should be transforming from world to rest, so we shouldn't include plasticity
	float3 n = normalize(MatVecMult3f(&A, splitDirection));

	CgPlane cgPlane;
	cgPlane.normal = n;
	cgPlane.offset = -dot(n, c.restCom);
	c.cg.planes[c.cg.numPlanes++] = cgPlane;

	cgPlane.normal *= -1.0f;
	cgPlane.offset *= -1.0f;
	newCluster.cg.planes[newCluster.cg.numPlanes++] = cgPlane;

	newCluster.justFractured = wp.delayRepeatedFracture ? 1 : 0;
	c.justFractured = wp.delayRepeatedFracture ? 1 : 0;

	newCluster.timeSinceLastFracture = wp.toughnessBoost > 0.0f ? 0.0f : newCluster.timeSinceLastFracture;
	c.timeSinceLastFracture = wp.toughnessBoost > 0.0f ? 0.0f : c.timeSinceLastFracture;

	//Mark particles in old cluster to split. New one was done above
	for (int j = 0; j < c.numMembers; ++j)
	{
		particles[c.members[j].index].flags = SPLIT;
//		particles[c.members[j].index].color = (float3)(0.0f, 0.0f, 1.0f);
	}
	AddCluster(&newCluster, clusters, activeClusters);
	clusters[ps[i].cIndex] = c;
}

__kernel void SplitParticles(__global Particle *particles,__global Cluster *clusters, __global uint *activeParticles,__global PotentialFracture * potentialFractures,__global uint *fractureIndices)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active || (particles[i].flags != SPLIT))
		return;

//	printf("Handling fracture for particle[%d]\n", i);

	int numFractures = 0;
	while (fractureIndices[numFractures / 32] & (0x1 << numFractures % 32))
		numFractures++;

	PotentialFracture localFrac[32];
	for (int j = 0; j < numFractures; ++j)
	{
		localFrac[j] = potentialFractures[j];
	}

	prioritise_fractures(localFrac, numFractures);

	Particle p = particles[i];
	
	uint fractureIndex = 0;
	while (fractureIndices[fractureIndex/32] & (0x1 << fractureIndex % 32))
	{
		//printf("Handling fracture %d of cluster %d in particle %d.\n",fractureIndex, potentialFractures[fractureIndex].cIndex, i);
		Particle q = p;
		q.flags = 0;
		q.numClusters = 0;
		float w1 = 0.0f;

		for (int k = 0; k < p.numClusters; ++k)
		{
			int cIndex = p.clusters[k].index; 
			float cWeight = p.clusters[k].weight;

			if (cWeight < 0)
				continue;

			bool needsSplit =		(dot(p.position					- localFrac[fractureIndex].worldCom, localFrac[fractureIndex].splitDirection) >= 0) !=
									(dot(clusters[cIndex].worldCom	- localFrac[fractureIndex].worldCom, localFrac[fractureIndex].splitDirection) >= 0);
				
			if (!needsSplit)
				continue;

			w1 += cWeight;
				
			q.numClusters++;// = needsSplit;
			q.clusters[q.numClusters] = p.clusters[k];
			p.clusters[k].weight = -1;// *= (needsSplit ? -1.0f : 1.0f);// pow(-1.0f, (float)needsSplit);
		}

		if (q.numClusters > 0)
		{
//			p.color = (float3)(1.0f, 1.0f, 1.0f);

			q.mass = (w1 / p.totalweight) * p.mass;
			q.totalweight = w1;

			p.mass = ((p.totalweight - w1) / p.totalweight) * p.mass;
			p.totalweight -= w1;
			AddParticle(&q, particles, activeParticles);
		}
		++fractureIndex;
	}
	p.flags = 0;
	particles[i] = p;
}

//TODO - Update cluster neighbors (not needed for broken heart)
__kernel void CheckNeighbors(__global Cluster *clusters, __global Particle *particles, __global uint *activeClusters)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	// Actually might want to do this per-particle?
}

__kernel void SplitOutliers(__global Particle *particles,__global Cluster *clusters, __global uint *activeParticles,WorldParams wp)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active || particles[i].numClusters < 2)
		return;

	Particle p = particles[i];
	
	for (int j = 0; j < p.numClusters; ++j)
	{
		int cIndex =	p.clusters[j].index;
		float cWeight = p.clusters[j].weight;

		if (cWeight < 0)
			continue;

		if (norm(p.position - clusters[cIndex].worldCom) >
			((1.0f + wp.gamma) * wp.outlierThreshold * norm(p.restPosition - clusters[cIndex].restCom)))
		{
//			printf("Splitting Outlier Particle %d.\n", i);
			Particle q = p;

			q.numClusters = 0;
			q.clusters[q.numClusters].index = cIndex;
			q.clusters[q.numClusters].weight = cWeight;
			++q.numClusters;

			q.mass = (cWeight / p.totalweight) * p.mass;
			q.totalweight = cWeight;
			
			float newMass = ((p.totalweight - cWeight) / p.totalweight) * p.mass;
			
			p.clusters[j] = p.clusters[--p.numClusters];
			p.mass = newMass;
			p.totalweight -= cWeight;

			// TODO - remove p from members in c
//			c.numMembers--;
//			c.members[i] = c.members[c.numMembers];

			AddParticle(&q, particles, activeParticles);
		}
	}
	particles[i].mass = p.mass;
	particles[i].totalweight = p.totalweight;
	particles[i].totalweight = p.totalweight;
	particles[i].numClusters = p.numClusters;
	for (int j = 0; j < p.numClusters; ++j)
		particles[i].clusters[j] = p.clusters[j];
}

__kernel void CullSmallClusters(__global Cluster *clusters,__global Particle *particles, __global uint *activeClusters)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;
	
	Cluster c = clusters[i];

	float threshold = 1e-4f;

	if (c.numMembers < 4 || c.mass < threshold)
	{
//		printf("Removing Cluster %d. Mass is %f, numMembers = %d.\n", i, c.mass, c.numMembers);
		for (int j = 0; j < c.numMembers; ++j)
		{
			int memberIndex = c.members[j].index;
			float memberWeight = c.members[j].weight;
//			atomicAdd_g_f((volatile __global float*)&particles[memberIndex].totalweight, -memberWeight);
		}
		atomic_and((volatile __global uint*)&activeClusters[i / 32], ~(0x1 << (i % 32)));
	}
/*	
	barrier(CLK_GLOBAL_MEM_FENCE);

	for (int j = 0; j < c.numNeighbors; ++j)
	{
		int neighborIndex = c.neighbors[j];
		//If neigbor is not an active cluster
		if ( !(activeClusters[neighborIndex / 32] & (0x1 << (neighborIndex % 32))) )
		{
			c.neighbors[j] = c.neighbors[--c.numNeighbors];
			//Just changed this value so need to check this index again
			--j;
		}
	}
*/
	for (int j = 0; j < c.numNeighbors; ++j)
		clusters[i].neighbors[j] = c.neighbors[j];
	clusters[i].numNeighbors = c.numNeighbors;
//	clusters[i] = c;
}

__kernel void RemoveLonelyParticles(__read_only __global Particle *particles,__global Cluster *clusters, __global uint *activeParticles)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	//In this case also return if particle is not lonely
	if (!active || particles[i].numClusters > 0)
		return;
	//invalidate particle since it belongs to no clusters
	//printf("Invalidating particle %d because of loneliness.\n", i);
	//particles[i].position = (float3)(0.0f, 0.0f, 0.0f);
	atomic_and((volatile __global uint*)&activeParticles[i / 32], ~(0x1 << (i % 32)));
}

// TODO - Do I really need this?? I already do count after recount to keep everything in sync
__kernel void UpdateClusterMembers(__global Cluster * clusters,__global Particle *particles,__global uint *activeParticles,__global uint *activeClusters)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	for (int j = 0; j < clusters[i].numMembers; ++j)
	{
		int memberIndex = clusters[i].members[j].index;
		//If particle is now inactive	
		if (!(activeParticles[memberIndex / 32] & (0x1 << (memberIndex % 32))))
		{
			clusters[i].members[j] = clusters[i].members[--clusters[i].numMembers];
			
			//Just changed this member so need to re-check it
			--j;
		}
	}
}

//CountClusters
__kernel void ClearParticleClusters(__global Particle *particles,__global uint *activeParticles)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

//	if (i > 20132)
//		printf("Clearing clusters for particle %d.\n", i);

	particles[i].numClusters = 0;
//	particles[i].totalweight = 0.0f;
}

__kernel void ClearClusterMembers(__global Cluster *clusters,__global uint *activeClusters)
{
	uint i = get_global_id(0);

	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;
//	if (i > 99)
//		printf("Clearing members for cluster %d.\n", i);

	clusters[i].numMembers = 0;
}

__kernel void CountClusters(__global Cluster *clusters, __global Particle *particles,__global uint *activeClusters,__global uint *activeParticles)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;
	Cluster c = clusters[i];

	for (int j = 0; j < c.numMembers; ++j)
	{
		float memberWeight = c.members[j].weight;
		int	memberIndex = c.members[j].index;
		
		//Skip inactive particles
		if (!(activeParticles[memberIndex / 32] & (0x1 << (memberIndex % 32))))
		{
			c.members[j] = c.members[--c.numMembers];
			--j;
			continue;
		}
		int numClusters = atomic_inc(&particles[memberIndex].numClusters);
		particles[memberIndex].clusters[numClusters].index = (int)i;
		particles[memberIndex].clusters[numClusters].weight = memberWeight;
//		atomicAdd_g_f((volatile __global float*)&particles[memberIndex].totalweight, memberWeight);
	}
}

__kernel void CountClustersPerParticle(__global Particle *particles, __global Cluster *clusters,__global uint *activeParticles,__global uint *activeClusters)
{
	uint i = get_global_id(0);
	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;
/*
	if (i > 20132)
	{
		printf("Counting clusters for particle %d. NumClusters = %d\n", i, particles[i].numClusters);
		return;
	}
*/
	Particle p = particles[i];
	p.totalweight = 0.0f;

	for (int j = 0; j < p.numClusters; ++j)
	{
		float	clusterWeight	= p.clusters[j].weight;
		int		clusterIndex	= p.clusters[j].index;

		if (clusterWeight < 0 || (!activeClusters[clusterIndex / 32] & (0x1 << (clusterIndex % 32))))
			continue;

		p.totalweight += clusterWeight;

		int numMembers = atomic_inc(&clusters[clusterIndex].numMembers);
		clusters[clusterIndex].members[numMembers].index	= (int)i;
		clusters[clusterIndex].members[numMembers].weight	= clusterWeight;
	}

	particles[i] = p;
}

__kernel void UpdateClusterProperties(__global Cluster *clusters,__global Particle *particles,__global uint *activeClusters)
{
	uint i = get_global_id(0);
	uint width = get_global_size(0);

	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Cluster c = clusters[i];
	
	//Calculate Cluster mass
	float mass = 0.0f;
	float3 com = (float3)(0.0f, 0.0f, 0.0f);
	float3 restCom = (float3)(0.0f, 0.0f, 0.0f);
	
	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;
		float w = (memberWeight / particles[memberIndex].totalweight) * particles[memberIndex].mass;

		mass += w;
		com += w * particles[memberIndex].position;
		restCom += w * particles[memberIndex].restPosition;
	}

	c.mass = mass;
	c.worldCom = (com / mass);
	c.restCom = (restCom / mass);

	//Need restCom calculated for this one.
	c.width = 0.0f;
	c.aInv = mat3fzero;

	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;
	
		float3 qj = particles[memberIndex].restPosition - c.restCom;
				
		mat3f aInvIncrement = VecOuterProduct(qj, qj);
		
		aInvIncrement = MatScalarMult3f(aInvIncrement, (memberWeight / particles[memberIndex].totalweight) * particles[memberIndex].mass);
		c.width = max(c.width, norm(c.restCom - particles[memberIndex].restPosition));
		
		MatMatAdd3f_inPlace(&c.aInv, &aInvIncrement);
	}

	//Do Pseudoinverse
	mat3f A = c.aInv;
	mat3f U, V;
	float3 sigma;
	svdcmp(&A, &sigma, &U, &V);
	
	vector3f s;
	vector3f sigInv;
	s.v = sigma;
	for (int j = 0; j < 3; ++j)
	{
		if (s.a[j] < 1e-6f)
			c.Fp = mat3fidentity;

		sigInv.a[j] = fabs(s.a[j]) > 1e-12f ? 1.0f / s.a[j] : 1.0f / 1.0e-12f;
		c.mass = s.a[j] < 1e-12f ? 0.0f : c.mass;

/*
		if (s.a[j] < 1e-12f)
		{
//			printf("Marking Cluster %d for deletion. Cluster has %d members.\n", i, c.numMembers);
			c.mass = 0.0f;	//Mark cluster for deletion
		}
*/
	}

	clusters[i].aInv = MatMatMult3f(MatMatMult3f(V, Vec3fasDiagonal(sigInv.v)), Mat3fTranspose(&U));

//	clusters[i].aInv  = c.aInv;
	clusters[i].mass  = c.mass;
	clusters[i].width = c.width;
	
	clusters[i].worldCom = c.worldCom;
	clusters[i].restCom  = c.restCom;
}

__kernel void UpdateClusterTransforms(__global Cluster *clusters,__global Particle *particles,__global uint *activeClusters)
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Cluster c = clusters[i];

	mat3f Apq = mat3fzero;

	for (uint j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float memberWeight = c.members[j].weight;	

		float3 pj = particles[memberIndex].position - c.worldCom;
		float3 qj = particles[memberIndex].restPosition - c.restCom;
		float fracWeight = memberWeight / particles[memberIndex].totalweight;

		mat3f temp = MatScalarMult3f(VecOuterProduct(pj, qj), fracWeight * particles[memberIndex].mass);
		MatMatAdd3f_inPlace(&Apq, &temp);
	}

	mat3f A = MatMatMult3f(Apq, clusters[i].aInv);
	clusters[i].restToWorldTransform = A;
	clusters[i].worldToRestTransform = Mat3fInverse(A);
}

__kernel void SelfCollisions()
{
	uint i = get_global_id(0);

	//TODO - Will need to implement a BVH for this

}

//TODO - Probably merge kernels
__kernel void MovingPlaneInteraction(__global Particle *particles, __constant struct MovingPlane *movingPlanes,__global uint *activeParticles,WorldParams wp,int numMovingPlanes,float elapsedTime)
{
	uint i = get_global_id(0);

	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];

	bool bounced = true;

	int maxIterations = 10;
	int numIterations = 0;


	while (bounced)
	{
		bounced = false;

		for (int j = 0; j < numMovingPlanes; ++j)
		{
			if (wp.dragWithPlanes)
			{
				DragParticle(&p, movingPlanes[j]);
			}
			else
			{
				BounceParticle(&p, movingPlanes[j], elapsedTime);
			}

			if (BacksideReflectBounce(&p, movingPlanes[j], elapsedTime, 0.0f))
			{
				bounced = true;
			}
		}
		++numIterations;
		if (numIterations > maxIterations)
			break;
	}

	particles[i].velocity = p.velocity;	
	particles[i].position = p.position;
}

__kernel void TwistingPlaneInteraction(__global Particle *particles, __constant struct TwistingPlane *twistingPlanes,__global uint*activeParticles,int numTwistingPlanes,float elapsedTime)
{
	uint i = get_global_id(0);

	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];

	for (int j = 0; j < numTwistingPlanes; ++j)
	{
		TwistParticle(&p, twistingPlanes[j], elapsedTime);
	}
	
	particles[i].velocity = p.velocity;	
}

__kernel void TiltingPlaneInteraction(__global Particle *particles, __constant struct TiltingPlane *tiltingPlanes,__global uint *activeParticles,int numTiltingPlanes,float elapsedTime)
{
	uint i = get_global_id(0);

	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];

	for (int j = 0; j < numTiltingPlanes; ++j)
	{
		TiltParticle(&p, tiltingPlanes[j], elapsedTime);
	}

	particles[i].velocity = p.velocity;
}

__kernel void UpdateFpAndCom(__global Cluster *clusters,__global Particle *particles,__global uint *activeClusters )
{
	uint i = get_global_id(0);
	bool active = activeClusters[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Cluster c = clusters[i];
	c.Fp = c.FpNew;

	//Compute Cluster Weighted COM
	float mass = 0.0f;
	float3 com = (float3)(0.0f, 0.0f, 0.0f);

	for (int j = 0; j < c.numMembers; ++j)
	{
		int memberIndex = c.members[j].index;
		float w = (c.members[j].weight / particles[memberIndex].totalweight) * particles[memberIndex].mass;
		mass += w;
		com += (w * particles[memberIndex].position);
	}

	clusters[i].worldCom = (com / mass);	
	clusters[i].Fp = c.Fp;
}
