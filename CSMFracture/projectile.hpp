#pragma once

#include <Eigen/Dense>

struct Particle;

class Projectile {

public:

  Eigen::Vector3f start, velocity;
  float radius, momentumScale;  
  Projectile(const Eigen::Vector3f& _start, const Eigen::Vector3f& _velocity, 
	  float _radius, float _momentumScale)
	:start{_start}, velocity{_velocity}, radius{_radius}, momentumScale{_momentumScale}
  {}

  void bounceParticle(Particle& particle, float timeElapsed) const;

};
