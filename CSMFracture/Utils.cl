#ifndef UTILS_CL
#define UTILS_CL
#include "Particle.h"

void AddClusterAt( const Cluster *c,  uint i, __global Cluster *clusters,	volatile __global unsigned int *activeClusters);
void AddParticleAt(const Particle *p, uint i, __global Particle *particles,	volatile __global unsigned int *activeParticles);

uint AddCluster( const Cluster *c,  __global Cluster *clusters,	 volatile __global unsigned int *activeClusters);
uint AddParticle(const Particle *p, __global Particle *particles, volatile __global unsigned int *activeParticles);

uint NextFreeParticleIndex(uint start, volatile __global unsigned int *activeParticles);
uint NextFreeClusterIndex(uint start,  volatile __global unsigned int *activeClusters);

void quick_sort(int *a, int n);
int partition_members(ClusterMember *a, int n, const float3 *splitDirection, const float3 *worldCom, const __global struct Particle *particles);

//Returns next free fracture index
uint NextFreeFractureIndex(uint start, volatile __global unsigned int *fractureIndices)
{
	int i = start;
	bool valid = fractureIndices[i / 32] & (0x1 << (i % 32));
	unsigned int current, expected, next;
	while (true)
	{
		current = fractureIndices[i / 32];
		valid = current & (0x1 << (i % 32));

		// TODO - Less branching
		if (valid)
		{
			++i;
			continue;
		}

		//Make sure another thread doesn't take this index
		expected = current;
		next = expected | (0x1 << (i % 32));
		current = atomic_cmpxchg((volatile __global unsigned int*)&fractureIndices[i / 32], expected, next);
		if (current == expected)
			break;
	}

	return i;
}


//Inserts fracture into position i in buffer and sets the relevant bit to set it as active in activeClusters buffer
void AddFractureAt(const PotentialFracture *pf, uint i, __global PotentialFracture *fractures, volatile __global unsigned int *fractureIndices)
{
	// TODO - Maybe return an int so you can return an error if particle is already active?
	//Set particle as valid
	//printf("Adding fracture at index %d.\n", i);
	atomic_or((volatile __global unsigned int*)&fractureIndices[i / 32], (0x1 << (i % 32)));
	fractures[i] = *pf;
}

//Inderts a fracture into the first available index
uint AddFracture(const PotentialFracture *pf, __global PotentialFracture *fractures, volatile __global unsigned int *fractureIndices)
{
	//find lowest empty index
	uint i = NextFreeFractureIndex(0, fractureIndices);
	
	//printf("Adding Fracture for cluster %d at index %d. Fracture indices now look like: %d.\n",pf->cIndex, i, fractureIndices[0]);

	//Write cluster out to global memory
	fractures[i] = *pf;
	return i;
}

//Returns next free cluster index
uint NextFreeClusterIndex(uint start, volatile __global unsigned int *activeClusters)
{
	int i = start;
	bool valid = activeClusters[i / 32] & (0x1 << (i % 32));
	unsigned int current, expected, next;
	while (true)
	{
		current = activeClusters[i / 32];
		valid = current & (0x1 << (i % 32));

		// TODO - Less branching
		if (valid)
		{
			++i;
			continue;
		}
		//Make sure another thread doesn't take this index
		expected = current;
		next = expected | (0x1 << (i % 32));

		current = atomic_cmpxchg((volatile __global unsigned int*)&activeClusters[i / 32], expected, next);
		if (current == expected)
			break;
	}

	return i;
}

//Inserts cluster c into position i in buffer and sets the relevant bit to set it as active in activeClusters buffer
void AddClusterAt(const Cluster *c, uint i, __global Cluster *clusters, volatile __global unsigned int *activeClusters)
{
	// TODO - Maybe return an int so you can return an error if particle is already active?
	//Set particle as valid
	//printf("Adding cluster at index %d.\n", i);
	atomic_or((volatile __global unsigned int*)&activeClusters[i / 32], (0x1 << (i % 32)));
	clusters[i] = *c;
}

//Inderts a particle into the first available index
uint AddCluster(const Cluster *c, __global Cluster *clusters, volatile __global unsigned int *activeClusters)
{
	//find lowest empty index
	uint i = NextFreeClusterIndex(0, activeClusters);
	
	//printf("Adding cluster at index %d.\n", i);

	//Write cluster out to global memory
	clusters[i] = *c;
	return i;
}

//Returns the next available particle index 
uint NextFreeParticleIndex(uint start, volatile __global unsigned int *activeParticles)
{
	uint i = start;
	bool valid = activeParticles[i / 32] & (0x1 << (i % 32));
	unsigned int current, expected, next;
	while (true)
	{
		current = activeParticles[i / 32];
		valid = current & (0x1 << (i % 32));

		// TODO - Less branching
		if (valid)
		{
			++i;
			continue;
		}
		//Make sure another thread doesn't take this index
		expected = current;
		next = expected | (0x1 << (i % 32));

		current = atomic_cmpxchg((volatile __global unsigned int*)&activeParticles[i / 32], expected, next);
		if (current == expected)
			break;
	}
	return i;
}

//Inserts particle p into position i in buffer and sets the relevant bit to set it as active in activeParticles buffer
void AddParticleAt(const Particle *p, uint i, __global Particle *particles, volatile __global unsigned int *activeParticles)
{
	// TODO - Maybe return an int so you can return an error if particle is already active?
	//Set particle as valid
	atomic_or((volatile __global unsigned int*)&activeParticles[i / 32], (0x1 << (i % 32)) );
	particles[i] = *p;
}

//Inderts a particle into the first available index
uint AddParticle(const Particle *p, __global Particle *particles, volatile __global unsigned int *activeParticles)
{
	uint i = NextFreeParticleIndex(0, activeParticles);
	AddParticleAt(p, i, particles, activeParticles);
	return i;
}
/*
void quick_sort(int *a, int n) {
	int i, j, p, t;
	if (n < 2)
		return;
	p = a[n / 2];
	for (i = 0, j = n - 1;; i++, j--) {
		while (a[i] < p)
			i++;
		while (p < a[j])
			j--;
		if (i >= j)
			break;
		t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
	quick_sort(a, i);
	quick_sort(a + i, n - i);
}
*/
//TODO - Find out does OpenCL allow function pointers? 
//TODO - This is hugeley memory intensive!
int partition_members(ClusterMember *a, int n, const float3 *splitDirection, const float3 *worldCom, const __global struct Particle *particles) {
	ClusterMember t;
	int i = 0;
	int j = n - 1;

	if (n < 2)
		return -1;

//	printf("Partitioning cluster.\n");
	while (true)
	{
		float dotI = dot(*worldCom - particles[a[i].index].position, *splitDirection);
		float dotJ = dot(*worldCom - particles[a[j].index].position, *splitDirection);

		//decrement j until we find an unsorted element
		while (dotJ <= 0)
		{
			dotJ = dot(*worldCom - particles[a[--j].index].position, *splitDirection);
		}

		//decrement i until we find an unsorted element
		while (dotI > 0)
		{
			dotI = dot(*worldCom - particles[a[++i].index].position, *splitDirection);
		}

		if (i >= j)
			break;

		//swap elements
		t = a[i];
		a[i] = a[j];
		a[j] = t;
//		printf("i = %d\n.", i);
//		printf("j = %d\n.", j);
	}

	return i; //Return index of 1st element that failed predicate
}

int prioritise_fractures(PotentialFracture *a, int n) {
	PotentialFracture t;
//	int i = 0;
//	int j = n - 1;

	if (n < 2)
		return -1;

	for (int i = 0; i < n - 1; ++i)
	{
		for (int j = i; j < n; ++j)
		{
			float toughI = a[i].effectiveToughness;
			float toughJ = a[j].effectiveToughness;

			if (toughI > toughJ)
			{
				//swap elements
				t = a[i];
				a[i] = a[j];
				a[j] = t;
			}
		}
	}

	return 0;
}

// Atomic add for float types from this article: https://streamcomputing.eu/blog/2016-02-09/atomic-operations-for-floats-in-opencl-improved/
inline void atomicAdd_g_f(volatile __global float *addr, float val)
{
	union {
		uint	u32;
		float	f32;
	} next, expected, current;

	current.f32 = *addr;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 + val;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) addr, expected.u32, next.u32);

	} while (current.u32 != expected.u32);
}

// Atomic add for float types from this article: https://streamcomputing.eu/blog/2016-02-09/atomic-operations-for-floats-in-opencl-improved/
inline void atomicSub_g_f(volatile __global float *addr, float val)
{
	union {
		uint	u32;
		float	f32;
	} next, expected, current;

	current.f32 = *addr;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 - val;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) addr, expected.u32, next.u32);

	} while (current.u32 != expected.u32);
}

// Atomic multiply for float types from this article: https://streamcomputing.eu/blog/2016-02-09/atomic-operations-for-floats-in-opencl-improved/
inline void atomicMult_g_f(volatile __global float *addr, float val)
{
	union {
		uint	u32;
		float	f32;
	} next, expected, current;

	current.f32 = *addr;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 * val;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) addr, expected.u32, next.u32);

	} while (current.u32 != expected.u32);
}

// Atomic divide for float types from this article: https://streamcomputing.eu/blog/2016-02-09/atomic-operations-for-floats-in-opencl-improved/
inline void atomicDiv_g_f(volatile __global float *addr, float val)
{
	union {
		unsigned int u32;
		float		 f32;
	} next, expected, current;

	current.f32 = *addr;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 / val;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) addr, expected.u32, next.u32);

	} while (current.u32 != expected.u32);
}

//Hacked together atomic add for float3 vector types based on the above method
inline void atomicAdd_g_f3(volatile __global vector3f *addr, float3 val)
{
	union {
		unsigned int u32;
		float		 f32;
	} next, expected, current;

	current.f32 = addr->v.x;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 + val.x;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) &addr->a[0], expected.u32, next.u32);
	} while (current.u32 != expected.u32);

	current.f32 = addr->v.y;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 + val.y;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) &addr->a[1], expected.u32, next.u32);
	} while (current.u32 != expected.u32);

	current.f32 = addr->v.z;
	do
	{
		expected.f32 = current.f32;
		next.f32 = expected.f32 + val.z;

		current.u32 = atomic_cmpxchg((volatile __global unsigned int *) &addr->a[2], expected.u32, next.u32);
	} while (current.u32 != expected.u32);
}

#endif //UTILS_CL