#ifndef GPU_TYPES_H
#define GPU_TYPES_H

#ifdef __cplusplus
#include <Eigen/Eigen>
#include <CL/cl_platform.h>
#endif



#ifdef __cplusplus
typedef cl_uint uint;
#endif

typedef union
{
#ifdef __cplusplus
	cl_float3	v;
#else
	float3 v;
#endif
	float	a[4];
} vector3f;

typedef union
{
#ifdef __cplusplus
	cl_double3	v;
#else
	double3 v;
#endif
	double a[3];
} vector3d;


typedef struct {
	float x;
	float y;
	float z;
} vec3f;

typedef struct {
	float x;
	float y;
	float z;
} vec3d;

typedef struct {
	float x;
	float y;
	float z;
	float w;
} vec4f;

typedef struct {
	uint x;
	uint y;
	uint z;
}vec3ui;

typedef struct {
	int x;
	int y;
	int z;
}vec3i;

typedef struct {
	double a0, a1, a2;
	double b0, b1, b2;
	double c0, c1, c2;
}mat3d;

//TODO - Implement this with opencl vector types for SIMD instructions
typedef struct {
	float data[3][3];
}mat3f;

typedef struct {
	float data[3][3];
}mat2f;

#endif //GPU_TYPES_H