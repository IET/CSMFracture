//#include "particle.h"
//#include <time.h>
//#include <stdlib.h>
#include "ClusteringParams.h"
#include "GPUDataTypes.h"
#include "Particle.h"
#include "eigenSVD.cl"
#include "Utils.cl"

__kernel void AddParticles(__global Particle *particles, __global unsigned int *activeParticles, __global Particle *newParticles)
{
	uint i = get_global_id(0);

	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;

	Particle p = particles[i];

	if (i > 20130)
	{
		//uint nextAvailableIndex = NextFreeParticleIndex(i, activeParticles);
		//p.id = nextAvailableIndex;
		p.flags |= SPLIT;
//		newParticles[i] = p;
		printf("particle %d is adding a particle.\n", i);
		AddParticle(&p, particles, activeParticles);
	}
}

__kernel void WriteParticles(__global Particle *newParticles, __global Particle *particles)
{
	uint i = get_global_id(0);

	if (i > 200)
		return;

	if (newParticles[i].flags & SPLIT)
	{
		newParticles[i].flags = 0;
		particles[newParticles[i].id] = newParticles[i];
	}
}

__kernel void TestParticles(__global Particle *particles, __global uint *activeParticles)
{
	uint i = get_global_id(0);

	bool active = activeParticles[i / 32] & (0x1 << (i % 32));
	if (!active)
		return;
	if (particles[i].position.x == 0 && particles[i].position.y == 0 && particles[i].position.z == 0)
		printf("particles[%d].position = [%f, %f, %f]\n", i, particles[i].position.x, particles[i].position.y, particles[i].position.z);
}

__kernel void ParticleTestKernel(__global struct Particle *particles)
{
	uint i = get_global_id(0);
	if (i > 20131)
		return;

	printf("Particles %d x position = %f\n", i, particles[i].position.x);
	particles[i].position.x = 0.0f;
}

__kernel void SVDTestKernel(__constant mat3f *A, __global mat3f *U, __global mat3f *V, __global float3 *s)
{
	uint i = get_global_id(0);

	//Copy variables to private memory
	mat3f a = A[i];
	mat3f v = V[i];
	float3 S = s[i];
	mat3f u;

	//Preform SVD
	svdcmp(&a, &S, &u, &v);

	//Assign values to nuffers in global memory
	U[i] = a;
	V[i] = v;
	s[i] = S;
}

// Testing interfacing with Eigen
__kernel void TestEigenKernel(__global vec3f *vectors)
{
	uint x = get_global_id(0);

	uint width = get_global_size(0);

	vectors[x].x = vectors[x].x + (float)x;
}


// Testing a particle struct
__kernel void TestParticleKernel(__global struct Particle *particles)
{
	uint x = get_global_id(0);
	if (x > 20131)
		return;
	//printf("Kernel Id is: %d\n", x);
	uint width = get_global_size(0);
	//printf("Particle position is: (%f, %f, %f)\n", particles[x].position.x, particles[x].position.y, particles[x].position.z);
	//printf("Particle Mass is: %f\n\n", particles[x].mass);

	//particles[x].mass = particles[x].mass + 0.5f;

	particles[x].position.x = particles[x].position.x + 0.01f;
}

//Testing using cluster struct
__kernel void TestClusterKernel(__global struct Cluster *clusters)
{
	uint x = get_global_id(0);

	uint width = get_global_size(0);
	printf("Running test cluster kernel.\n");
	//clusters[x].mass = clusters[x].mass -20.0f;
	clusters[x].cstrain = 0.0f;
}