#include "world.h"
#include <fstream>

#include "json.h"
#include "utils.h"
#include "color_spaces.h"

#include <CL\cl_platform.h>

#include "enumerate.hpp"
using benlib::enumerate;
#include "range.hpp"
using benlib::range;


void World::loadFromJson(const std::string& _filename) {
	worldParameters.dragWithPlanes = true;
	elapsedTime = 0;
	filename = _filename;

	clusters.clear();
	particles.clear();

	std::ifstream ins(filename);

	Json::Value root;
	Json::Reader jReader;

	if (!jReader.parse(ins, root)) {
		std::cout << "couldn't read input file: " << filename << '\n'
			<< jReader.getFormattedErrorMessages() << std::endl;
		exit(1);
	}

	clusteringParams.neighborRadius = root.get("neighborRadius", 0.1f).asFloat();
	clusteringParams.nClusters = root.get("nClusters", 1).asInt();
	clusteringParams.neighborRadiusMax =
		root.get("neighborRadiusMax", std::numeric_limits<float>::max()).asFloat();
	clusteringParams.nClustersMax = root.get("nClustersMax", std::numeric_limits<int>::max()).asInt();
	clusteringParams.clusterItersMax = root.get("clusterItersMax", 10000).asInt();
	clusteringParams.clusteringAlgorithm = root.get("clusteringAlgorithm", 0).asInt();
	clusteringParams.clusterOverlap = root.get("clusterOverlap", 0.0f).asFloat();
	clusteringParams.clusterKernel = root.get("clusterKernel", 0).asInt();
	clusteringParams.kernelWeight = root.get("kernelWeight", 2.0f).asFloat();
	clusteringParams.blackhole = root.get("blackhole", 1.0f).asFloat();

	float mass = root.get("mass", 0.1f).asFloat();

	auto particleFilesIn = root["particleFiles"];
	for (auto i : range(particleFilesIn.size())) {
		auto particleInfo = readParticleFile(particleFilesIn[i].asString());

		std::vector<Particle> newParticles;
		newParticles.reserve(particleInfo.positions.size());

		for (const auto & pos : particleInfo.positions) {
			newParticles.emplace_back();
			newParticles.back().position = pos;
			newParticles.back().restPosition = pos;
			newParticles.back().velocity = Eigen::Vector3f::Zero();
			newParticles.back().mass = mass;
		}
		std::vector<Cluster> newClusters = iterateMakeClusters(newParticles, clusteringParams);
		mergeClusters(newParticles, newClusters);
	}

	auto movingParticleFilesIn = root["movingParticleFiles"];
	for (auto i : range(movingParticleFilesIn.size())) {
		auto& movingP = movingParticleFilesIn[i];
		auto particleInfo = readParticleFile(movingP["filename"].asString());
		Eigen::Vector3f offset = Eigen::Vector3f::Zero();
		float scale = movingP.get("scale", 1.0f).asFloat();
		auto& os = movingP["offset"];

		if (!os.isNull()) {
			offset.x() = os[0].asFloat();
			offset.y() = os[1].asFloat();
			offset.z() = os[2].asFloat();
		}

		Eigen::Vector3f velocity = Eigen::Vector3f::Zero();
		auto& vel = movingP["velocity"];

		if (!vel.isNull()) {
			velocity.x() = vel[0].asFloat();
			velocity.y() = vel[1].asFloat();
			velocity.z() = vel[2].asFloat();
		}

		std::vector<Particle> newParticles;
		newParticles.reserve(particleInfo.positions.size());
		for (const auto& pos : particleInfo.positions) {
			newParticles.emplace_back();
			newParticles.back().position = scale*pos + offset;
			newParticles.back().restPosition = scale*pos + offset;
			newParticles.back().velocity = velocity;
			newParticles.back().mass = mass;
		}

		auto newClusters = iterateMakeClusters(newParticles, clusteringParams);
		mergeClusters(newParticles, newClusters);
	}


/* unused in all our examples, and doesn't really belong, refactored to the vis stuff --Ben
	auto cameraPositionIn = root["cameraPosition"];
	if(!cameraPositionIn.isNull() && cameraPositionIn.isArray() && cameraPositionIn.size() == 3){
	cameraPosition.x() = cameraPositionIn[0].asFloat();
	cameraPosition.y() = cameraPositionIn[1].asFloat();
	cameraPosition.z() = cameraPositionIn[2].asFloat();
	} else {
	cameraPosition = Eigen::Vector3f{0,7, -5};
	}
	auto cameraLookAtIn = root["cameraLookAt"];
	if(!cameraLookAtIn.isNull() && cameraLookAtIn.isArray() && cameraLookAtIn.size() == 3){
	cameraLookAt.x() = cameraLookAtIn[0].asFloat();
	cameraLookAt.y() = cameraLookAtIn[1].asFloat();
	cameraLookAt.z() = cameraLookAtIn[2].asFloat();
	} else {
	cameraLookAt = Eigen::Vector3f::Zero();
	}

	auto cameraUpIn = root["cameraUp"];
	if(!cameraUpIn.isNull() && cameraUpIn.isArray() && cameraUpIn.size() == 3){
	cameraUp.x() = cameraUpIn[0].asFloat();
	cameraUp.y() = cameraUpIn[1].asFloat();
	cameraUp.z() = cameraUpIn[2].asFloat();
	} else {
	cameraUp = Eigen::Vector3f{0,1,0};
	}
*/

	worldParameters.dt = root.get("dt", 1 / 60.0f).asFloat();
	worldParameters.numConstraintIters = root.get("numConstraintIters", 5).asInt();
	worldParameters.alpha = root.get("alpha", 1.0f).asFloat();
	worldParameters.omega = root.get("omega", 1.0f).asFloat();
	worldParameters.gamma = root.get("maxStretch", 0.1f).asFloat();
	worldParameters.gamma = root.get("gamma", worldParameters.gamma).asFloat();
	worldParameters.springDamping = root.get("springDamping", 0.0f).asFloat();
	worldParameters.toughness = root.get("toughness", std::numeric_limits<float>::infinity()).asFloat();
	worldParameters.toughnessBoost = root.get("toughnessBoost", 0.0f).asFloat();
	worldParameters.toughnessFalloff = root.get("toughnessFalloff", std::numeric_limits<float>::infinity()).asFloat();

	// plasticity parameters
	worldParameters.yield = root.get("yield", 0.0f).asFloat();
	worldParameters.nu = root.get("nu", 0.0f).asFloat();
	worldParameters.hardening = root.get("hardening", 0.0f).asFloat();

	worldParameters.collisionRestitution = root.get("collisionRestitution", 0.5f).asFloat();
	worldParameters.collisionGeometryThreshold = root.get("collisionGeometryThreshold", 0.5f).asFloat();
	std::cout << "collisionGeometryThreshold " << worldParameters.collisionGeometryThreshold << std::endl;
	worldParameters.outlierThreshold = root.get("outlierThreshold", 2.0f).asFloat();

	auto fractureIn = root["fracture"];
	if (!fractureIn.isNull() && fractureIn.asString().compare("off") == 0) {
		fractureOn = false;
		std::cout << "Fracture off" << std::endl;
	}
	else
		fractureOn = true;

	auto delayRepeatedFractureIn = root["delayRepeatedFracture"];
	if (!delayRepeatedFractureIn.isNull() && delayRepeatedFractureIn.asString().compare("off") == 0) {
		worldParameters.delayRepeatedFracture = false;
		std::cout << "delayRepeatedFracture off" << std::endl;
	}
	else
		worldParameters.delayRepeatedFracture = true;

	auto selfCollisionsIn = root["selfCollisions"];
	if (!selfCollisionsIn.isNull() && selfCollisionsIn.asString().compare("off") == 0) {
		selfCollisionsOn = false;
		std::cout << "Self Collisions Off" << std::endl;
	}
	else
		selfCollisionsOn = true;

	auto& gravityIn = root["gravity"];
	if (!gravityIn.isNull() && gravityIn.isArray() && gravityIn.size() == 3) {
		worldParameters.gravity.x() = gravityIn[0].asFloat();
		worldParameters.gravity.y() = gravityIn[1].asFloat();
		worldParameters.gravity.z() = gravityIn[2].asFloat();
	}
	else {
		std::cout << "default gravity" << std::endl;
		worldParameters.gravity = Eigen::Vector3f{ 0.0f, -9.81f, 0.0f };
	}

	std::cout << "Processing Planes" << std::endl;

	auto& planesIn = root["planes"];
	for (auto i : range(planesIn.size())) {
		if (planesIn[i].size() != 4) {
			std::cout << "not a good plane... skipping" << std::endl;
			continue;
		}
		std::cout << "Adding a Plane" << std::endl;

		//x, y, z, (of normal), then offset value
		planes.push_back(Eigen::Vector4f{ 
			planesIn[i][0].asFloat(),
			planesIn[i][1].asFloat(),
			planesIn[i][2].asFloat(),
			planesIn[i][3].asFloat() });
		planes.back().head(3).normalize();
	}

	std::cout << "Processing Moving Planes" << std::endl;

	auto& movingPlanesIn = root["movingPlanes"];
	for (auto i : range(movingPlanesIn.size())) {
		auto normalIn = movingPlanesIn[i]["normal"];
		if (normalIn.size() != 3) {
			std::cout << "bad moving plane, skipping" << std::endl;
			continue;
		}
		Eigen::Vector3f normal(normalIn[0].asFloat(),
			normalIn[1].asFloat(),
			normalIn[2].asFloat());
		normal.normalize();
		movingPlanes.emplace_back(normal,
			movingPlanesIn[i]["offset"].asFloat(),
			movingPlanesIn[i]["velocity"].asFloat());

	}
	std::cout << movingPlanes.size() << " moving planes" << std::endl;

	std::cout << "Processing Twisting Planes" << std::endl;

	auto& twistingPlanesIn = root["twistingPlanes"];
	for (auto i : range(twistingPlanesIn.size())) {
		auto normalIn = twistingPlanesIn[i]["normal"];
		if (normalIn.size() != 3) {
			std::cout << "bad twisting plane, skipping" << std::endl;
			continue;
		}
		Eigen::Vector3f normal(normalIn[0].asFloat(),
			normalIn[1].asFloat(),
			normalIn[2].asFloat());
		normal.normalize();
		twistingPlanes.emplace_back(normal,
			twistingPlanesIn[i]["offset"].asFloat(),
			twistingPlanesIn[i]["angularVelocity"].asFloat(),
			twistingPlanesIn[i]["width"].asFloat(),
			twistingPlanesIn[i].get("lifetime", std::numeric_limits<float>::max()).asFloat());

	}
	std::cout << twistingPlanes.size() << " twisting planes" << std::endl;

	std::cout << "Processing Tilting Planes" << std::endl;

	auto& tiltingPlanesIn = root["tiltingPlanes"];
	for (auto i : range(tiltingPlanesIn.size())) {
		auto normalIn = tiltingPlanesIn[i]["normal"];
		if (normalIn.size() != 3) {
			std::cout << "bad tilting plane, skipping" << std::endl;
			continue;
		}
		Eigen::Vector3f normal(normalIn[0].asFloat(),
			normalIn[1].asFloat(),
			normalIn[2].asFloat());
		normal.normalize();
		auto tiltIn = tiltingPlanesIn[i]["tilt"];
		if (tiltIn.size() != 3) {
			std::cout << "bad tilting plane, skipping" << std::endl;
			continue;
		}
		Eigen::Vector3f tilt(tiltIn[0].asFloat(),
			tiltIn[1].asFloat(),
			tiltIn[2].asFloat());
		tilt.normalize();

		tiltingPlanes.emplace_back(normal, tilt,
			tiltingPlanesIn[i]["offset"].asFloat(),
			tiltingPlanesIn[i]["angularVelocity"].asFloat(),
			tiltingPlanesIn[i]["width"].asFloat(),
			tiltingPlanesIn[i].get("lifetime", std::numeric_limits<float>::max()).asFloat());

	}
	std::cout << tiltingPlanes.size() << " tilting planes" << std::endl;

	std::cout << "Processing Projectiles" << std::endl;

	auto& projectilesIn = root["projectiles"];
	for (auto i : range(projectilesIn.size())) {
		auto& projectile = projectilesIn[i];
		Eigen::Vector3f start(projectile["start"][0].asFloat(),
			projectile["start"][1].asFloat(),
			projectile["start"][2].asFloat());
		Eigen::Vector3f vel(projectile["velocity"][0].asFloat(),
			projectile["velocity"][1].asFloat(),
			projectile["velocity"][2].asFloat());

		projectiles.emplace_back(start, vel, projectile["radius"].asFloat(),
			projectile.get("momentumScale", 0).asFloat());

	}

	std::cout << "Processing Cylinders" << std::endl;

	auto& cylindersIn = root["cylinders"];
	for (auto i : range(cylindersIn.size())) {
		auto& cylinder = cylindersIn[i];
		Eigen::Vector3f normal(cylinder["normal"][0].asFloat(),
			cylinder["normal"][1].asFloat(),
			cylinder["normal"][2].asFloat());

		Eigen::Vector3f supportPoint(cylinder["supportPoint"][0].asFloat(),
			cylinder["supportPoint"][1].asFloat(),
			cylinder["supportPoint"][2].asFloat());


		cylinders.emplace_back(normal, supportPoint, cylinder["radius"].asFloat());
	}



	//do this in mergeClusters instead
	//for(int i=0; i<(int)particles.size(); i++) particles[i].id = i;

	for (auto& p : particles) 
		p.outsideSomeMovingPlane = false;

	for (auto& movingPlane : movingPlanes) {
		for (auto& p : particles) {
			p.outsideSomeMovingPlane |= movingPlane.outside(p);
		}
	}

	for (auto& twistingPlane : twistingPlanes) {
		for (auto& p : particles) {
			p.outsideSomeMovingPlane |= twistingPlane.outside(p);
		}
	}

	for (auto& tiltingPlane : tiltingPlanes) {
		for (auto& p : particles) {
			p.outsideSomeMovingPlane |= tiltingPlane.outside(p);
		}
	}

	std::cout << "Processing Initial Stretch" << std::endl;

	Eigen::Vector3f initialStretch;
	auto& initialStretchIn = root["initialStretch"];
	if (!initialStretchIn.isNull() && initialStretchIn.isArray() && initialStretchIn.size() == 3) {
		initialStretch.x() = initialStretchIn[0].asFloat();
		initialStretch.y() = initialStretchIn[1].asFloat();
		initialStretch.z() = initialStretchIn[2].asFloat();
	}
	else {
		initialStretch = Eigen::Vector3f{ 1.0f, 1.0f, 1.0f };
	}
	
	std::cout << "Updating Clusters" << std::endl;

	for (auto& c : clusters) {
		Eigen::Matrix3f Aqq;
		Aqq.setZero();
		for (int i = 0; i < c.numMembers; ++i) {
			auto &member = c.members[i];
			assert(member.index > -1);
			auto &p = particles[member.index];
			Eigen::Vector3f qj = p.restPosition - c.restCom;
			Aqq += qj * qj.transpose();
		}
		Eigen::JacobiSVD<Eigen::Matrix3f> solver(Aqq, Eigen::ComputeFullU | Eigen::ComputeFullV);

		float min, max;
		Eigen::Vector3f n;
		for (unsigned int i = 0; i<3; i++) {
			n = solver.matrixV().col(i);
			min = std::numeric_limits<float>::max();
			max = -std::numeric_limits<float>::max();
			for (int i = 0; i < c.numMembers; ++i) {
				auto &member = c.members[i];
				assert(member.index > -1);
				auto &p = particles[member.index];
				float d = n.dot(p.restPosition);
				if (d < min)
					min = d;
				if (d > max)
					max = d;
			}
			float center = n.dot(c.cg.c);
			//std::cout<<max - min<<" "<<neighborRadius<<std::endl;
			//if (max - min < 1.5f*neighborRadius) {
			if (max - center < worldParameters.collisionGeometryThreshold*neighborRadius) {
				std::cout << " adding plane: " << n(0) << "x + " << n(1) << "y + " << n(2) << "z + " << -max << std::endl;
				c.cg.addPlane(n, -max);
			}

			if (center - min < worldParameters.collisionGeometryThreshold*neighborRadius) {
				std::cout << " adding plane: " << -n(0) << "x + " << -n(1) << "y + " << -n(2) << "z + " << min << std::endl;
				c.cg.addPlane(-n, min);
			}
		}
	}

	updateClusterProperties(benlib::range(clusters.size()));
	std::cout << "Setting up plane constraints" << std::endl;

	for (auto& c : clusters) {
		if (c.mass < 1e-5f) {
			std::cout << "Cluster has mass " << c.mass << " and position: " << c.restCom << std::endl;
			exit(0);
		}
	}

	setupPlaneConstraints();
	std::cout << "Colouring Particles" << std::endl;

	//color the particles... hopefully the clusters have the COMS correct here
	for (auto& p : particles) {
		assert(p.numClusters <= MAX_CLUSTERS);
		std::vector<int> pClusters(p.numClusters);


		for (int k = 0; k < p.numClusters; ++k)
			pClusters.push_back(p.clusters[k].index);

		auto closestCluster = std::min_element(
			pClusters.begin(), pClusters.end(),
			[&p, this](int a, int b) {
			return (p.position - clusters[a].worldCom).squaredNorm() <
				(p.position - clusters[b].worldCom).squaredNorm();
		});

		HSLColor color(2.0f*((float)acos(-1))*(*closestCluster % 12) / 12.0f, 0.7f, 0.7f);
		int clusterIndex = std::distance(pClusters.begin(), closestCluster);
		RGBColor rgb = color.to_rgb();

		//clusters[p.clusters[clusterIndex]].color = {rgb.r, rgb.g, rgb.b};
		
		RGBColor colorRgb = color.to_rgb();
		p.color.s[0] = colorRgb.r; 
		p.color.s[1] = colorRgb.g;
		p.color.s[2] = colorRgb.b;
	}

	for (auto &p : particles) {
		p.position.x() *= initialStretch[0];
		p.position.y() *= initialStretch[1];
		p.position.z() *= initialStretch[2];
	}

	//apply initial rotation/scaling, etc
	//todo, read this from json
	//Eigen::Vector3f axis{0,0,1};
	//for (auto &p : particles) {
	//p.position.x() *= 2.5f;
	//p.velocity = 0.5f*p.position.cross(axis);
	//auto pos = p.position;
	//p.position[0] = 0.36f*pos[0] + 0.48f*pos[1] - 0.8f*pos[2];
	//p.position[1] = -0.8f*pos[0] + 0.6f*pos[1];// - 0.8f*pos[2];
	//p.position[2] = 0.48f*pos[0] + 0.64f*pos[1] + 0.6f*pos[2];
	//}

	numClusters = clusters.size();
	numParticles = particles.size();
	worldParameters.fractureOn = fractureOn;
}




World::ParticleSet World::readParticleFile(const std::string& _filename) {
	std::ifstream ins(_filename);

	ParticleSet ret;

	Eigen::Vector3f pos;
	ins >> pos.x() >> pos.y() >> pos.z();

	ret.bbMin = pos;
	ret.bbMax = pos;

	while (ins) {
		ret.positions.push_back(pos);

		ret.bbMin - ret.bbMin.cwiseMin(pos);
		ret.bbMax - ret.bbMax.cwiseMax(pos);

		ins >> pos.x() >> pos.y() >> pos.z();
	}

	std::cout << "total particles in file " << _filename << ": " << ret.positions.size() << std::endl;
	std::cout << "bounding box: [" << ret.bbMin[0] << ", " << ret.bbMin[1] << ", " << ret.bbMin[2];
	std::cout << "] x [" << ret.bbMax[0] << ", " << ret.bbMax[1] << ", " << ret.bbMax[2] << "]" << std::endl;
	return ret;
}

void World::saveParticleFile(const std::string& _filename) const {
	std::ofstream outs(_filename);

	Eigen::Vector3f pos;
	for (auto& p : particles) {
		outs << p.position.x() << " "
			<< p.position.y() << " "
			<< p.position.z() << std::endl;
	}
	outs.close();
}

void World::printCOM() const {
	Eigen::Vector3f worldCOM =
		std::accumulate(particles.begin(), particles.end(),
			Eigen::Vector3f{ 0,0,0 },
			[](Eigen::Vector3f acc, const Particle& p) {
		return acc + p.mass*p.position;
	});

	float totalMass = std::accumulate(particles.begin(), particles.end(),
		0.0f,
		[](float acc, const Particle& p) {
		return acc + p.mass;
	});

	worldCOM /= totalMass;
	std::cout << worldCOM.x() << std::endl;;
}


void World::dumpParticlePositions(const std::string& filename) const {
	std::ofstream outs(filename, std::ios_base::binary | std::ios_base::out);
	size_t numParticles = particles.size();
	outs.write(reinterpret_cast<const char*>(&numParticles), sizeof(numParticles));
	std::vector<float> positions(3 * numParticles);
	for (auto i : range(particles.size())) {
		positions[3 * i] = particles[i].position.x();
		positions[3 * i + 1] = particles[i].position.y();
		positions[3 * i + 2] = particles[i].position.z();
	}
	outs.write(reinterpret_cast<const char*>(positions.data()),
		3 * numParticles*sizeof(typename decltype(positions)::value_type));
}

void World::dumpColors(const std::string& filename) const {
	std::ofstream outs(filename, std::ios_base::binary | std::ios_base::out);
	size_t numParticles = particles.size();
	outs.write(reinterpret_cast<const char*>(&numParticles), sizeof(numParticles));
	std::vector<float> colors(3 * numParticles);
	for (auto&& pr : benlib::enumerate(particles)) {
		/*	//find nearest cluster
		auto i = pr.first;
		auto& p = pr.second;

		int min_cluster = p.clusters[0];
		auto com = clusters[min_cluster].worldCom;
		Eigen::Vector3f dir = p.position - com;
		float min_sqdist = dir.squaredNorm();
		for (auto& cInd : p.clusters) {
		com = clusters[cInd].worldCom;//computeNeighborhoodCOM(clusters[cInd]);
		dir = p.position - com;
		float dist = dir.squaredNorm();
		if (dist < min_sqdist) {
		min_sqdist = dist;
		min_cluster = cInd;
		}
		}

		RGBColor rgb = HSLColor(2.0f*acos(-1)*(min_cluster%12)/12.0f, 0.7f, 0.7f).to_rgb();*/
		//RGBColor rgb = HSLColor(2.0f*acos(-1)*min_cluster/clusters.size(), 0.7f, 0.7f).to_rgb();
		//if(clusters[min_cluster].members.size() > 1) {
		//      sqrt(min_sqdist) < 0.55f*clusters[min_cluster].renderWidth) {
		//glColor4d(rgb.r, rgb.g, rgb.b, 0.8f);
		auto i = pr.first;
		auto& p = pr.second;
		colors[3 * i] = p.color.s[0];//rgb.r;
		colors[3 * i + 1] = p.color.s[1];//rgb.g;
		colors[3 * i + 2] = p.color.s[2];//rgb.b;
									  //always use the color...
									  /*} else {
									  colors[3*i] = 1;
									  colors[3*i + 1] = 1;
									  colors[3*i + 2] = 1;
									  }*/
	}
	outs.write(reinterpret_cast<const char*>(colors.data()),
		3 * numParticles*sizeof(float));
}


void World::dumpClippedSpheres(const std::string& filename) const {
	std::ofstream outs(filename);

	outs << clusters.size() << '\n';
	for (const auto& cluster : clusters) {
		auto visTrans = cluster.getVisTransform();

		//Eigen::Vector3f newCenter = cluster.cg.c + visTrans.col(3).head(3);
		Eigen::Vector4f newCenter(cluster.cg.c(0), cluster.cg.c(1), cluster.cg.c(2), 1.0f);
		newCenter = visTrans * newCenter;
/*
		outs << cluster.cg.planes.size() << '\n'
			<< newCenter.x() << ' '
			<< newCenter.y() << ' '
			<< newCenter.z() << '\n'
			<< cluster.cg.r << '\n';

		for (const auto& plane : cluster.cg.planes) {
			//Eigen::Vector3f newNormal = visTrans.block(0,0,3,3)*plane.first;
			// outs << newNormal.x() << ' '
			//	   << newNormal.y() << ' '
			//	   << newNormal.z() << ' '
			//	   << plane.second << '\n';
			Eigen::Vector4f norm4(plane.first(0), plane.first(1), plane.first(2), 0);
			Eigen::Vector4f pt4(cluster.cg.c(0), cluster.cg.c(1), cluster.cg.c(2), 1);
			float d = norm4.dot(pt4) + plane.second;
			pt4 = pt4 - d*norm4;

			norm4 = visTrans * norm4;
			pt4 = visTrans * pt4;

			Eigen::Vector3f norm3(norm4(0), norm4(1), norm4(2));
			Eigen::Vector3f pt3(pt4(0), pt4(1), pt4(2));
			d = norm3.dot(pt3);

			outs << norm3.x() << ' '
				<< norm3.y() << ' '
				<< norm3.z() << ' '
				<< -d << '\n';
		}
*/
	}
}


void World::setupPlaneConstraints() {

	for (auto& c : clusters) {
		bool crossingPlane = false;
		for (auto& plane : movingPlanes) {
			bool firstSide = particles[c.members[0].index].position.dot(plane.normal) > plane.offset;

			for (int i = 0; i < c.numMembers; ++i) {
				auto &member = c.members[i];
				assert(member.index > -1);
				bool thisSide = particles[member.index].position.dot(plane.normal) > plane.offset;
				if (thisSide != firstSide) {
					crossingPlane = true;
					break;
				}
			}

			if (crossingPlane)
			{
				break;
			}
		}

		for (auto& plane : twistingPlanes) {
			if (crossingPlane)
			{
				break;
			}

			bool firstSide = particles[c.members[0].index].position.dot(plane.normal) > plane.offset;

			for (int i = 0; i < c.numMembers; ++i) {
				auto& member = c.members[i];
				assert(member.index > -1);
				bool thisSide = particles[member.index].position.dot(plane.normal) > plane.offset;
				if (thisSide != firstSide) {
					crossingPlane = true;
					break;
				}
			}
		}
		for (auto& plane : tiltingPlanes) {
			if (crossingPlane)
			{
				break;
			}
			bool firstSide = particles[c.members[0].index].position.dot(plane.normal) > plane.offset;

			for (int i = 0; i < c.numMembers; ++i) {
				auto &member = c.members[i];
				assert(member.index > -1);
				bool thisSide = particles[member.index].position.dot(plane.normal) > plane.offset;
				if (thisSide != firstSide) {
					crossingPlane = true;
					break;
				}
			}
		}
		if (crossingPlane) {
			c.toughness = 10 * worldParameters.toughness;//std::numeric_limits<float>::infinity();
		}
		else {
			c.toughness = worldParameters.toughness;
		}
	}
}

void World::mergeClusters(const std::vector<Particle>& newParticles,
	const std::vector<Cluster>& newClusters) {

	auto particleStart = particles.size();
	auto clusterStart = clusters.size();

	//stick them in there and update indices
	particles.insert(particles.end(), newParticles.begin(), newParticles.end());
	for (auto i : range(particleStart, particles.size())) {
		for (auto& c : particles[i].clusters) {
			c.index += clusterStart;
		}
	}

	clusters.insert(clusters.end(), newClusters.begin(), newClusters.end());
	for (auto i : range(clusterStart, clusters.size())) {
		for (int j = 0; j < clusters[i].numMembers; ++j) {
			assert(clusters[i].members[j].index > -1);
			assert(clusters[i].numMembers < MAX_MEMBERS && clusters[i].numMembers > 0);
			clusters[i].members[j].index += particleStart;
		}
	}
}
