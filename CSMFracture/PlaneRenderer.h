#pragma once
#include <Eigen\Eigen>
#include "Shader.hpp"
#include "ShaderManager.hpp"
#include "tiltingPlane.h"

class PlaneRenderer
{
private:
	static GLuint VAO;
	static GLuint VBO, EBO;
	static Shader *shader;
	static bool initialised;

public:

	static GLfloat vertices[24];

	static GLuint indices[6];
	
	static void DrawPlane(Eigen::Matrix4f &v, Eigen::Matrix4f &p, Eigen::Vector3f &position, Eigen::Vector3f &normal, const float &size, Eigen::Vector4f &color);
	//Tilting planes
	static void DrawPlane(Eigen::Matrix4f &v, Eigen::Matrix4f &p, Eigen::Vector3f &position, const TiltingPlane &tp, const float &t, const float &size, Eigen::Vector4f &color);

	static void Init();
	~PlaneRenderer();
};

