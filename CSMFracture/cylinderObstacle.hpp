#pragma once

#include <Eigen/Dense>

struct Particle;

class CylinderObstacle{

public:
  Eigen::Vector3f normal, supportPoint;
  float radius;
  CylinderObstacle(const Eigen::Vector3f& _normal, const Eigen::Vector3f& _supportPoint, float _radius)
	:normal(_normal.normalized()), supportPoint{_supportPoint}, radius{_radius}
  {}

  void bounceParticle(Particle& particle) const;
};
