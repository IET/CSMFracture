#include "PlaneRenderer.h"
#include "tiltingPlane.h"
										//Vertices			//Normals
GLfloat PlaneRenderer::vertices[24] = {	-1.0f, -1.0f, 0.0f,	0.0f, 0.0f, 1.0f,
										 1.0f, -1.0f, 0.0f,	0.0f, 0.0f, 1.0f,
										-1.0f,  1.0f, 0.0f,	0.0f, 0.0f, 1.0f,
										 1.0f, 1.0f, 0.0f,	0.0f, 0.0f, 1.0f };
GLuint PlaneRenderer::VAO;
GLuint PlaneRenderer::VBO;
GLuint PlaneRenderer::EBO;
Shader *PlaneRenderer::shader;

GLuint PlaneRenderer::indices[6] = {	0, 1, 2,
										2, 1, 3 };
bool PlaneRenderer::initialised = false;

void PlaneRenderer::Init()
{
	PlaneRenderer::shader = ShaderManager::loadShader("plane");

	glGenVertexArrays(1, &PlaneRenderer::VAO);
	glGenBuffers(1, &PlaneRenderer::VBO);
	glGenBuffers(1, &PlaneRenderer::EBO);

	glBindVertexArray(PlaneRenderer::VAO);
	glBindBuffer(GL_ARRAY_BUFFER, PlaneRenderer::VBO);
	glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(GLfloat), &PlaneRenderer::vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, PlaneRenderer::EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLint), &PlaneRenderer::indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));		//3

	glBindVertexArray(0);

	PlaneRenderer::initialised = true;
}

void PlaneRenderer::DrawPlane(Eigen::Matrix4f &v, Eigen::Matrix4f &p, Eigen::Vector3f &position, Eigen::Vector3f &normal, const float &size, Eigen::Vector4f &color)
{
	if (!PlaneRenderer::initialised)
		Init();

	// TODO - Need to bind uniforms for normals and colors
	//TODO - Also determine orientation from the normal
	PlaneRenderer::shader->enableShader();

	normal.normalize();
	Eigen::Vector3f right = normal.cross(Eigen::Vector3f(0, 1, 0));
	right.normalize();

	Eigen::Vector3f up = normal.cross(right);
	up.normalize();

	Eigen::Matrix4f m;

	m << right.x() * size,	up.x(),			normal.x(),			position.x(),
		right.y(),			up.y() * size,	normal.y(),			position.y(),
		right.z(),			up.z(),			normal.z() * size,	position.z(),
		0.0f,				0.0f,			0.0f,				1.0f;

	PlaneRenderer::shader->setUniformVector4fv("color", color);
	PlaneRenderer::shader->setUniformMatrix4fv("modelMat", m);
	PlaneRenderer::shader->setUniformMatrix4fv("viewMat", v);
	PlaneRenderer::shader->setUniformMatrix4fv("projectionMat", p);

	// Draw mesh
	glBindVertexArray(PlaneRenderer::VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	
	PlaneRenderer::shader->disableShader();
}

//	Eigen::Vector3f position = (normal * world.movingPlanes[i].offset) + (world.movingPlanes[i].velocity * elapsedTime * normal);
//	Renderer::DrawPlane(view, projection, position, normal, PLANE_SIZE, Eigen::Vector4f(0.6f, 0.2f, 0.2f, 1.0f));


void PlaneRenderer::DrawPlane(Eigen::Matrix4f &v, Eigen::Matrix4f &p, Eigen::Vector3f &position, const TiltingPlane &tp, const float &t, const float &size, Eigen::Vector4f &color)
{
	if (!PlaneRenderer::initialised)
		Init();

	// tilt = right vector
	// tangent = up vector

	float time = t < tp.lifetime ? t : tp.lifetime;

	float rOffset = time * tp.angularVelocity;

	// TODO - Need to bind uniforms for normals and colors
	//TODO - Also determine orientation from the normal
	PlaneRenderer::shader->enableShader();

	//tp.normal.normalize();
	Eigen::Vector3f tangent = tp.tilt.cross(tp.normal);

	Eigen::AngleAxisf rot(rOffset, tp.tilt);


	Eigen::Matrix3f rotMat = rot.matrix();
	Eigen::Matrix4f rotMat4;

	Eigen::Matrix4f m;

	rotMat4 << 
		rotMat(0,0),	rotMat(0, 1),	rotMat(0, 2), 0,
		rotMat(1, 0),	rotMat(1, 1),	rotMat(1, 2), 0,
		rotMat(2, 0),	rotMat(2, 1),	rotMat(2, 2), 0,
		0.0f, 0.0f, 0.0f, 1.0f;

	m << tp.tilt.x(),	tangent.x(),	tp.normal.x(),	0,
		tp.tilt.y(),	tangent.y(),	tp.normal.y(),	0,
		tp.tilt.z(),	tangent.z(),	tp.normal.z(),	0,
		0.0f, 0.0f, 0.0f, 1.0f;

	m = rotMat4 * m;

	m(0, 0) *= size;
	m(1, 1) *= size;
	m(2, 2) *= size;

	m(0, 3) = position.x();
	m(1, 3) = position.y();
	m(2, 3) = position.z();


	PlaneRenderer::shader->setUniformVector4fv("color", color);
	PlaneRenderer::shader->setUniformMatrix4fv("modelMat", m);
	PlaneRenderer::shader->setUniformMatrix4fv("viewMat", v);
	PlaneRenderer::shader->setUniformMatrix4fv("projectionMat", p);

	// Draw mesh
	glBindVertexArray(PlaneRenderer::VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	PlaneRenderer::shader->disableShader();
}

PlaneRenderer::~PlaneRenderer()
{
}


