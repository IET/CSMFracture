#include "world.h"
#include <fstream>

#include "utils.h"

#include <random>
#include <iostream>

#include "accelerationGrid.h"

#include "enumerate.hpp"
using benlib::enumerate;
#include "range.hpp"
using benlib::range;

inline float sqr(const float &x) { return x*x; }

void World::timestep() {

	for (auto& twistingPlane : twistingPlanes) {
		for (auto& p : particles) {
			if (twistingPlane.outside(p) && twistingPlane.lifetime < elapsedTime)
				p.outsideSomeMovingPlane = false;
		}
	}

	for (auto& tiltingPlane : tiltingPlanes) {
		for (auto& p : particles) {
			if (tiltingPlane.outside(p) && tiltingPlane.lifetime < elapsedTime)
				p.outsideSomeMovingPlane = false;
		}
	}

	if (worldParameters.toughnessBoost > 0.0f) {
		for (auto &c : clusters) {
			c.toughness = worldParameters.toughness * (1.0f + worldParameters.toughnessBoost*exp(-worldParameters.toughnessFalloff*c.timeSinceLastFracture));
			c.timeSinceLastFracture += worldParameters.dt;
		}
	}

	//scope block for profiler
	std::vector<FractureInfo> potentialSplits;
	{
		auto timer = prof.timeName("dynamics");
		for (auto& p : particles) {
			p.oldPosition = p.position;
			p.goalPosition.setZero();
			p.goalVelocity.setZero();
		}

		for (auto&& en : benlib::enumerate(clusters)) {
			auto& cluster = en.second;
			cluster.worldCom = sumWeightedWorldCOM(cluster.members, cluster.numMembers);
			Eigen::Vector3f clusterVelocity = computeClusterVelocity(cluster);
			Eigen::Matrix3f Apq = computeApq(cluster);
			Eigen::Matrix3f A = Apq*cluster.aInv;
			

			if (worldParameters.nu > 0.0f)
				A = A*cluster.Fp.inverse(); // plasticity

			//do the SVD here so we can handle fracture stuff
			Eigen::JacobiSVD<Eigen::Matrix3f> solver(A,
				Eigen::ComputeFullU | Eigen::ComputeFullV);

			Eigen::Matrix3f U = solver.matrixU(), V = solver.matrixV();
			Eigen::Vector3f sigma = solver.singularValues();
			if (fabs(sigma(0) - 1.0f) > cluster.toughness && !cluster.justFractured) {
				potentialSplits.push_back({ en.first,
					sigma(0) - cluster.toughness,
					V.col(0) });
				std::cout << "Cluster " << en.first << " is about to fracture!" << std::endl;
			}
			else if (cluster.justFractured && fabs(sigma(0) - 1.0f) <= cluster.toughness)
			{
				//can this just be an else? --Ben
				cluster.justFractured = false;
			}

			{
				//auto timer = prof.timeName("plasticity");
				cluster.updatePlasticity(sigma, U, V, worldParameters.yield, worldParameters.nu, worldParameters.hardening);
			}

			Eigen::Matrix3f T = U*V.transpose();
			if (worldParameters.nu > 0.0f) 
				T = T*cluster.Fp; // plasticity

			Eigen::Vector3f po, gP, gV;
			for (int i = 0; i < cluster.numMembers; ++i) {
				auto &member = cluster.members[i];
				assert(member.index > -1);
				auto &p = particles[member.index];
				float w = member.weight / p.totalweight;
				p.goalPosition += w*(T*(p.restPosition - cluster.restCom) + cluster.worldCom);
				p.goalVelocity += w*clusterVelocity;

				gP = p.goalPosition;
				gV = p.goalVelocity;
			}
		} // for clusters

		assertFinite();

		Eigen::Vector3f vel;
		for (auto& p : particles) {
			if (p.numClusters > 0) {
				//p.goalPosition /= p.numClusters;
				//p.goalVelocity /= p.numClusters;
			}
			else {
				p.goalPosition = p.position;
				p.goalVelocity = p.velocity;
			}
			if (!p.outsideSomeMovingPlane) {
				p.velocity += worldParameters.dt * worldParameters.gravity + (worldParameters.alpha / worldParameters.dt)*(p.goalPosition - p.position) +
					worldParameters.springDamping * (p.goalVelocity - p.velocity);
			}
			vel = p.velocity;
			p.position += worldParameters.dt * p.velocity;
		}

		assertFinite();

		for (auto iter : range(worldParameters.numConstraintIters)) {
			(void)iter; //unused
			strainLimitingIteration();
		}

		for (auto& p : particles) {
			p.velocity = (1.0f / worldParameters.dt)*(p.position - p.oldPosition);		
		}
		for (auto &c : clusters)
			c.worldCom = sumWeightedWorldCOM(c.members, c.numMembers);

		assertFinite();

	}	//End dynamics scope block

	//std::cout<<"doFracture"<<std::endl;

	if (fractureOn) {
		auto timer = prof.timeName("fracture");

		doFracture(std::move(potentialSplits));
		//std::cout<<"splitoutliers"<<std::endl;
		splitOutliers();
		//std::cout<<"cullsmallclusters"<<std::endl;
		cullSmallClusters();
		//std::cout<<"removelonelyparticles"<<std::endl;
		removeLonelyParticles();
		//std::cout<<"updateClusterProperties"<<std::endl;
	}
	
	updateClusterProperties(range(clusters.size()));

	//std::cout<<"updateTransforms"<<std::endl;
	for (auto &c : clusters)
	{
		updateTransforms(c);
	}

	//std::cout<<"selfCollisions"<<std::endl;
	//if (selfCollisionsOn)
	//	selfCollisions();

	bounceOutOfPlanes();

	for (auto& c : clusters) {
		c.Fp = c.FpNew;
		c.worldCom = sumWeightedWorldCOM(c.members, c.numMembers);
	}

	elapsedTime += worldParameters.dt;
	//std::cout << "elapsed time: " << elapsedTime << std::endl;
}

void World::strainLimitingIteration() {
	const float gammaSquared = worldParameters.gamma * worldParameters.gamma;
	for (auto& p : particles) {
		p.goalPosition.setZero();
	}

	for (auto& c : clusters) {
		c.worldCom = sumWeightedWorldCOM(c.members, c.numMembers);
		Eigen::Matrix3f Apq = computeApq(c);
		Eigen::Matrix3f A = Apq*c.aInv;
		if (worldParameters.nu > 0.0f)
			A = A*c.Fp.inverse(); // plasticity

		auto pr = utils::polarDecomp(A);
		Eigen::Matrix3f T = pr.first;

		if (worldParameters.nu > 0.0f)
			T = T * c.Fp;

		for (int i = 0; i < c.numMembers; ++i) {
			
			auto &member = c.members[i];
			assert(member.index > -1);
			auto &q = particles[member.index];
			Eigen::Vector3f rest = (q.restPosition - c.restCom);
			Eigen::Vector3f goal = T*(rest)+c.worldCom;
			float ratio = (goal - q.position).squaredNorm() / (c.width*c.width);

			Eigen::Vector3f diff = q.goalPosition;

			if (ratio > gammaSquared) {
				q.goalPosition +=
					(member.weight / q.totalweight) *
					(goal + sqrt(gammaSquared / ratio) *
						(q.position - goal));
			}
			else {
				q.goalPosition += (member.weight / q.totalweight) * q.position;
			}
		}
	} //for clusters

	int particleIndex = 0;
	for (auto& p : particles) {
		if (p.numClusters == 0) {
			p.goalPosition = p.position;
		}

		p.position = worldParameters.omega*p.goalPosition + (1.0f - worldParameters.omega)*p.position;
		particleIndex++;
	}
}

///////////////////////////////
// FRACTURE
///////////////////////////////

void World::doFracture(std::vector<World::FractureInfo> &potentialSplits)
{
	if (!potentialSplits.empty()) {
		std::cout << "potential splits: " << potentialSplits.size() << std::endl;
	}
	else
		return;

	auto start = std::chrono::high_resolution_clock::now();
	auto timer = prof.timeName("fracture");

	benlib::Profiler fractureProf;
	//do fracture
	std::sort(potentialSplits.begin(), potentialSplits.end(),
		[](const FractureInfo& a, const FractureInfo& b) {
		return a.effectiveToughness < b.effectiveToughness;
	});

	bool aCancel = false;
	for (auto &ps : potentialSplits)
	{
		auto setupTimer = fractureProf.timeName("setup");
		size_t cIndex = ps.clusterIndex;
		std::cout << "Fracturing cluster " << cIndex << std::endl;

		auto worldCOM = clusters[cIndex].worldCom;

		//recompute A matrix:
		Eigen::Matrix3f Apq = computeApq(clusters[cIndex]);
		Eigen::Matrix3f A = Apq*clusters[cIndex].aInv;
		if (worldParameters.nu > 0.0f)
			A = A*clusters[cIndex].Fp.inverse(); // plasticity

		//do the SVD here so we can handle fracture stuff
		Eigen::JacobiSVD<Eigen::Matrix3f> solver(A, Eigen::ComputeFullU | Eigen::ComputeFullV);

		Eigen::Vector3f sigma = solver.singularValues();
		if (fabs(sigma(0) - 1) < clusters[cIndex].toughness || clusters[cIndex].justFractured) {
			if (!aCancel) {
				aCancel = true;
				std::cout << "cancelled fracture with updated stuff" << std::endl;
			}
			continue;
		}

		Eigen::Vector3f splitDirection = solver.matrixV().col(0);

		auto& c = clusters[cIndex];
		//std::cout<<"SPLITTING "<<cIndex<<std::endl;

		std::vector<ClusterMember> memberVector(c.members, c.members + c.numMembers);

		assert(c.numMembers == memberVector.size());

		auto it = std::partition(memberVector.begin(), memberVector.end(),
			[&worldCOM, &splitDirection, this](const ClusterMember& a) {
			return (worldCOM - particles[a.index].position).dot(splitDirection) > 0;
		});

		assert(it != memberVector.end());
		
		auto oldSize = std::distance(memberVector.begin(), it);
		auto newSize = std::distance(it, memberVector.end());
/*
		std::cout << "A: " << A << std::endl;
		std::cout << "Split direction: " << std::endl << splitDirection << std::endl;
		std::cout << "WorldCom: " << std::endl << worldCOM << std::endl;
		std::cout << "oldSize: " << oldSize << ", " << "newSize: " << newSize << "c.numMembers: " << c.numMembers << std::endl;

		for (int i = 0; i < memberVector.size(); ++i)
		{
			if (i == oldSize)
				std::cout << "====================================================" << std::endl;
			std::cout << "(worldCOM - particles[a.index].position).dot(splitDirection) > 0: " << ((worldCOM - particles[memberVector[i].index].position).dot(splitDirection) > 0) << std::endl;
		}
*/		
		if (newSize == 0 || oldSize == 0) { continue; }

		//expected to be mostly in the x direction for the cube example, and it was
		//std::cout << "split direction: " << splitDirection << std::endl;

		//make a new cluster
		Cluster newCluster;
		newCluster.numMembers = newSize;
		for (auto i : range(newSize)) {
			newCluster.members[i] = *(it + i);
//			particles[newCluster.members[i].index].color = cl_float3{ 1.0f, 0.0f, 0.0f };
		}

		// copy relevant variables
		newCluster.Fp = c.Fp; // plasticity
		newCluster.FpNew = c.FpNew;
		newCluster.cstrain = c.cstrain; // plasticity
		newCluster.toughness = c.toughness;

		for (int i = 0; i < c.numNeighbors; ++i)
		{
			newCluster.neighbors[i] = c.neighbors[i];
		}
		newCluster.numNeighbors = c.numNeighbors;

		//delete the particles from the old one
		c.numMembers = oldSize;
		for (int i = 0; i < c.numMembers; ++i)
		{
			c.members[i] = memberVector[i];
//			particles[c.members[i].index].color = cl_float3{ 0.0f, 0.0f, 1.0f };
		}

		// Update Collision Geometry
		newCluster.cg = c.cg;

		//we should be transforming from world to rest, so we shouldn't include plasticity
		Eigen::Vector3f n = ((Apq*c.aInv).inverse()*splitDirection).normalized();

		//we need to worry about signs at some point
		c.cg.addPlane(n, -(n.dot(c.restCom)));
		newCluster.cg.addPlane(-n, (n.dot(c.restCom)));

		clusters.push_back(newCluster);
		if (worldParameters.delayRepeatedFracture) {
			clusters[clusters.size() - 1].justFractured = true;
			clusters[cIndex].justFractured = true;

		}
		if (worldParameters.toughnessBoost > 0.0f) {
			clusters[clusters.size() - 1].timeSinceLastFracture = 0.0f;
			clusters[cIndex].timeSinceLastFracture = 0.0f;
		}

		// add new cluster as a neighbor.  We may delete it later.
		for (int i = 0; i <  clusters[cIndex].numNeighbors; ++i) {
			int n = clusters[cIndex].neighbors[i];
			clusters[n].neighbors[clusters[n].numNeighbors++] = clusters.size() - 1;

			if (clusters[n].numNeighbors > MAX_NEIGHBORS)
			{
				std::cout << clusters[n].numNeighbors << std::endl;
				assert(0);
			}
		}
		
		setupTimer.stopTiming();
		{
			auto updateClusterTimer = fractureProf.timeName("updateClusterProperties");
			updateClusterProperties(std::initializer_list<size_t>{cIndex, clusters.size() - 1});
		}

		{
			auto propTimer = fractureProf.timeName("propagate");
			//split from other clusters
			std::vector<int> allParticles;
			for (int i = 0; i < clusters[cIndex].numMembers; ++i) {
				auto &member = clusters[cIndex].members[i];
				assert(member.index > -1); 
				allParticles.push_back(member.index);
			}

			for (int i = 0; i < newCluster.numMembers; ++i) {
				auto &member = newCluster.members[i];
				assert(member.index > -1);
				allParticles.push_back(member.index);
			}

			std::vector<Particle> newParticles;

			for (auto& member : allParticles) {
				auto& p = particles[member];
				float w1 = 0.0f;
				int n = 0;

				for (int clusterIndex = 0; clusterIndex < p.numClusters; ++clusterIndex) {
					auto &i = p.clusters[clusterIndex].index;
					if (i == cIndex || i == clusters.size() - 1)
						continue;

					//Note : redefinition of c in for loop scope
					auto &c = clusters[(i)];

					if (((p.position - worldCOM).dot(splitDirection) >= 0) !=
						((c.worldCom - worldCOM).dot(splitDirection) >= 0))
					{
						unsigned int j = 0;
						while (c.members[j].index != member && j < c.numMembers)
							j++;

						assert(j != c.numMembers);
						w1 += c.members[j].weight;
						n++;
						c.members[j].index = particles.size() + newParticles.size();
					}
				}

				if (n == 0)
					continue;

				p.flags |= SPLIT;

				Particle q(p);
//				p.color = cl_float3{ 1.0f, 1.0f, 1.0f };
//				std::cout << "Splitting Particle " << member << std::endl;
				q.numClusters = n;
				q.mass = (w1 / p.totalweight) * p.mass;
				q.totalweight = w1;

				p.numClusters -= n;
				p.mass = ((p.totalweight - w1) / p.totalweight) * p.mass; 
				p.totalweight -= w1;

				assert(p.mass > 0 && q.mass > 0);

				newParticles.push_back(q);
//				std::cout << "Adding Particle at index: " << particles.size() + newParticles.size() << std::endl;
			}	//For allParticles

			std::vector<size_t> affectedClusters = std::initializer_list<size_t>{ cIndex, clusters.size() - 1 };
			affectedClusters.insert(affectedClusters.end(), clusters[cIndex].neighbors, clusters[cIndex].neighbors + clusters[cIndex].numNeighbors);

			particles.insert(particles.end(), newParticles.begin(), newParticles.end());
			updateClusterProperties(affectedClusters);

		} // end propagate scope block

		// TODO - The below doesn't actually make sense since ids aren't maintained after initialisation.
		//			New particles created above will have a non-unique id
		//			Tried using actual particle indices instead but it didn't immediately work, will investigate further later - Dan
/*		
		{

			auto neighborTimer = fractureProf.timeName("updateClusterNeighbors");
			// This loop will remove clusters that do not span the fracture plane
			// from neighbor lists.  It looks for a certificate particle
			// to retain the connection.  
			for (auto i : std::initializer_list<size_t>{ cIndex, clusters.size() - 1 }) {
				std::unordered_set<int> eraseFromA, ids;
				Cluster &a = clusters[i];

				for (int j = 0; j < a.numMembers; ++j)
				{
					auto & p = a.members[j];
					ids.insert(particles[p.index].id);
				}
				for (int j = 0; j < a.numNeighbors; ++j) 
				{
					Cluster &b = clusters[a.neighbors[j]];
					bool stillNeighbors = false;
					//for (auto &p : b.members) {
					int k;
					for (k = 0; k < b.numMembers; ++k)
					{
						auto &p = b.members[k];
						if (ids.count(particles[p.index].id) > 0) {
							stillNeighbors = true;
							break;
						}
					}

					if (!stillNeighbors) {
						eraseFromA.insert(a.neighbors[j]);
						--b.numNeighbors;
						// swap k with element just beyond end of array.
						// deletes k without leaving a gap in the array
						b.neighbors[k] = b.neighbors[b.numNeighbors];
					}
				}
				for (auto &j : eraseFromA)
				{
					--a.numNeighbors;
					a.neighbors[j] = a.neighbors[a.numNeighbors];
				}
			}

		} // end update cluster neighbours scope block
*/
		// Adam added these lines on 10/21.  Maybe fractured clusters shouldn't collide.
		//		clusters[clusters.size() - 1].neighbors.insert(cIndex);
		auto &c1 = clusters[clusters.size() - 1];
		auto &c2 = clusters[cIndex];
		c1.neighbors[c1.numNeighbors++] = cIndex;
		c2.neighbors[c2.numNeighbors++] = clusters.size() - 1;

		if (c1.numNeighbors > MAX_NEIGHBORS)
		{
			std::cout << c1.numNeighbors << std::endl;
			assert(0);
		}

		if (c2.numNeighbors > MAX_NEIGHBORS)
		{
			std::cout << c2.numNeighbors << std::endl;
			assert(0);
		}
	}
	
	auto endTime = std::chrono::high_resolution_clock::now();
	float secsElapsed = std::chrono::duration<float>(endTime - start).count();

	if (secsElapsed > 0.001f) {
		std::cout << "fracture took more than 1ms: " << secsElapsed << "s" << std::endl;
		fractureProf.dump<std::chrono::duration<float>>(std::cout);
	}
}

void World::splitOutliers() {
	for (auto&& en : benlib::enumerate(clusters)) {
		auto& c = en.second;

		for (int i = 0; i < c.numMembers; ++i) {
			auto &member = c.members[i];
			assert(member.index > -1);
			auto &p = particles[member.index];
			if ((p.numClusters > 1) && ((p.position - c.worldCom).norm() >
				(1.0f + worldParameters.gamma) * worldParameters.outlierThreshold * (p.restPosition - c.restCom).norm()))
			{
				Particle q(p);
				//int prevclusters = p.numClusters;
				q.numClusters = 0;
				q.clusters[q.numClusters].index = en.first;
				q.clusters[q.numClusters].weight = member.weight;
				++q.numClusters;

				q.mass = (member.weight / p.totalweight) * p.mass;
				q.totalweight = member.weight;
				float newMass = ((p.totalweight - member.weight) / p.totalweight) * p.mass;

				p.numClusters--;
				p.mass = newMass;
				p.totalweight -= member.weight;

				//Remove p from the list of members in c
				c.numMembers--;
				c.members[i] = c.members[c.numMembers];
				
				p.flags |= SPLIT;
				q.flags |= SPLIT;

				if ((p.mass <= 0.0f) || (q.mass <= 0.0f))
				{
					std::cout << std::endl;
					std::cout << "index: " << member.index << std::endl;
					std::cout << "p.mass: " << p.mass << ", q.mass: " << q.mass << std::endl;
					std::cout << "p.totalWeight: " << p.totalweight << ", q.totalWeight: " << q.totalweight << std::endl;
					//std::cout << "p.prevNumClusters: " << prevclusters << std::endl;
					std::cout << "p.numClusters: " << p.numClusters << ", q.numClusters: " << q.numClusters << std::endl;
					std::cout << "p.clusters: " << p.clusters[0].index << ", " << p.clusters[1].index << ", " << p.clusters[2].index << std::endl;
					std::cout << "q.clusters: " << q.clusters[0].index << ", " << q.clusters[1].index << ", " << q.clusters[2].index << std::endl;
					std::cout << "c.numMembers: " << c.numMembers << std::endl;
					std::cout << "c.mass: " << c.mass << std::endl;
					std::cout << "member.weight: " << member.weight << std::endl << std::endl;

					//assert(0);
				}

				particles.push_back(q);
			}
		}
	}
}

void World::cullSmallClusters() {
	float threshold = 1e-4f;
	auto sizeBefore = clusters.size();

	for (auto &c : clusters) {
		if (c.numMembers < 4 || c.mass < threshold) {
			for (int i = 0; i < c.numMembers; ++i) {
				auto &member = c.members[i];
				assert(member.index > -1);
				particles[member.index].totalweight -= member.weight;
			}
		}
	}

	std::vector<int> mapping;
	mapping.resize(clusters.size());
	int i1 = 0, i2 = 0;
	for (auto &c : clusters) {
		if (c.numMembers < 4 || c.mass < threshold) {
			mapping[i1++] = -1;
		}
		else {
			mapping[i1++] = i2++;
		}
	}

	for (auto &c : clusters) {
		std::vector<int> newNeighbors;

		for (int i = 0; i < c.numNeighbors; ++i) {
			int n = c.neighbors[i];
			if (mapping[n] != -1)
				newNeighbors.push_back(mapping[n]);
		}
		for (int i = 0; i < newNeighbors.size(); ++i)
		{
			c.neighbors[i] = newNeighbors[i];
		}
		c.numNeighbors = newNeighbors.size();
	}

	utils::actuallyEraseIf(clusters,
		[](const Cluster& c) {
		return (c.numMembers < 4 || c.mass < 1e-4f);
	});

	if (clusters.size() != sizeBefore) {
		std::cout << "deleted " << sizeBefore - clusters.size() << " clusters" << std::endl;
	}
	countClusters();
}

void World::removeLonelyParticles() {
	std::vector<int> mapping;
	mapping.resize(particles.size());
	int i1 = 0, i2 = 0;
	for (auto &p : particles) {
		if (p.numClusters > 0) {
			mapping[i1++] = i2++;
		}
		else {
			mapping[i1++] = -1;
		}
	}

	for (auto &c : clusters) {
		for (int i = 0; i < c.numMembers; ++i) {
			auto &member = c.members[i];
			assert(member.index > -1);
			assert(mapping[member.index] != -1);
			member.index = mapping[member.index];
		}
	}

	//need to call cleanup on the reved particles...
	auto it = std::stable_partition(particles.begin(), particles.end(),
		[](const Particle& p) {return p.numClusters > 0; });

	particles.erase(it, particles.end());
}

/////////////////////////////////////
// COLLISIONS
/////////////////////////////////////

void World::bounceOutOfPlanes() {
	prof.timeName("ground collisions");
	bool bounced = true;
	int iters = 0;

	const float epsilon = 1e-5f;

	while (bounced) {
		bounced = false;

		for (auto & plane : planes) {
			Eigen::Vector3f norm = plane.head(3);

			for (auto & p : particles) {
				if (p.position.dot(norm) < plane.w()) {
					bounced = true;
					p.position += (epsilon + plane.w() - p.position.dot(norm))*norm;

					//zero velocity in the normal direction
					p.velocity -= p.velocity.dot(norm)*norm;
					p.velocity *= 0.4f; //friction
				}
			}
		}

		++iters;
	}

	//handle moving planes
	for (auto& movingPlane : movingPlanes) {
		for (auto& p : particles) {
			if (worldParameters.dragWithPlanes) {
				movingPlane.dragParticle(p, elapsedTime);
			}
			else {
				movingPlane.bounceParticle(p, elapsedTime);
			}

			if (movingPlane.backsideReflectBounceParticle(p, elapsedTime, 0.0f))
				bounced = true;
		}
	}

	//handle twisting planes
	for (auto& twistingPlane : twistingPlanes) {
		for (auto& p : particles) {
			twistingPlane.twistParticle(p, elapsedTime);
		}
	}

	//handle tilting planes
	for (auto& tiltingPlane : tiltingPlanes) {
		for (auto& p : particles) {
			tiltingPlane.tiltParticle(p, elapsedTime);
		}
	}


	for (auto& projectile : projectiles) {
		for (auto& particle : particles) {
			projectile.bounceParticle(particle, elapsedTime);
		}
	}

	for (auto& cylinder : cylinders) {
		for (auto& particle : particles) {
			cylinder.bounceParticle(particle);
		}
	}
}

// Note: Used to be buildClusterMaps
void World::initializeNeighbors() {
	auto start = std::chrono::high_resolution_clock::now();
	std::cout << "Initialising Neighbors" << std::endl;
	countClusters();
	std::vector<std::vector<ParticleCluster> > idToClusters;
	idToClusters.resize(particles.size());
	
	for (auto &p : particles) {
		std::vector<ParticleCluster> vect(p.clusters, p.clusters + p.numClusters);
		idToClusters[p.id].insert(idToClusters[p.id].end(), vect.begin(), vect.end());
	}

	for (auto &i : idToClusters) 
	{
		for (auto &c : i) 
		{	
			if (clusters[c.index].numNeighbors > MAX_NEIGHBORS)
			{
				std::cout << clusters[c.index].numNeighbors;
				assert(0);
			}
			std::unordered_set<int> neighbors(clusters[c.index].neighbors, clusters[c.index].neighbors + clusters[c.index].numNeighbors);
			for (auto &d : i) 
			{
				int dIndex = d.index;
				if (c.index == dIndex)
					continue;
				
				if (neighbors.count(dIndex) < 1)
				{
					clusters[c.index].neighbors[clusters[c.index].numNeighbors++] = dIndex;
					neighbors.insert(dIndex);
				}
			}

			if (clusters[c.index].numNeighbors > MAX_NEIGHBORS)
			{
				std::cout << clusters[c.index].numNeighbors << std::endl;
				assert(0);
			}
		}
	}
}

bool CollisionGeometry::project(Eigen::Vector3f &x) {
	//return false;
	Eigen::Vector3f y;
	float n;
	Eigen::Vector3f d = x - c;
	float m = d.norm();
	if (m >= r)
		return false;

	y = c + (r / m)*d;
	n = r - m;

	for (int i = 0; i < numPlanes; ++i) {
		m = planes[i].normal.dot(x) + planes[i].offset;
		if (m >= 0.0f)
			return false;
		if (-m < n) {
			n = -m;
			y = x - m * planes[i].normal;
		}
	}

	assert((x - y).norm() <= n + 1e-4f);
	x = y;
	return true;
}

inline Eigen::Vector3f restToWorld(const Cluster &c, const Eigen::Vector3f &x) {
	assert(x.allFinite());
	Eigen::Vector3f y = c.worldCom + c.restToWorldTransform * (x - c.restCom);
	assert(y.allFinite());
	return y;
}


inline Eigen::Vector3f worldToRest(const Cluster &c, const Eigen::Vector3f &x) {
	assert(x.allFinite());
	Eigen::Vector3f y = c.restCom + c.worldToRestTransform * (x - c.worldCom);
	if (!y.allFinite()) {
		std::cout << x << std::endl;
		std::cout << y << std::endl;
		std::cout << c.worldToRestTransform << std::endl;
		std::cout << c.numMembers << std::endl;
	}
	assert(y.allFinite());
	return y;
}

struct ClusterComGetter {
	Eigen::Vector3f operator()(const Cluster& c) const {
		return c.worldCom;
	}
};

struct ClusterRadiusGetter {
	float operator()(const Cluster& c) const {
		return c.width;
	}
};


void World::selfCollisions() {
	
	const float alpha = worldParameters.collisionRestitution;

	AccelerationGrid<Cluster, ClusterComGetter> accelerationGrid;
	accelerationGrid.numBuckets = 16; //todo, tune me
	accelerationGrid.updateGridWithRadii(clusters, ClusterRadiusGetter{});
	auto potentialClusterPairs = accelerationGrid.getPotentialPairs();

	int wereNeighbors = 0;
	int collided = 0;

	for (auto& clusterPair : potentialClusterPairs) {
		auto i = clusterPair.first;
		auto j = clusterPair.second;

		int jCount = 0;
		for (int k = 0; k < clusters[i].numNeighbors; ++k)
		{
			//TODO - Create C99 compatible hash table for faster searching
			//TODO - Possible bug here. May want to copy array into a set instead.
			if (clusters[i].neighbors[k] == j)
				++jCount;

			//Early exit since we're just looking to test if it's there more than once
			if (jCount > 0)
				break;
		}

		if (jCount > 0)
		{
			wereNeighbors++;
			continue;
		}

		collided++;
		auto& c = clusters[i];
		auto& d = clusters[j];

		if (d.numMembers < 10)
			continue;

		if ((c.worldCom - d.worldCom).squaredNorm() < sqr(c.width + d.width))
		{
			for (int i = 0; i < c.numMembers; ++i) {
				auto &member = c.members[i];
				assert(member.index > -1);
				auto &p = particles[member.index];
				//if (p.flags & Particle::SPLIT) continue;
				if ((p.position - d.worldCom).squaredNorm() < sqr(d.width)) {
					// these next two lines look unnecessary with clustermaps
					// it = find(p.clusters.begin(), p.clusters.end(), j);
					// (it == p.clusters.end()) {
					std::vector<int> clustVect(p.numClusters);
					for (int k = 0; k < p.numClusters; ++k)
						clustVect.push_back(p.clusters[k].index);
					
					if (!utils::containsValue(clustVect, j)) { //easier to read... --Ben
						Eigen::Vector3f x = worldToRest(d, p.position);
						if (d.cg.project(x)) {
							x = restToWorld(d, x);

							Eigen::Vector3f y = d.worldCom + d.width * (p.position - d.worldCom).normalized();

							if ((x - p.position).squaredNorm() > (y - p.position).squaredNorm())
							{
								x = y;
							}

							p.position = alpha*x + (1.0f - alpha)*p.position;
						}
					}
				}
			}

			//swap c and d
			for (int i = 0; i < d.numMembers; ++i) {
				auto &member = d.members[i];
				assert(member.index > -1);
				auto &p = particles[member.index];
				//if (p.flags & Particle::SPLIT) continue;
				if ((p.position - c.worldCom).squaredNorm() < sqr(c.width)) {
					// these next two lines look unnecessary with clustermaps
					std::vector<int> clustVect(p.numClusters);
					for (int k = 0; k < p.numClusters; ++k)
						clustVect.push_back(p.clusters[k].index);
					if (!utils::containsValue(clustVect, i)) {
						Eigen::Vector3f x = worldToRest(c, p.position);
						if (c.cg.project(x)) {
							x = restToWorld(c, x);
							Eigen::Vector3f y = c.worldCom + c.width * (p.position - c.worldCom).normalized();

							if ((x - p.position).squaredNorm() > (y - p.position).squaredNorm())
							{
								x = y;
							}

							p.position = alpha*x + (1.0f - alpha)*p.position;
						}
					}
				}
			}
		}
	}

	/*
	for (auto && en1 : benlib::enumerate(clusters)) {
	auto &c = en1.second;
	if (c.members.size() < 10) continue;
	for (auto && en2 : benlib::enumerate(clusters)) {
	if (en1.first == en2.first) continue;
	// 6/24: I think the old == was a bug here
	// 6/29, this helper function should be easier to use than raw std::find --Ben
	if( utils::containsValue(clusterCollisionMap[en1.first], en2.first)){ continue; }

	auto &d = en2.second;
	if (d.members.size() < 10) continue;
	if ((c.worldCom - d.worldCom).squaredNorm() < sqr(c.width + d.width)) {
	for (auto& member : c.members){
	auto &p = particles[member.index];
	//if (p.flags & Particle::SPLIT) continue;
	if ((p.position - d.worldCom).squaredNorm() < sqr(d.width)) {
	// these next two lines look unnecessary with clustermaps
	auto it = find(p.clusters.begin(), p.clusters.end(), en2.first);
	if (it == p.clusters.end()) {
	Eigen::Vector3f x = worldToRest(d, p.position);
	if (d.cg.project(x)) {
	//std::cout<<en1.first<<" "<<en2.first<<" "<<n.first<<std::endl;
	//for (auto foo : clusterCollisionMap[en1.first]) std::cout<<foo<<" ";
	//std::cout<<std::endl;
	//for (auto foo : clusterCollisionMap[en2.first]) std::cout<<foo<<" ";
	//std::cout<<std::endl;
	x = restToWorld(d, x);
	Eigen::Vector3f y = d.worldCom + d.width * (p.position - d.worldCom).normalized();
	if ((x-p.position).squaredNorm() > (y-p.position).squaredNorm()) {x=y;}
	//if ((x-p.position).norm() > (p.position-d.worldCom).norm()) {
	if ((x-p.position).norm() > d.width) {
	std::cout<<"--------------------------"
	<<(x-p.position).norm()<<" "<<d.width<<" "<<d.cg.r<<std::endl;
	std::cout<<d.worldCom(0)<<" "<<d.worldCom(1)<<" "<<d.worldCom(2)<<std::endl;
	std::cout<<p.position(0)<<" "<<p.position(1)<<" "<<p.position(2)<<std::endl;
	std::cout<<x(0)<<" "<<x(1)<<" "<<x(2)<<std::endl;
	}
	//std::cout<<"before: "<<(p.position - d.worldCom).norm()<<" "<<d.width<<std::endl;
	p.position = alpha*x + (1.0f-alpha)*p.position;
	//std::cout<<"after: "<<(p.position - d.worldCom).norm()<<" "<<d.width<<std::endl;
	}
	}
	}
	}
	}
	}
	}*/

	//std::cout << "<><><><><><><><><><><><><><><><><><><><><><><><><><><><>" << std::endl;
	//std::cout << "COLLIDED:        " << collided << std::endl;
	//std::cout << "WERE NEIGHBORS:  " << wereNeighbors << std::endl;
	//std::cout << "<><><><><><><><><><><><><><><><><><><><><><><><><><><><>" << std::endl;
}

///////////////////////////////////////////
// UTILS
///////////////////////////////////////////

Eigen::Matrix3f World::computeApq(const Cluster& c) const {
	Eigen::Matrix3f Apq;
	Apq.setZero();

	for (int i = 0; i < c.numMembers; ++i) {
		auto &member = c.members[i];
		assert(member.index > -1);
		auto &p = particles[member.index];
		Eigen::Vector3f pj = p.position - c.worldCom;
		Eigen::Vector3f qj = p.restPosition - c.restCom;

		Apq += (member.weight / p.totalweight)*p.mass * pj * qj.transpose();
	}
	return Apq;
}

Eigen::Vector3f World::computeClusterVelocity(const Cluster &c) const {
	float mass = 0.0f;
	Eigen::Vector3f vel = Eigen::Vector3f::Zero();
	for (int i = 0; i < c.numMembers; ++i) {
		auto &member = c.members[i];
		assert(member.index > -1);
		auto &p = particles[member.index];
		float w = (member.weight / p.totalweight) * p.mass;
		mass += w;
		vel += w * p.velocity;
	}
	return (vel / mass);
}

void World::updateTransforms(Cluster& c) const {
	Eigen::Matrix3f A = computeApq(c)*c.aInv;
	//if (nu > 0.0f) A = A*c.Fp.inverse(); 

	//auto pr = utils::polarDecomp(A);
	//Eigen::Matrix3f T = pr.first;
	//if (nu > 0.0f) T = T*c.Fp; 
	c.restToWorldTransform = A;
	c.worldToRestTransform = A.inverse();
}

void World::countClusters() {
	int totalClusters = 0;
	int totalMembers = 0;
	
	for (auto& p : particles) {
		totalClusters += p.numClusters;
		p.numClusters = 0;
	}

	for (auto cInd : benlib::range(clusters.size())) {
		for (int i = 0; i < clusters[cInd].numMembers; ++i) {
			assert(clusters[cInd].numMembers < MAX_MEMBERS);
			auto &member = clusters[cInd].members[i];
			assert(member.index > -1 && member.weight > 0);
			auto &p = particles[member.index];
			p.clusters[p.numClusters].index = cInd;
			p.clusters[p.numClusters].weight = member.weight;
			++p.numClusters;
		}

		totalMembers += clusters[cInd].numMembers;
	}

	for (auto& p : particles) {
		//assert(p.totalweight > 0);
		//assert(p.mass > 0);
	}
}

// updates coms, width, mass, aInv
template <typename Container>
void World::updateClusterProperties(const Container& clusterIndices) {

	countClusters(); //TODO, just touch a subset...

	for (int i = 0; i < particles.size(); i++) {
		auto &p = particles[i];
		if (p.mass <= 0.0)
		{
			std::cout << std::endl;
			std::cout << "index: " << i << std::endl;
			std::cout << p.mass << " " << std::endl;
			std::cout << "p.numClusters: " << p.numClusters << std::endl;
			std::cout << "p.totalWeight: " << p.totalweight << std::endl;

			std::cout << std::endl;
			for (int j = 0; j < p.numClusters; ++j)
			{
				std::cout << "p.clusters[" << j << "].mass = " << clusters[p.clusters[j].index].mass << std::endl;
				std::cout << "p.clusters[" << j << "].numMembers = " << clusters[p.clusters[j].index].numMembers << std::endl;
			}

			assert(p.mass > 0.0);
		}
	}

	// compute cluster mass, com, width, and aInv
	for (auto cIndex : clusterIndices) {
		auto& c = clusters[cIndex];
		//c.Fp.setIdentity(); // plasticity
		c.mass = sumWeightedMass(c.members, c.numMembers);
		if (!(c.mass >= 0)) {
			std::cout << c.mass << " " << c.numMembers << " " << c.members[0].weight << std::endl;
		}

		assert(c.mass >= 0);
		c.restCom = sumWeightedRestCOM(c.members, c.numMembers);
		c.worldCom = sumWeightedWorldCOM(c.members, c.numMembers);

		if (!c.restCom.allFinite())
		{
			std::cout << c.restCom << std::endl;
		}

		if (!c.worldCom.allFinite())
		{
			std::cout << c.worldCom << std::endl;
		}

		assert(c.restCom.allFinite());
		assert(c.worldCom.allFinite());

		c.width = 0.0;

		for (int i = 0; i < c.numMembers; ++i) {
			auto &member = c.members[i];
			assert(member.index > -1);
			c.width = std::max(c.width, (c.restCom - particles[member.index].restPosition).norm());
		}

		c.aInv.setZero();
		// adam says: this should take weights into account
		for (int i = 0; i < c.numMembers; ++i) {
			auto &member = c.members[i];
			assert(member.index > -1);
			auto &p = particles[member.index];
			Eigen::Vector3f qj = p.restPosition - c.restCom;

			Eigen::Matrix3f aInvIncrememnt = (member.weight / p.totalweight)*p.mass * qj * qj.transpose();
			
			c.aInv += (member.weight / p.totalweight)*p.mass * qj * qj.transpose();
		}

		//do pseudoinverse	
		Eigen::JacobiSVD<Eigen::Matrix3f> solver(c.aInv, Eigen::ComputeFullU | Eigen::ComputeFullV);
		Eigen::Vector3f sigInv;
		for (auto i : range(3)) {
			if (solver.singularValues()(i) < 1e-6) 
				c.Fp.setIdentity();

			// adam says: on 10/27 I changed this from 0 to 1.0/1.0e-12.  This seemed to resolve an inf issue during self collisions.
			// But, I left in the deletion code below anyway.
			sigInv(i) = fabs(solver.singularValues()(i)) > 1e-12 ? 1.0 / solver.singularValues()(i) : 1.0 / 1.0e-12;

			if (solver.singularValues()(i) < 1e-12) {
				std::cout << "Marking cluster " << cIndex << " for deletion. " << std::endl;
				c.mass = 0.0; // mark the cluster for deletion
			}
		}

		c.aInv = solver.matrixV()*sigInv.asDiagonal()*solver.matrixU().transpose();//c.aInv.inverse().eval();

		if (!c.aInv.allFinite()) {
			std::cout << c.aInv << std::endl;
			std::cout << solver.singularValues() << std::endl;
		}
		assert(c.aInv.allFinite());
	}

}
