#include "projectile.hpp"
#include "particle.h"

void Projectile::bounceParticle(Particle& particle, float timeElapsed) const {
  
  Eigen::Vector3f currentPosition = start + timeElapsed*velocity;
  Eigen::Vector3f fromCenter = particle.position - currentPosition;
  if(fromCenter.norm() < radius){
	particle.position = currentPosition + radius*fromCenter.normalized();
	particle.velocity += momentumScale*velocity;
  }

}
