#include "clUtils.hpp"
#include <iostream>
//#include <fstream>
#include <string> 

// Return the lowest multiple of b that encompasses a 
size_t clUtils::uSnap(size_t a, size_t b) {
	return ((a % b) == 0) ? a : (a - (a % b) + b);
}

// Checkl if GL Interop extension is supported on device
bool clUtils::checkGLInterop(cl::Device &device)
{
	bool glInterop = true;
	std::string extensions = device.getInfo<CL_DEVICE_EXTENSIONS>();
	size_t found = extensions.find("cl_khr_gl_sharing");
	if (found == std::string::npos)
	{
		std::cerr << "ERROR::GL/CL Sharing not supported." << std::endl << std::endl;

		std::cout << "Supported Extensions: " << std::endl;
		std::cout << extensions;

		glInterop = false;
	}
	return glInterop;
}
	
// Get OpenCL Platform
bool clUtils::getPlatform(bool useGPU, cl::Platform &clPlatform)
{
	std::vector<cl::Platform> allPlatforms;
	cl::Platform::get(&allPlatforms);

	if (allPlatforms.size() == 0)
	{
		std::cout << "Error: No OpenCL Platform Found." << std::endl;
		return false;
	}
	else
	{
		std::cout << "Found the Following CL Platforms:" << std::endl;
		for (auto& p : allPlatforms)
		{
			std::cout << p.getInfo<CL_PLATFORM_NAME>() << std::endl;
		}
		std::cout << std::endl;

		std::cout << "------------------------------------------------------------------" << std::endl;

		bool foundPlatform = false;
		for (int i = 0; i < allPlatforms.size(); i++)
		{
			clPlatform = allPlatforms[i];
			std::string vendorName = clPlatform.getInfo<CL_PLATFORM_VENDOR>();

			if (useGPU)
			{
				if (!strcmp(vendorName.c_str(), "Advanced Micro Devices, Inc.") || !strcmp(vendorName.c_str(), "NVIDIA Corporation"))
				{
					foundPlatform = true;
					break;
				}
			}
			else
			{
				if (/*!strcmp(vendorName.c_str(), "Advanced Micro Devices, Inc.") ||*/ !strcmp(vendorName.c_str(), "Intel(R) Corporation"))
				{
					foundPlatform = true;
					break;
				}
			}
		}

		if (!foundPlatform)
		{
			std::cout << "No appropriate CL clPlatform found. Falling Back to clPlatform:" << std::endl << clPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
		}
		else
		{
			std::cout << "Using OpenCL Platform: " << clPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
		}
	}
	std::cout << "------------------------------------------------------------------" << std::endl << std::endl;

	return true;
}

// Get Handle For OpenCL Device
bool clUtils::getDevice(bool useGPU, cl::Platform &platform, cl::Device &device)
{
	std::vector<cl::Device> allDevices;
	cl_int status = CL_SUCCESS;

	if (useGPU)
		status = platform.getDevices(CL_DEVICE_TYPE_GPU, &allDevices);
	else
		status = platform.getDevices(CL_DEVICE_TYPE_CPU, &allDevices);

	if (status != CL_SUCCESS)
	{
		std::cout << "Error, Failed to get CL Devices." << std::endl;
		return false;
	}
	else
	{
		std::cout << "Found OpenCL Devices:" << std::endl << std::endl;

		cl_int maxCUs = 0;
		int chosenDevIndex = 0;
		for (int i = 0; i < allDevices.size(); i++)
		{
			std::string vendor = allDevices[i].getInfo<CL_DEVICE_VENDOR>();
			std::string devName = allDevices[i].getInfo<CL_DEVICE_NAME>();
			cl_device_type devType = allDevices[i].getInfo<CL_DEVICE_TYPE>();
			std::string deviceType;
			switch (devType)
			{
			case CL_DEVICE_TYPE_CPU:
				deviceType = " CPU";
				break;
			case CL_DEVICE_TYPE_GPU:
				deviceType = " GPU";
				break;
			case CL_DEVICE_TYPE_ACCELERATOR:
				deviceType = " accelerator";
				break;
			case CL_DEVICE_TYPE_CUSTOM:
				break;
			case CL_DEVICE_TYPE_DEFAULT:
				break;
			}

			cl_int CUs = allDevices[i].getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
			cl_int clockSpeed = allDevices[i].getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>();
			cl_double globalMem = (cl_double)allDevices[i].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
			std::string cVersion = allDevices[i].getInfo<CL_DEVICE_VERSION>();
			cl_double maxConstBuffer =	(cl_double)allDevices[i].getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>();
			cl_double maxMemAlloc =		(cl_double)allDevices[i].getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
			cl_double globalCacheSize = (cl_double)allDevices[i].getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_SIZE>();
			cl_double globalCacheLine = (cl_double)allDevices[i].getInfo<CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE>();
			cl_double localMemorySize = (cl_double)allDevices[i].getInfo<CL_DEVICE_LOCAL_MEM_SIZE>();
			cl_bool	dedicatedLocalMem = CL_LOCAL | allDevices[i].getInfo<CL_DEVICE_LOCAL_MEM_TYPE>();

			if (CUs > maxCUs)
			{
				maxCUs = CUs;
				chosenDevIndex = i;
			}

			std::cout << vendor << " " << devName << deviceType << ": " << std::endl << std::endl <<
				"OpenCL Version: " << cVersion << std::endl <<
				CUs << " compute units" << std::endl <<
				clockSpeed << " Mhz Clock Speed" << std::endl <<
				"Global Memory Size: " 	<< globalMem / (1024 * 1024) << " MB" << std::endl <<
				"Global Memory Cache: " << globalCacheSize / (1024) << " KB" << std::endl <<
				"Global Memory Cache Line: " << globalCacheLine << " B" << std::endl <<
				"Local Memory Size: " << localMemorySize / (1024) << " KB" << std::endl <<
				"Dedicated Local Memory: " << (dedicatedLocalMem ? "Yes" : "No") << std::endl <<
				"Max constant buffer size: " << maxConstBuffer / 1024 << " KB" << std::endl <<
				"Max memory allocation: " << maxMemAlloc / (1024 * 1024) << " MB" << std::endl;
				 
			//std::cout << "==================================================================" << std::endl;
			std::cout << std::endl;
		}

		device = allDevices[chosenDevIndex];
		std::cout << std::endl;
	}

	std::cout << "------------------------------------------------------------------" << std::endl;
	std::cout << "Using OpenCL Device: " << device.getInfo<CL_DEVICE_VENDOR>() << " " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
	std::cout << "------------------------------------------------------------------" << std::endl << std::endl;

	return true;
}

//Get shared OpenGL/OpenCL Context
bool clUtils::getClGlContext(cl::Platform &platform, cl::Device &device, cl::Context &context, bool usingGPU, bool glInterop)
{
	cl_platform_id platformId = platform();

#ifdef linux
	cl_context_properties properties[] = {
		CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
		CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(),
		CL_CONTEXT_PLATFORM, (cl_context_properties)platform,
		0 };
#elif defined _WIN32
	cl_context_properties properties[] = {
		CL_GL_CONTEXT_KHR, (cl_context_properties)wglGetCurrentContext(),
		CL_WGL_HDC_KHR, (cl_context_properties)wglGetCurrentDC(),
		CL_CONTEXT_PLATFORM, (cl_context_properties)platformId,
		0 };
#elif defined TARGET_OS_MAC
	CGLContextObj glContext = CGLGetCurrentContext();
	CGLShareGroupObj shareGroup = CGLGetShareGroup(glContext);
	cl_context_properties properties[] = {
		CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE,
		(cl_context_properties)shareGroup,
		0 };
#endif

	cl_int status = CL_SUCCESS;
	try {
		if (usingGPU && glInterop)
		{
			//cl_context clContext = clCreateContext(properties, 1, &device(), NULL, NULL, &status);
			//context = cl::Context(clContext);
			context = cl::Context(CL_DEVICE_TYPE_GPU, properties);
		}
		else if (usingGPU)
		{
			context = cl::Context::getDefault();
		}
		else
		{

			cl_context_properties properties[] = {
				CL_CONTEXT_PLATFORM, (cl_context_properties)platformId,
				0 };
			context = cl::Context(device, properties);
		}
		//clContext = clCreateContext(properties, 0, 0, NULL, NULL, &status);
		//context = cl::Context(clContext);
	}
	catch (cl::Error err)
	{
		return false;
	}
	if (status != CL_SUCCESS)
	{
		if (usingGPU)
			std::cout << "Error creating shared GL/CL context: " << clUtils::getErrorString(status) << std::endl;
		else
			std::cout << "Error creating OpenCL context: " << clUtils::getErrorString(status) << std::endl;
	}
	return true;
}

// Build OpenCL Program From a Given Source File
bool clUtils::buildClProgram(cl::Context &context, cl::Device &device, cl::Program &prog, std::string srcPath)
{
	std::string srcString;

	std::cout << "Building OpenCL Program: " << srcPath << std::endl;
	//srcPath = "../CSMFracture/" + srcPath;

	if (!convertToString(srcPath.c_str(), srcString))
	{
		std::cout << "Error Loading Source File: " << srcString << std::endl;
		return false;
	}

	prog = cl::Program(context, srcString);
	std::string buildFlags = "-I . -I ../CSMFracture/";

	try {
		prog.build(buildFlags.c_str());
		std::cout << "Build Output:" << std::endl;
		std::cout << prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
	}
	catch (cl::Error err) {
		std::cout << "Failed to build OpenCL Program" << std::endl;
		std::cout << "Error Returned: " << clUtils::getErrorString(err.err()) << " from Method: " << err.what() << std::endl << std::endl;
		std::cout << "Build Output:" << std::endl;
		std::cout << prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
		return false;
	}

	std::cout << std::endl;

	return true;
}

bool clUtils::generateBinary(cl::Program &program)
{
	std::vector<char*> binaries = program.getInfo<CL_PROGRAM_BINARIES>();
	
	for (int i = 0; i < binaries.size(); ++i)
	{
		size_t kernelLength = program.getInfo<CL_PROGRAM_BINARY_SIZES>()[i];

		FILE *fp = fopen((std::to_string(i) + ".bin").c_str(), "wb");
		fwrite(binaries[i], 1, kernelLength, fp);
		fclose(fp);
	}
}

bool clUtils::readBinary(const std::string &binPath, cl::Context &context, cl::Device &device, cl::Program &program)
{
	FILE *fp = fopen(binPath.c_str(), "rb");

	if (fp == NULL)
	{	

		std::cerr << "Invalid binary file provided" << std::endl;
		return false;
	}

	fseek(fp, 0, SEEK_END);

	size_t kernel_length = ftell(fp);
	rewind(fp);

	unsigned char *binary = (unsigned char*)malloc(sizeof(unsigned char)*kernel_length + 10);

	cl_int binStatus, clStatus;
	
	fread(binary, 1, kernel_length, fp);
	fclose(fp);

	program = cl::Program(clCreateProgramWithBinary(context(), 1, &device(), &kernel_length, (const unsigned char**)&binary, &binStatus, &clStatus));
//	program = cl::Program(prog);
	if (clStatus != CL_SUCCESS)
	{
		std::cerr << "Failed to read binary " << binPath << ". Error Returned: " << getErrorString(clStatus) << std::endl;
		std::cerr << "Kernel File Length: " << kernel_length << std::endl;
		return false;
	}

	std::string buildFlags = "-I . -I ../CSMFracture/";
	program.build(buildFlags.c_str());

	free(binary);
	return true;
}

// Convert Kernel File to String
bool clUtils::convertToString(const char *filename, std::string& s)
{
	size_t size;
	char*  str;

	// create a file stream object by filename
	std::fstream f(filename, (std::fstream::in | std::fstream::binary));


	if (!f.is_open())
	{
		return false;
	}
	else
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);

		str = new char[size + 1];
		if (!str)
		{
			f.close();
			return false;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';

		s = str;
		delete[] str;
		return true;
	}
}

// Translate OpemCL Error Code Into Associated string
const char *clUtils::getErrorString(cl_int error)
{
	switch (error) {
		// run-time and JIT compiler errors
	case 0: return "CL_SUCCESS";
	case -1: return "CL_DEVICE_NOT_FOUND";
	case -2: return "CL_DEVICE_NOT_AVAILABLE";
	case -3: return "CL_COMPILER_NOT_AVAILABLE";
	case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
	case -5: return "CL_OUT_OF_RESOURCES";
	case -6: return "CL_OUT_OF_HOST_MEMORY";
	case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
	case -8: return "CL_MEM_COPY_OVERLAP";
	case -9: return "CL_IMAGE_FORMAT_MISMATCH";
	case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
	case -11: return "CL_BUILD_PROGRAM_FAILURE";
	case -12: return "CL_MAP_FAILURE";
	case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
	case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
	case -15: return "CL_COMPILE_PROGRAM_FAILURE";
	case -16: return "CL_LINKER_NOT_AVAILABLE";
	case -17: return "CL_LINK_PROGRAM_FAILURE";
	case -18: return "CL_DEVICE_PARTITION_FAILED";
	case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

		// compile-time errors
	case -30: return "CL_INVALID_VALUE";
	case -31: return "CL_INVALID_DEVICE_TYPE";
	case -32: return "CL_INVALID_PLATFORM";
	case -33: return "CL_INVALID_DEVICE";
	case -34: return "CL_INVALID_CONTEXT";
	case -35: return "CL_INVALID_QUEUE_PROPERTIES";
	case -36: return "CL_INVALID_COMMAND_QUEUE";
	case -37: return "CL_INVALID_HOST_PTR";
	case -38: return "CL_INVALID_MEM_OBJECT";
	case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
	case -40: return "CL_INVALID_IMAGE_SIZE";
	case -41: return "CL_INVALID_SAMPLER";
	case -42: return "CL_INVALID_BINARY";
	case -43: return "CL_INVALID_BUILD_OPTIONS";
	case -44: return "CL_INVALID_PROGRAM";
	case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
	case -46: return "CL_INVALID_KERNEL_NAME";
	case -47: return "CL_INVALID_KERNEL_DEFINITION";
	case -48: return "CL_INVALID_KERNEL";
	case -49: return "CL_INVALID_ARG_INDEX";
	case -50: return "CL_INVALID_ARG_VALUE";
	case -51: return "CL_INVALID_ARG_SIZE";
	case -52: return "CL_INVALID_KERNEL_ARGS";
	case -53: return "CL_INVALID_WORK_DIMENSION";
	case -54: return "CL_INVALID_WORK_GROUP_SIZE";
	case -55: return "CL_INVALID_WORK_ITEM_SIZE";
	case -56: return "CL_INVALID_GLOBAL_OFFSET";
	case -57: return "CL_INVALID_EVENT_WAIT_LIST";
	case -58: return "CL_INVALID_EVENT";
	case -59: return "CL_INVALID_OPERATION";
	case -60: return "CL_INVALID_GL_OBJECT";
	case -61: return "CL_INVALID_BUFFER_SIZE";
	case -62: return "CL_INVALID_MIP_LEVEL";
	case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
	case -64: return "CL_INVALID_PROPERTY";
	case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
	case -66: return "CL_INVALID_COMPILER_OPTIONS";
	case -67: return "CL_INVALID_LINKER_OPTIONS";
	case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

		// extension errors
	case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
	case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
	case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
	case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
	case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
	case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
	default: return "Unknown OpenCL error";
	}
}
