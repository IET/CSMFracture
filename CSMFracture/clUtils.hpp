#pragma once
#include <iostream>
#include <fstream>
#define __CL_ENABLE_EXCEPTIONS
#include <CL\cl.hpp>

namespace clUtils {
	size_t uSnap(size_t a, size_t b);
	const char *getErrorString(cl_int error);
	bool checkGLInterop(cl::Device &device);
	bool getPlatform(bool useGPU, cl::Platform &clPlatform);
	bool getDevice(bool useGPU, cl::Platform &platform, cl::Device &device);
	bool getClGlContext(cl::Platform &platform, cl::Device &device, cl::Context &context, bool usingGPU, bool glInterop);
	bool buildClProgram(cl::Context &context, cl::Device &device, cl::Program &prog, std::string srcPath);
	bool generateBinary(cl::Program &program);
	bool readBinary(const std::string &binPath, cl::Context &context, cl::Device &device, cl::Program &program);
	bool convertToString(const char *filename, std::string& s);
}
