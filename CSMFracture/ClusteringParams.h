#ifndef CLUSTERING_PARAMS_H
#define CLUSTERING_PARAMS_H

#ifdef __cplusplus
#include <Eigen/Eigen>
#endif

struct ClusteringParams {
	float neighborRadius;
	int nClusters;
	float neighborRadiusMax;
	int nClustersMax;
	int clusterItersMax;
	int clusteringAlgorithm; // 0 = default (fuzzy c-means with weights); 1 = k-means; 2 = random
	float clusterOverlap;
	int clusterKernel; // 0 = 1 / (r^2 + eps), 1 = constant, 2 = poly6,
					   //3 = constant/poly6 blend (with kernelweight), 4 = fuzzy c-means

	float kernelWeight;// only for the blended kernel, and fuzzy c-means
	float blackhole;
	float poly6norm, sqrNeighborRadius;
#ifdef __cplusplus
	float kernel(const Eigen::Vector3f &x) const;
#endif
};

#endif //CLUSTERING_PARAMS_H