#ifndef WORLD_PARAMS_H
#define WORLD_PARAMS_H

typedef struct {
#ifdef __cplusplus
	Eigen::Vector3f gravity; float pad;
#else
	float3 gravity;
#endif
	int numConstraintIters;
	float omega, gamma, alpha, springDamping;
	float yield, nu, hardening;	//plasticity parameters
	float toughness, toughnessBoost, toughnessFalloff;
	float collisionRestitution;
	float collisionGeometryThreshold;
	float outlierThreshold;
	float dt;	//For fixed timestep
	int dragWithPlanes;
	int delayRepeatedFracture; int pad1;
	int fractureOn;
	int padArray[1];
}WorldParams;

#endif // WORLD_PARAMS_H
