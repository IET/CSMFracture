#include "particle.h"
#include "utils.h"
#include <iostream>

inline float sqr (float x) {return x*x;}

void Cluster::updatePlasticity(const Eigen::Vector3f &sigma, const Eigen::Matrix3f &U, const Eigen::Matrix3f&V, 
	float yield, float nu, float hardening) {
	FpNew = Fp;

	if (nu > 0.0f) 
	{
		if (sigma(2) >= 1e-4f) 
		{ // adam says: the second clause is a quick hack to avoid plasticity when sigma is degenerate
			//std::cout << "Sigma = " << sigma << std::endl;
			// Adam says (11/4): This might help, but I don't have time to test at the moment
			//if (solver.singularValues()(0)/solver.singularValues()(2) < 1e-4f) return;  
			
			Eigen::Vector3f FpHat = sigma;
			FpHat *= 1.0f/cbrt(FpHat(0) * FpHat(1) * FpHat(2));
			float norm = sqrt(sqr(FpHat(0)-1.0f) + sqr(FpHat(1)-1.0f) + sqr(FpHat(2)-1.0f));
			float local_yield = yield + hardening * cstrain;

			if (norm > local_yield) 
			{	
				float gamma = std::min(1.0f, nu * (norm - local_yield) / norm);
				FpHat(0) = pow(FpHat(0), gamma);
				FpHat(1) = pow(FpHat(1), gamma);
				FpHat(2) = pow(FpHat(2), gamma);
				// update cluster.Fp
				FpNew = FpHat.asDiagonal() * V.transpose() * Fp * V.determinant();
				Eigen::Matrix3f FpHatDiag = FpHat.asDiagonal();

			}
		}
		cstrain += sqrt(sqr(sigma(0)-1.0f) + sqr(sigma(1)-1.0f) + sqr(sigma(2)-1.0f));
	}
}

CollisionGeometry & CollisionGeometry::operator= (const CollisionGeometry &that) {
  c = that.c; 
  r = that.r; 
  
  //planes = that.planes;
  for (int i = 0; i < that.numPlanes; ++i)
  {
	  planes[i] = that.planes[i];
  }
  numPlanes = that.numPlanes;

  //for (auto &p : that.planes) 
  //planes.push_back(std::pair<Eigen::Vector3f, float>(p.first, p.second));
  return *this;
};

Eigen::Matrix4f Cluster::getVisTransform() const {
    Eigen::Matrix4f total_t = Eigen::Matrix4f::Identity();
    //step 3, translate from rest space origin to world 
    total_t.block<3,1>(0,3) << worldCom;
    
    //step 2, rotate about rest-to-world transform
    Eigen::Matrix4f rot = Eigen::Matrix4f::Identity();
    //push the rotation onto a 4x4 glMatrix
    auto polar = utils::polarDecomp(restToWorldTransform);
    rot.block<3,3>(0,0) << polar.first;
    total_t = total_t * rot;

    //step 1, translate rest com to origin
    Eigen::Matrix4f local_t = Eigen::Matrix4f::Identity();
    local_t.block<3,1>(0,3) << -restCom;
    total_t = total_t * local_t;

    return total_t;
}
