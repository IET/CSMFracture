#ifndef PLANE_UTILS_H
#define PLANE_UTILS_H

#include "tiltingPlane.h"
#include "twistingPlane.h"
#include "movingPlane.h"

// TODO - maybe make these take __global pointers
// Not sure if it's quicker to all reads and writes to global memory
// or to copy to private memory, do all operations, and copy back.

inline bool ParticleOutsidePlane(const Particle *p, float3 norm, float offset)
{
	if (dot(p->restPosition, norm) > offset)
		return true;

	return false;
}

inline bool BounceParticle(Particle *p, struct MovingPlane mp, float elapsedTime)
{
	float currentOffset = mp.offset + elapsedTime * mp.velocity;

	if (ParticleOutsidePlane(p, mp.normal, mp.offset))
	{
		float3 tangent = p->restPosition - (dot(p->restPosition, mp.normal)) * mp.normal;
		p->position = tangent + currentOffset * mp.normal;
	}
	return true;
}

inline bool DragParticle(Particle *p, const struct MovingPlane mp)
{
	if (ParticleOutsidePlane(p, mp.normal, mp.offset))
	{
		p->velocity = mp.velocity * mp.normal;
	}
	return true;
}


inline bool BacksideReflectBounce(Particle *p, struct MovingPlane mp, float elapsedTime, float epsilon)
{
	if (ParticleOutsidePlane(p, mp.normal, mp.offset))
		return false;

	float w = mp.offset + (elapsedTime * mp.velocity);

	if (dot(p->position, mp.normal) > w)
	{
		p->position += (epsilon + w - dot(p->position, mp.normal)) * mp.normal;

		//Zero velocity in normal direction
		p->velocity -= dot(p->velocity, mp.normal) * mp.normal;
		p->velocity *= 0.4f;	//Friction
		//TODO - Make friction a variable parameter

		return true;
	}

	return false;
}

inline void TwistParticle(Particle *p, struct TwistingPlane tp, float elapsedTime)
{
	if (elapsedTime > tp.lifetime)
		return;

	if (ParticleOutsidePlane(p, tp.normal, tp.offset))
	{
		//Rotate about support point
		const float sos = dot(tp.normal, tp.normal);
		const float3 supportPoint = (float3)(tp.normal.x * tp.offset / sos, tp.normal.y * tp.offset / sos, tp.normal.z * tp.offset / sos);

		//Project current position onto plane
		//Compute direction vector from support point to particle position
		float3 posInPlane = p->position - supportPoint;
		posInPlane -= dot(p->position, tp.normal) * tp.normal;

		float3 tangent1, tangent2;

		tangent1 = cross(tp.normal, (float3)(1.0f, 0.0f, 0.0f));
		if (tangent1.x <= 1e-3f || tangent1.y <= 1e-3f || tangent1.z <= 1e-3f)
		{
			tangent1 = cross(tp.normal, (float3)(0.0f, 0.0f, 1.0f));
			if (tangent1.x <= 1e-3f || tangent1.y <= 1e-3f || tangent1.z <= 1e-3f)
			{
				tangent1 = cross(tp.normal, (float3)(0.0f, 1.0f, 0.0f));
			}
		}
		tangent1 = normalize(tangent1);

		tangent2 = cross(tp.normal, tangent1);
		tangent2 = normalize(tangent2);

		// dot posInPlane with current tangent space to compute rotation velocity
		float t2_rot = tp.angularVelocity * dot(posInPlane, tangent1);
		float t1_rot = tp.angularVelocity * dot(posInPlane, tangent2);

		p->velocity = -t1_rot * tangent1 + t2_rot * tangent2;
	}
}

inline void TiltParticle(Particle *p, struct TiltingPlane tp, float elapsedTime)
{
	if (elapsedTime > tp.lifetime)
		return;

	if (ParticleOutsidePlane(p, tp.normal, tp.offset))
	{
		//Rotate about support point
		const float sos = dot(tp.normal, tp.normal);
		const float3 supportPoint = (float3)(tp.normal.x * tp.offset / sos, tp.normal.y * tp.offset / sos, tp.normal.z * tp.offset / sos);

		float3 tangent = cross(tp.normal, tp.tilt);

		//Project current position onto plane
		//Compute direction vector from support point to particle position
		float3 posInPlane = p->position - supportPoint;
		posInPlane -= dot(p->position, tp.tilt) * tp.tilt;

		// dot posInPlane with current tangent space to compute rotation velocity
		float t2_rot = tp.angularVelocity * dot(posInPlane, tp.normal);
		float t1_rot = tp.angularVelocity * dot(posInPlane, tangent);

		p->velocity = t1_rot * tp.normal - t2_rot * tp.normal;
	}
}

#endif //PLANE_UTILS_H