#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 translation;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec4 colors;

const int MAX_CLUSTERS = 500;

out vec2 TexCoords;
out vec4 ClusterColor;

uniform mat4 projection;
uniform mat4 view;

void main()
{
	TexCoords = texCoords;
	vec3 scale = vec3(0.5);

	mat4 model = mat4(	vec4(scale.x,	0.0,	 0.0,	  0.0),
						vec4(0.0,		scale.y, 0.0,	  0.0),
						vec4(0.0,		0.0,	 scale.z, 0.0),						
						vec4(translation  ,1.0));
	
    gl_Position = projection * view * model * vec4(position, 1.0f); 

	ClusterColor = colors;
}

