#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;
uniform vec4 color;

out vec4 vertColor;

void main()
{
	vertColor = color;
	gl_Position = projectionMat * viewMat * modelMat * vec4(position.xyz, 1.0);
}